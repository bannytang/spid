/***************************************************************************************************
 *  vender/spt/spid/spicomm.c
 *
 *  Spectra Technologies
 *  SPI Communication drivers for handling the Tx/Rx Data between Android and SC.
 *  Copyright (C) 2018 Harris Lee.
 *
 *  Date : 22 Jan 2018.
 *  Last updated : 16 May 2019.
 *
***************************************************************************************************/
/*==============================================*/
/* Naming conventions                           */
/* ~~~~~~~~~~~~~~~~~~                           */
/*         Class define : Leading C             */
/*        Struct define : Leading S             */
/*               Struct : Leading s             */
/*               Class  : Leading c             */
/*             Constant : Leading K             */
/*      Global Variable : Leading g             */
/*    Function argument : Leading a             */
/*       Local Variable : All lower case        */
/*            Byte size : Leading b             */
/*            Word size : Leading w  (16 bits)  */
/*           DWord size : Leading d  (32 bits)  */
/*          DDWord size : Leading dd (64 bits)  */
/*              Pointer : Leading p             */
/*==============================================*/

/**************************************************************************************************/
// Headers
/**************************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include <pthread.h>

#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/ioctl.h>
#include <sys/stat.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

#include "../common/dbg.h"
#include "../common/Util.h"
#include "../common/gpio.h"
#include "../common/Crc16.h"
#include "../common/ClassFnID.h"

#include "SPIComm.h"


/**************************************************************************************************/
// Function/Config Enable/Disable Switches
/**************************************************************************************************/
/*********************************************************************************/
// K_SPI_HEADER_SC_BIG_ENDIAN
// Config the Endian of the valuable of the SC Command / Data Length in SPI Header.
// Define it for the valuable is Big Endian. Remark it for Little Endian.
/*********************************************************************************/
#define K_SPI_HEADER_SC_BIG_ENDIAN


/*********************************************************************************/
// K_SPI_SC_CRC_SWAP_DATA_HI_LO
// Config the Endian of the CRC Calculation of the Data.
// Define it for the CRC Calculation with 16 bits Data High/Low Byte Swap. (i.e. CRC16_SwapHiLo())
// Remark it for the CRC Calculation with Data in origin sequence order. (i.e. CRC16())
/*********************************************************************************/
#define K_SPI_SC_CRC_SWAP_DATA_HI_LO


/*********************************************************************************/
// K_SPI_DATA_FOR_DMA
// Config the SPI Data Format for the DMA Transfer Mode. (1 - DMA Mode got problem. Byte Endian issue. 2 - Data >= 48 bytes using DMA)
// Define it for the SPI Data packing in the SPI DMA Mode Format.
// Remark it for the SPI Data packing in the SPI FIFO Mode Format.
/*********************************************************************************/
#define K_SPI_DATA_FOR_DMA


/**************************************************************************************************/
// Common Definition
/**************************************************************************************************/
#undef LOG_TAG
#define LOG_TAG                                 "spicomm"

#define K_SPI_DEV_SPI0                          "/dev/spidev0.0"
#define K_SPI_MODE                              SPI_MODE_3
#define K_SPI_SPEED                             1800000 // SPI Speed in Hz -> 1.8 MHz.
#define K_SPI_BITS_PER_WORD                     16      // 16 Bits Data.


/**************************************************************************************************/
// SPI Header Structure for Tx/Rx.
/**************************************************************************************************/
// +-----------+-------+-----------+----------+-----------+----------+----------+---+-----------+-----------+-----------+--------------+---------------+
// |   Byte1   | Byte2 |   Byte3   |  Byte4   |   Byte5   |  Byte6   |  Byte7   |...| Byte(6+n) | Byte(7+n) | Byte(8+n) |  Byte(9+n)   |  Byte(13+n)   |
// | StartCode |  LRC  |   CMD1    |  CMD2    |   LEN1    |  LEN1    | 1st Byte |   | Nth Byte  |   CRC1    |   CRC2    | 6Bytes Dummy | 2Bytes Result |
// |  0xA5/A7  |       | High Byte | Low Byte | High Byte | Low Byte | of Data  |...| of Data   | High Byte | Low Byte  |  Postamble   |  Postamble    |
// +-----------+-------+-----------+----------+-----------+----------+----------+---+-----------+-----------+-----------+--------------+---------------+
typedef struct {
  uint8_t b_StartCode;                  // Start Code (SYNC) (i.e. 0xA5. For Repeat message set bit 1 => A7).
  uint8_t b_LRC;                        // LRC = SYNC ^ CMD1 ^ CMD2 ^ LEN1 ^ LEN2.
  uint16_t w_Cmd;                       // Command Code. (i.e. 0x0000)
  uint16_t w_Length;                    // Data Length.
  uint8_t bs_Data[];                    // "w_Length" of the Actual Data.
//  uint16_t_crc;                       // CRC16 Modbus (x^16 + x^15 + x^2 + 1)(Initial 0xFFFF) value of the "bs_Data".
//  uint16_t w_DummyPostamble;          // Dummy Postamble.
//  uint16_t w_DummyPostamble;          // Dummy Postamble.
//  uint16_t w_Postamble;               // Tx - Postamble to get the Transmission Result(i.e. ACK/NAK/WAIT). Rx - Transmission Result ACK/NAK/WAIT.
} TS_SPI_WRITE_HEADER;


// +-----------+-------+-----------+----------+--------------+-------------+--------------+
// |   Byte1   | Byte2 |   Byte3   |  Byte4   |    Byte5     |    Byte7    |    Byte9     |
// | StartCode |  LRC  |   CMD1    |  CMD2    | 2Bytes Dummy | 2Bytes R1R2 | 2Bytes !R1R2 |
// |  0xA5/A7  |       | High Byte | Low Byte |  Postamble   |  Postamble  |  Postamble   |
// +-----------+-------+-----------+----------+--------------+-------------+--------------+
typedef struct {
  uint8_t b_StartCode;                  // Start Code (SYNC) (i.e. 0xA5. For Repeat message set bit 1 => A7).
  uint8_t b_LRC;                        // LRC = !(SYNC ^ CMD1).
  uint16_t w_Cmd;                       // Command Code. (i.e. 0x8000)
  uint16_t w_DummyPostamble;            // Dummy Postamble.
  uint16_t w_R1R2;                      // Tx - Postamble to get the Response Length(i.e. R1|R2). Rx - R1|R2 value.
  uint16_t w_CompR1R2;                  // Tx - Postamble to get Complement of the Response Length(i.e. !R1|!R2). Rx - !R1|!R2 value.
} TS_SPI_READRESPLEN_HEADER;


// +-----------+-------+-----------+----------+-----------+----------+--------------+-----------+---+-----------+-------------+--------------+
// |   Byte1   | Byte2 |   Byte3   |  Byte4   |   Byte5   |  Byte6   |    Byte7     |  Byte9    |...| Byte(8+n) |  Byte(9+n)  |  Byte(10+n)  |
// | StartCode |  LRC  |   CMD1    |  CMD2    |   LEN1    |  LEN1    | 2Bytes Dummy | 1st Data  |   | Nth Data  | CRC1 HiByte | CRC2 LowByte |
// |  0xA5/A7  |       | High Byte | Low Byte | High Byte | Low Byte |  Postamble   | Postamble |...| Postamble |  Postamble  |  Postamble   |
// +-----------+-------+-----------+----------+-----------+----------+--------------+-----------+---+-----------+-------------+--------------+
typedef struct {
  uint8_t b_StartCode;                  // Start Code (SYNC) (i.e. 0xA5. For Repeat message set bit 1 => A7).
  uint8_t b_LRC;                        // LRC = SYNC ^ CMD1 ^ CMD2 ^ LEN1 ^ LEN2.
  uint16_t w_Cmd;                       // Command Code. (i.e. 0x80001)
  uint16_t w_Length;                    // Data Length (i.e. R1|R2).
  uint16_t w_DummyPostamble;            // Dummy Postamble.
  uint8_t bs_Data[];                    // Tx - "w_Length" bytes of Postamble to get the Response Data. Rx - "w_Length" of the Response Data.
//  uint16_t_crc;                       // Tx - 2 bytes of Postamble to get the CRC16. Rx - The CRC16 Modbus value of the "bs_Data".
} TS_SPI_READRESP_HEADER;


#define K_SPI_STARTCODE                       0xA5        // Start Code.

// SC Message Command Code.
#define K_SPI_WRITE_CMD                       ((uint16_t) 0x0000)       // Write Command
#define K_SPI_READ_RESP_LEN_CMD               ((uint16_t) 0x8000)       // Read Response Length Command
#define K_SPI_READ_RESP_CMD                   ((uint16_t) 0x8001)       // Read Response Command

#define K_SPI_STARTCODE_SEQUENCE_BIT          0x02        // Sequence Bit(Bit 1) in SYNC Byte to indicate that the Command sequence is Success or Not.


// Tx/Rx State, "gb_TxRxState"
enum {
  K_SPI_TXRX_STATE_IDLE,
  K_SPI_TXRX_STATE_LOCK,
  K_SPI_TXRX_STATE_SEND_CMD,
  K_SPI_TXRX_STATE_RECV_LEN,
  K_SPI_TXRX_STATE_RECV_DATA,
  K_SPI_TXRX_STATE_COMPLETE,
  K_SPI_TXRX_STATE_FAIL
};


// SPI Header Size for all of the Tx/Rx SPI Header.
#define K_SPI_HDR_CRC_SIZE                          2       // SPI Header Common CRC Size in Byte. i.e. CRC16 Value of the "s_data".

#define K_SPI_WRITE_HDR_SIZE                        (sizeof(TS_SPI_WRITE_HEADER))
#define K_SPI_WRITE_HDR_CMD_SIZE                    (sizeof(((TS_SPI_WRITE_HEADER *)0)->w_Cmd))
#define K_SPI_WRITE_HDR_LENGTH_SIZE                 (sizeof(((TS_SPI_WRITE_HEADER *)0)->w_Length))
#define K_SPI_WRITE_HDR_DUMMYPOSTAMBLE_SIZE         6       // SPI Write Header Dummy Postamble Size in Byte.
#define K_SPI_WRITE_HDR_ACKPOSTAMBLE_SIZE           2       // SPI Write Header Postamble Size in Byte for Acknowledgment.

#define K_SPI_READ_RESP_LEN_HDR_SIZE                (sizeof(TS_SPI_READRESPLEN_HEADER))
#define K_SPI_READ_RESP_HDR_SIZE                    (sizeof(TS_SPI_READRESP_HEADER))
#define K_SPI_READ_HDR_DUMMYPOSTAMBLE_SIZE          2       // SPI Read Response Length/ Read Response Header Dummy Postamble Size in Byte.


// Tx/Rx Buffer Overhead Size for all Header/Overhead
#define K_SPI_TX_RX_BUF_HEADER_OVERHEAD_SIZE        (K_SPI_WRITE_HDR_SIZE + K_SPI_HDR_CRC_SIZE + K_SPI_WRITE_HDR_DUMMYPOSTAMBLE_SIZE + K_SPI_WRITE_HDR_ACKPOSTAMBLE_SIZE)

// Total TX/RX Buffer Size with all Header/Overhead in Byte
#define K_SPI_TX_RX_BUF_SIZE_WITH_OVERHEAD          (K_SPI_MAX_TX_RX_SIZE + K_SPI_TX_RX_BUF_HEADER_OVERHEAD_SIZE)

// Total TX/RX Buffer Size with all Header/Overhead in Word(16 Bits)
#define K_SPI_TX_RX_BUF_SIZE_WITH_OVERHEAD_WORD     ((unsigned int)((K_SPI_TX_RX_BUF_SIZE_WITH_OVERHEAD + 1) / 2))

#define K_SPI_CRC_INITIAL_VALUE                     0xffff    // SPI Modbus CRC16 Initial Value.

// Timer Interval Setting
#ifdef DEBUG_SHOW_MESSG_SPICOMM
#define K_SPI_TX_GAP_SEC                            0 // 2    // in Sec, 5s.
#define K_SPI_RX_GAP_SEC                            0 // 2    // in Sec, 5s.
#else
#define K_SPI_TX_GAP_SEC                            0   // in Sec, 0s.
#define K_SPI_RX_GAP_SEC                            0   // in Sec, 0s.
#endif

#define K_SPI_TX_GAP_MS                             1   // in ms, 1ms. (max 4.29 Sec)  // No Tx gap requirement between commands.
#define K_SPI_RX_GAP_MS                             1   // in ms, 1ms. (max 4.29 Sec)  // Recommended that 1ms gap should be applied between retry although it does not really need.

#define K_SPI_TX_GAP_NS                             (K_SPI_TX_GAP_MS * 1000000)
#define K_SPI_RX_GAP_NS                             (K_SPI_RX_GAP_MS * 1000000)

#define K_SPI_TX_GAP_WAIT_SEC                       0   // in Sec, 0s.
#define K_SPI_TX_GAP_WAIT_MS                        50  // in ms, 50ms. (max 4.29 Sec)
#define K_SPI_TX_GAP_WAIT_NS                        (K_SPI_TX_GAP_WAIT_MS * 1000000)

// Number of Tx/Rx Retry when Error.
#define K_SPI_TXRX_NO_OF_RETRY                      5

#define K_SPI_GPIO_IN_PIN_SP_POWER_SLEEP            92      // GPIO Pin (Input, Base: PinCtl IO) for Secure CPU Power Sleep.

#ifdef K_SPI_DATA_FOR_DMA
#define K_SPI_DATA_SIZE_FOR_DMA                     48      // SPI Data Message >= 48 bytes using DMA Mode.
#endif


// Tx/Rx State for "isSPITxRxReady()"
enum {
  K_SPI_TXRX_READY,
  K_SPI_TXRX_BUSY,
  K_SPI_TXRX_SC_ERR,
};


/**************************************************************************************************/
// Global
/**************************************************************************************************/
static int gd_SPI_fd = -1;        // SPI Device File Descriptor.

// Common Buffers
// SPI Main Global Tx/Rx Buffer.
// uint16_t make sure buffer alignd in Word(16Bits) Starting Address. i.e. Address in Multiple of 2.
static uint16_t gbs_TxBuf[K_SPI_TX_RX_BUF_SIZE_WITH_OVERHEAD_WORD];
static uint16_t gbs_RxBuf[K_SPI_TX_RX_BUF_SIZE_WITH_OVERHEAD_WORD];

// Tx/Rx SPI Packets.
static struct spi_ioc_transfer s_ico_transfer;

// SPID SPI Rx Buffer Status Flag and Mutex Lock.
static pthread_mutex_t gs_TxRxStateMutex = PTHREAD_MUTEX_INITIALIZER;
static volatile uint8_t gb_TxRxState;   // Tx/Rx State.
static volatile uint8_t gb_TxRxRetry;   // Tx/Rx Retry Count.
static volatile uint8_t gb_Error;

static timer_t gs_TxTimerHdrID;    // Tx Timer Handler ID.
static timer_t gs_RxTimerHdrID;    // Rx Timer Handler ID.

// File Descriptor of GPIO Input Pin for Secure CPU Power Sleep.
static int gd_GpioInPwrSleepFd;

#ifndef DEBUG_SPI_DISABLE_SC_SLEEP_DETECTION
// SPI Chip Select Power Sleep Wake Up Low Time.
#define K_SPI_CS_WAKE_UP_LOW_SEC                0     // in Sec, 0s.
#define K_SPI_CS_WAKE_UP_LOW_MS                 5     // in ms. SPI CS will be lowered for 3ms before start sending data to make sure it is awaken.
#define K_SPI_CS_WAKE_UP_LOW_NS                 (K_SPI_CS_WAKE_UP_LOW_MS * 1000000)     // in ns

// SPI Chip Select Power Sleep Wake Up Low Time.
static const struct timespec K_CSPwrSleepLowTime = { K_SPI_CS_WAKE_UP_LOW_SEC, K_SPI_CS_WAKE_UP_LOW_NS };
#endif

#ifdef DEBUG_CONFIG_SW_PLATFORM
// Flag to indicate that the Software is run on the Fibocom EVK or not.
uint8_t gb_isFibocom_EVK;
#endif

#ifdef K_SPI_DATA_FOR_DMA
static uint8_t gb_isSPITxDMAMode;
#endif

#ifdef DEBUG_ENABLE_TIMING
extern struct timeval gs_start, gs_finish;
#endif


/**************************************************************************************************/
// DEBUG_PACK_RESPONSE_DATA
// For Debug Only.
// Functions to Packing Simulated SC Responsed Data for the Specified SC Command.
/**************************************************************************************************/
#ifdef DEBUG_PACK_RESPONSE_DATA
#include <sys/system_properties.h>
#include "../common/ClassFnID.h"
#include "spid.h"

// Structure to mapping the Simulated SC Response Data.
typedef struct {
  uint16_t w_DataLen;                   // Length of the Response Data.
  uint8_t *bp_RespData;                 // Response Data.
} TS_SPI_RESPDATA;


// Add SC Response Data here if need... (Refers to "OSM-A10 Secure CPU-xxxx.doc" for detail description.)
#ifndef K_SPI_PAYLOAD_SC_BIG_ENDIAN
// Little Endian Payload
// SC Command/Response Message Format Data Mapping.
// +------------+---------+----------+-----------+-------------+-------------+------------+---+--------------+
// |   Byte 2   |  Byte 1 |  Byte 4  |   Byte 3  |   Byte 6    |   Byte 5    |   Byte 7   |...| Byte (6 + n) |
// | FunctionID | ClassID |  GCM_IV  |  GCM_IV   | Data Length | Data Length | First Byte |   |   Nth Byte   |
// |            |         | Low Byte | High Byte |  Low Byte   |  High Byte  |  of Data   |...|   of Data    |
// +------------+---------+----------+-----------+-------------+-------------+------------+---+--------------+

// LPT Class
static const uint8_t K_SPIRDLptOpen[] = { K_SysFuncLptOpen, K_SysCmdClassLpt, 0x00, 0x00, 0x02, 0x00,
                                          0x00, 0x01 };
static const uint8_t K_SPIRDLptPrint[] = { K_SysFuncLptPrint, K_SysCmdClassLpt, 0x00, 0x00, 0x00, 0x00 };
static const uint8_t K_SPIRDLptClose[] = { K_SysFuncLptClose, K_SysCmdClassLpt, 0x00, 0x00, 0x00, 0x00 };

// Buzzer Class
static const uint8_t K_SPIRDBuzBeep[] = { K_SysFuncBuzBeep, K_SysCmdClassBuz, 0x00, 0x00, 0x00, 0x00 };
static const uint8_t K_SPIRDBuzOff[] = { K_SysFuncBuzOff, K_SysCmdClassBuz, 0x00, 0x00, 0x00, 0x00 };

// MSR Class
static const uint8_t K_SPIRDMSROpen[] = { K_SysFuncMsrOpen, K_SysCmdClassMsr, 0x00, 0x00, 0x02, 0x00,
                                          0x00, 0x01 };

static const uint8_t K_SPIRDMSRClose[] = { K_SysFuncMsrClose, K_SysCmdClassMsr, 0x00, 0x00, 0x00, 0x00 };

static const uint8_t K_SPIRDMSRReadAlert[] = { // K_SysFuncMsrReadAlert, K_SysCmdClassMsr, 0x00, 0x00, (0x02 + 0x27 + 0x4c + 0x6a), 0x00,
                                               "\x02\x03\x00\x00\xdf\x00"
                                               "\x07\x00"
                                               "\x00""1234567890=2222222222=1234567890=1234""\x00"
                                               "\x00""ABCDEFGHIJKLMNOPQRSTUVWXYZ+1234567890+1234567890+1234567890+1234567890+123""\x00"
                                               "\x00""1234567890=3333333333=1234567890=1234567890=1234567890=1234567890=1234567890=1234567890=1234567890=123456" };
//                                               "\x00""1234567890=3333333333=1234567890=1234567890=1234567890=1234567890=1234567890=1234567890=1234567890=12345" };

// Other Class
static const uint8_t K_SPIRDTest[] = { K_SysFuncLptControl, K_SysCmdClassLpt, 0x00, 0x00, 0x10, 0x00,
//static const uint8_t K_SPIRDTest[] = { K_SysFuncLptAlert, K_SysCmdClassLpt, 0x00, 0x00, 0x10, 0x00,
                                       0xA5, 0xAA, 0x55, 0x11, 0x22, 0x33, 0x44, 0x55,
                                       0x66, 0x77, 0x88, 0x99, 0xff, 0x55, 0xaa, 0xa5 };
#else
// Big Endian Payload
// SC Command/Response Message Format Data Mapping.
// +---------+------------+-----------+----------+-------------+-------------+------------+---+--------------+
// |  Byte 1 |   Byte 2   |   Byte 3  |  Byte 4  |   Byte 5    |   Byte 6    |   Byte 7   |...| Byte (6 + n) |
// | ClassID | FunctionID |  GCM_IV   |  GCM_IV  | Data Length | Data Length | First Byte |   |   Nth Byte   |
// |         |            | High Byte | Low Byte |  High Byte  |  Low Byte   |  of Data   |...|   of Data    |
// +---------+------------+-----------+----------+-------------+-------------+------------+---+--------------+

// LPT Class
static const uint8_t K_SPIRDLptOpen[] = { K_SysCmdClassLpt, K_SysFuncLptOpen, 0x00, 0x00, 0x00, 0x02,
                                          0x00, 0x01 };
static const uint8_t K_SPIRDLptPrint[] = { K_SysCmdClassLpt, K_SysFuncLptPrint, 0x00, 0x00, 0x00, 0x00 };
static const uint8_t K_SPIRDLptClose[] = { K_SysCmdClassLpt, K_SysFuncLptClose, 0x00, 0x00, 0x00, 0x00 };

// Buzzer Class
static const uint8_t K_SPIRDBuzBeep[] = { K_SysCmdClassBuz, K_SysFuncBuzBeep, 0x00, 0x00, 0x00, 0x00 };
static const uint8_t K_SPIRDBuzOff[] = { K_SysCmdClassBuz, K_SysFuncBuzOff, 0x00, 0x00, 0x00, 0x00 };

// MSR Class
static const uint8_t K_SPIRDMSROpen[] = { K_SysCmdClassMsr, K_SysFuncMsrOpen, 0x00, 0x00, 0x00, 0x02,
                                          0x00, 0x01 };

static const uint8_t K_SPIRDMSRClose[] = { K_SysCmdClassMsr, K_SysFuncMsrClose, 0x00, 0x00, 0x00, 0x00 };

static const uint8_t K_SPIRDMSRReadAlert[] = { // K_SysCmdClassMsr, K_SysFuncMsrReadAlert, 0x00, 0x00, 0x00, (0x02 + 0x27 + 0x4c + 0x6a),
                                               "\x03\x02\x00\x00\x00\xdf"
                                               "\x00\x07"
                                               "\x00""1234567890=2222222222=1234567890=1234""\x00"
                                               "\x00""ABCDEFGHIJKLMNOPQRSTUVWXYZ+1234567890+1234567890+1234567890+1234567890+123""\x00"
                                               "\x00""1234567890=3333333333=1234567890=1234567890=1234567890=1234567890=1234567890=1234567890=1234567890=12345" };


// Other Class
static const uint8_t K_SPIRDTest[] = { K_SysCmdClassLpt, K_SysFuncLptControl, 0x00, 0x00, 0x00, 0x10,
//static const uint8_t K_SPIRDTest[] = { K_SysCmdClassLpt, K_SysFuncLptAlert, 0x00, 0x00, 0x00, 0x10,
                                       0xA5, 0xAA, 0x55, 0x11, 0x22, 0x33, 0x44, 0x55,
                                       0x66, 0x77, 0x88, 0x99, 0xff, 0x55, 0xaa, 0xa5 };
#endif

// Simulated SC Response Data Mapping Tables.
static const TS_SPI_RESPDATA gs_SPIRespData[] = {
// Don't modify the First 3 items. For Dispatch Response Testing.
// gs_SPIRespData[0] => Turn On Dispatch Response. gs_SPIRespData[1] => Turn Off Dispatch Response. gs_SPIRespData[2] => Dispatch Response Data.
  { sizeof(K_SPIRDBuzBeep), (uint8_t *)K_SPIRDBuzBeep },
  { sizeof(K_SPIRDBuzOff), (uint8_t *)K_SPIRDBuzOff },
  { sizeof(K_SPIRDMSRReadAlert), (uint8_t *)K_SPIRDMSRReadAlert },

  { sizeof(K_SPIRDLptOpen), (uint8_t *)K_SPIRDLptOpen },
  { sizeof(K_SPIRDLptPrint), (uint8_t *)K_SPIRDLptPrint },
  { sizeof(K_SPIRDLptClose), (uint8_t *)K_SPIRDLptClose },
  { sizeof(K_SPIRDMSROpen), (uint8_t *)K_SPIRDMSROpen },
  { sizeof(K_SPIRDMSRClose), (uint8_t *)K_SPIRDMSRClose },
  { sizeof(K_SPIRDTest), (uint8_t *) K_SPIRDTest }
};


/**************************************************************************************************/
// Don't Modify below...
/**************************************************************************************************/
#define K_SPI_NO_OF_RESPDATA          (sizeof(gs_SPIRespData) / sizeof(gs_SPIRespData[0]))

// Flag to indicate that the Pack Response Data function is Enabled or not.
uint8_t gb_isPackRespData;

// Store the latest SC Command (i.e. Class ID | Function ID ) which send out by write command.
static uint16_t gw_LastSCCmd;

// Timer for simulate the Dispatch Message.
static timer_t gs_DispatchTxTimerHdrID;

// Time to dispatch a message.
#define K_DISPATCH_MESG_TX_SEC                10    // in Sec, 10s.

#endif


/**************************************************************************************************/
// SPI Tx/Rx Handler
/**************************************************************************************************/
/**************************************************************************************************/
static void SPI_TxHandler()   // union sigval as_val)   // Remove Warning as not used.
{
  size_t d_TxLen = s_ico_transfer.len;

#ifdef DEBUG_SHOW_MESSG_SPICOMM
#ifdef DEBUG_PACK_RESPONSE_DATA
  if (gb_isPackRespData != K_SPIPackRespData_RndWriteCRC)
#endif
    LogD_HexDump(gbs_TxBuf, d_TxLen, 16, "CmdTx");
#endif

#ifdef K_SPI_DATA_FOR_DMA
  if (gb_isSPITxDMAMode)
    s_ico_transfer.bits_per_word = 8;
  else
    s_ico_transfer.bits_per_word = 16;
#endif

#ifdef DEBUG_ENABLE_TIMING
  if (gettimeofday(&gs_finish, NULL) == -1)
    LOGE_TIME("SPI_TxHandler: Get Time Err.");
  else
    LOGD_TIME("SPI_TxHandler: Before SPI Send: Elapsed: %.6f ms", (((gs_finish.tv_sec - gs_start.tv_sec) * 1000.0) + ((gs_finish.tv_usec - gs_start.tv_usec) / 1000.0)));
#endif

  if (ioctl(gd_SPI_fd, SPI_IOC_MESSAGE(1), &s_ico_transfer) < 0) {
    LOGE("SPI_TxHandler: Can't Send SPI Message: %s", strerror(errno));

SPI_TXHANDLER_RETRY:
    if (--gb_TxRxRetry) {
      // Not using periodical timer to prevent Thread re-entry/Racing condition.
      if (!TimerHandlerStart(&gs_TxTimerHdrID, K_SPI_TX_GAP_SEC, K_SPI_TX_GAP_NS, 0, 0)) {
SPI_TXHANDLER_TX_TIMER_ERR:
        gb_Error = K_SPI_ERR_TX_TIMER_FAIL;
        gb_TxRxState = K_SPI_TXRX_STATE_FAIL;
      }
      return;
    }
    else {
      // ACK not received and Retry Fail, Tx Fail.
      LOGE("SPI_TxHandler: Retry Fail! Timeout, Tx Fail!");
      gb_Error = K_SPI_ERR_TX_TIMEOUT;
      gb_TxRxState = K_SPI_TXRX_STATE_FAIL;
      return;
    }
  }

#ifdef DEBUG_ENABLE_TIMING
  if (gettimeofday(&gs_finish, NULL) == -1)
    LOGE_TIME("SPI_TxHandler: Get Time Err.");
  else
    LOGD_TIME("SPI_TxHandler: After SPI Send: Elapsed: %.6f ms", (((gs_finish.tv_sec - gs_start.tv_sec) * 1000.0) + ((gs_finish.tv_usec - gs_start.tv_usec) / 1000.0)));
#endif

#ifdef DEBUG_SHOW_MESSG_SPICOMM
#ifdef DEBUG_PACK_RESPONSE_DATA
  if (gb_isPackRespData != K_SPIPackRespData_RndWriteCRC)
#endif
    LogD_HexDump(gbs_RxBuf, d_TxLen, 16, "CmdRx");
#endif

  // Verify Ack Code.
  uint8_t *sp_SPIAckResp = ((uint8_t *)gbs_RxBuf) + d_TxLen - 2;
  if ((sp_SPIAckResp[0] ^ sp_SPIAckResp[1]) != 0xff) {
    LOGE("SPI_TxHandler: Ack Verify Error: 0x%02X", *((uint16_t *)(sp_SPIAckResp)));
    gb_Error = K_SPI_ERR_TX_ACK_CODE;
    goto SPI_TXHANDLER_RETRY;   // ACK not received, Tx Retry.
  }

  // Ack Code Ok.
  switch (sp_SPIAckResp[0]) {
    case K_ACK:
      LOGD("SPI_TxHandler: SC reply ACK");
      gb_TxRxState = K_SPI_TXRX_STATE_COMPLETE;
      return;

    case K_NACK:
      LOGE("SPI_TxHandler: SC reply NACK");
      gb_Error = K_SPI_ERR_TX_SC_NACK;
      goto SPI_TXHANDLER_RETRY;   // ACK not received, Tx Retry.

    case K_WAIT:
      LOGE("SPI_TxHandler: SC reply WAIT");
      if (--gb_TxRxRetry) {
        // Not using periodical timer to prevent Thread re-entry/Racing condition.
        if (!TimerHandlerStart(&gs_TxTimerHdrID, K_SPI_TX_GAP_WAIT_SEC, K_SPI_TX_GAP_WAIT_NS, 0, 0))
          goto SPI_TXHANDLER_TX_TIMER_ERR;
      }
      else {
        // WAIT Retry Fail, Tx Fail.
        LOGE("SPI_TxHandler: WAIT Retry Fail! Timeout, Tx Fail!");
        gb_Error = K_SPI_ERR_TX_TIMEOUT_WAIT;
        gb_TxRxState = K_SPI_TXRX_STATE_FAIL;
        return;
      }
      break;

    default:
      LOGE("SPI_TxHandler: SC reply unknown Ack Code: 0x%02X", *((uint16_t *)(sp_SPIAckResp)));
      gb_Error = K_SPI_ERR_TX_ACK_CODE;
      goto SPI_TXHANDLER_RETRY;   // ACK not received, Tx Retry.
  }
}


static void SPI_RxHandler()   // union sigval as_val)   // Remove Warning as not used.
{
  size_t d_RxLen = s_ico_transfer.len;

#ifdef DEBUG_SHOW_MESSG_SPICOMM
  LogD_HexDump(gbs_TxBuf, d_RxLen, 16, "RespTx");
#endif

#ifdef K_SPI_DATA_FOR_DMA
  if (gb_isSPITxDMAMode)
    s_ico_transfer.bits_per_word = 8;
  else
    s_ico_transfer.bits_per_word = 16;
#endif

#ifdef DEBUG_ENABLE_TIMING
  if (gettimeofday(&gs_finish, NULL) == -1)
    LOGE_TIME("SPI_RxHandler: Get Time Err.");
  else
    LOGD_TIME("SPI_RxHandler: Before SPI Send: Elapsed: %.6f ms", (((gs_finish.tv_sec - gs_start.tv_sec) * 1000.0) + ((gs_finish.tv_usec - gs_start.tv_usec) / 1000.0)));
#endif

  if (ioctl(gd_SPI_fd, SPI_IOC_MESSAGE(1), &s_ico_transfer) < 0) {
    LOGE("SPI_RxHandler: Can't Send SPI Message: %s", strerror(errno));

SPI_RXHANDLER_RETRY:
    if (--gb_TxRxRetry) {
      // Not using periodical timer to prevent Thread re-entry/Racing condition.
      if (!TimerHandlerStart(&gs_RxTimerHdrID, K_SPI_RX_GAP_SEC, K_SPI_RX_GAP_NS, 0, 0)) {
        gb_Error = K_SPI_ERR_RX_TIMER_FAIL;
        gb_TxRxState = K_SPI_TXRX_STATE_FAIL;
      }
      return;
    }
    else {
      // Retry Fail, Rx Fail.
      LOGE("SPI_RxHandler: Retry Fail! Timeout, Rx Fail!");
      gb_Error = K_SPI_ERR_RX_TIMEOUT;
      gb_TxRxState = K_SPI_TXRX_STATE_FAIL;
      return;
    }
  }

#ifdef DEBUG_ENABLE_TIMING
  if (gettimeofday(&gs_finish, NULL) == -1)
    LOGE_TIME("SPI_RxHandler: Get Time Err.");
  else
    LOGD_TIME("SPI_RxHandler: After SPI Send: Elapsed: %.6f ms", (((gs_finish.tv_sec - gs_start.tv_sec) * 1000.0) + ((gs_finish.tv_usec - gs_start.tv_usec) / 1000.0)));
#endif

#ifdef DEBUG_SHOW_MESSG_SPICOMM
  LogD_HexDump(gbs_RxBuf, d_RxLen, 16, "RespRx");
#endif

  switch (gb_TxRxState) {
    case K_SPI_TXRX_STATE_RECV_LEN: {
      // Verify R1R2.
      TS_SPI_READRESPLEN_HEADER *sp_SPIReadRespLen = (TS_SPI_READRESPLEN_HEADER *) gbs_RxBuf;
      if ((sp_SPIReadRespLen->w_R1R2 ^ sp_SPIReadRespLen->w_CompR1R2) != 0xffff) {
        LOGE("SPI_RxHandler: R1R2 Verify Error: 0x%04X, 0x%04X.", sp_SPIReadRespLen->w_R1R2, sp_SPIReadRespLen->w_CompR1R2);
        gb_Error = K_SPI_ERR_RX_DATA_LEN;
        goto SPI_RXHANDLER_RETRY;   // Rx Retry.
      }
      gb_TxRxState = K_SPI_TXRX_STATE_RECV_DATA;
      break;
    }

    case K_SPI_TXRX_STATE_RECV_DATA: {
      // Verify Rx Data CRC.
      TS_SPI_READRESP_HEADER *sp_SPIReadResp = (TS_SPI_READRESP_HEADER *) gbs_RxBuf;
      uint16_t w_crc_calc, w_crc_read;
      d_RxLen -= (K_SPI_READ_RESP_HDR_SIZE + K_SPI_HDR_CRC_SIZE);   // Actual Data Length.
#ifdef K_SPI_HEADER_SC_BIG_ENDIAN
      w_crc_read = sp_SPIReadResp->bs_Data[d_RxLen];
      w_crc_read = (w_crc_read << 8) | sp_SPIReadResp->bs_Data[d_RxLen + 1];
#else
      w_crc_read = sp_SPIReadResp->bs_Data[d_RxLen + 1];
      w_crc_read = (w_crc_read << 8) | sp_SPIReadResp->bs_Data[d_RxLen];
#endif

#ifndef K_SPI_SC_CRC_SWAP_DATA_HI_LO
      w_crc_calc = CRC16((uint8_t *)(sp_SPIReadResp->bs_Data), d_RxLen, K_SPI_CRC_INITIAL_VALUE);
#else
      w_crc_calc = CRC16_SwapHiLo((uint8_t *)(sp_SPIReadResp->bs_Data), d_RxLen, K_SPI_CRC_INITIAL_VALUE);
#endif

      if (w_crc_calc != w_crc_read) {
        LOGE("SPI_RxHandler: CRC Incorrect: Read: 0x%04X. Calc: 0x%04X.", w_crc_read, w_crc_calc);
        gb_Error = K_SPI_ERR_RX_DATA_CRC;
        goto SPI_RXHANDLER_RETRY;   // Rx Retry.
      }

      LOGD("SPI_RxHandler: CRC Correct: 0x%04X.", w_crc_read);
      gb_TxRxState = K_SPI_TXRX_STATE_COMPLETE;
      break;
    }
  }
}


/**************************************************************************************************/
#ifdef DEBUG_PACK_RESPONSE_DATA
static size_t isSPITxRxReady(void);

static void DispatchMesg_TxHandler()
{
  if (isSPITxRxReady() == K_SPI_TXRX_READY) {
    gw_LastSCCmd = *((uint16_t *)gs_SPIRespData[2].bp_RespData);
#ifdef DEBUG_SHOW_MESSG_SPICOMM
    TS_SC_MESSAGE_HEADER *sp_SCMesg = (TS_SC_MESSAGE_HEADER *)gs_SPIRespData[2].bp_RespData;
    LOGD("DispatchMesg_TxHandler: ClassID: 0x%02X. FuncID: 0x%02X.", sp_SCMesg->b_ClassID, sp_SCMesg->b_FnID);
#endif
    // Read the SPI Response Data from the SC and Relpy the Response Data back to SCFService.
    SPIDReadSCResponse();
  }
}
#endif


/**************************************************************************************************/
// GPIOSPPwrSleepDisable
// Disable the GPIO Input Pin for the Secure CPU Power Sleep.
// Return : None.
/**************************************************************************************************/
static void GPIOSPPwrSleepDisable(void)
{
  gpio_close(gd_GpioInPwrSleepFd);
  gd_GpioInPwrSleepFd = -1;
  gpio_set_dir(K_SPI_GPIO_IN_PIN_SP_POWER_SLEEP, K_SYSFS_GPIO_BASE_OFFSET_PINCTL, K_GPIO_DIR_IN);
  gpio_set_edge(K_SPI_GPIO_IN_PIN_SP_POWER_SLEEP, K_SYSFS_GPIO_BASE_OFFSET_PINCTL, K_GPIO_EDGE_NONE);
  gpio_unexport(K_SPI_GPIO_IN_PIN_SP_POWER_SLEEP, K_SYSFS_GPIO_BASE_OFFSET_PINCTL);
}


/**************************************************************************************************/
// SPI Device Open/Close
/**************************************************************************************************/
/**************************************************************************************************/
// SPI_Open
// Open the SPI device.
// Return : TRUE -> Success.
//        : FALSE -> Fail.
/**************************************************************************************************/
size_t SPI_Open(void)
{
  TS_SPI_WRITE_HEADER *sp_SPIWriteCmd = (TS_SPI_WRITE_HEADER *) gbs_TxBuf;
  unsigned int d_Speed = K_SPI_SPEED;                       // SPI Speed.
  unsigned char b_Mode = K_SPI_MODE;                        // SPI Mode.
  unsigned char b_BitsPerWord = K_SPI_BITS_PER_WORD;        // SPI Bits Per Word.

  gb_Error = K_SPI_ERR_NONE;

  // Init GPIO Port.
  // Reset File Descriptor of the GPIO Input Pin for the Secure CPU Power Sleep.
  gd_GpioInPwrSleepFd = -1;

  // Force to disable all of the GPIO Ports. Otherwise, the GPIO maybe Open error if it has been enabled before.
  GPIOSPPwrSleepDisable();

  // Init GPIO Input Pin for the Secure CPU Power Sleep..
  LOGD("SPI_Open: Init GPIO for SC Power Sleep");
  if (!gpio_export(K_SPI_GPIO_IN_PIN_SP_POWER_SLEEP, K_SYSFS_GPIO_BASE_OFFSET_PINCTL))
    return FALSE;
  if (!gpio_set_dir(K_SPI_GPIO_IN_PIN_SP_POWER_SLEEP, K_SYSFS_GPIO_BASE_OFFSET_PINCTL, K_GPIO_DIR_IN)) {
SPI_OPEN_GPIO_ERR_EXIT:
    GPIOSPPwrSleepDisable();
    return FALSE;
  }
  if (!gpio_set_edge(K_SPI_GPIO_IN_PIN_SP_POWER_SLEEP, K_SYSFS_GPIO_BASE_OFFSET_PINCTL, K_GPIO_EDGE_BOTH))
    goto SPI_OPEN_GPIO_ERR_EXIT;
  if ((gd_GpioInPwrSleepFd = gpio_open(K_SPI_GPIO_IN_PIN_SP_POWER_SLEEP, K_SYSFS_GPIO_BASE_OFFSET_PINCTL)) == -1)
    goto SPI_OPEN_GPIO_ERR_EXIT;

  // Init SPI Port.
  if ((gd_SPI_fd = open(K_SPI_DEV_SPI0, O_RDWR)) < 0) {
    LOGE("SPI_Open: Open SPI Device: %s Fail. Error: %s.", K_SPI_DEV_SPI0, strerror(errno));
    goto SPI_OPEN_GPIO_ERR_EXIT;
  }
  LOGD("SPI_Open: Open SPI Device: %s Success. gd_SPI_fd: %d", K_SPI_DEV_SPI0, gd_SPI_fd);

  // SPI Mode
  if (ioctl(gd_SPI_fd, SPI_IOC_WR_MODE, &b_Mode) < 0) {
    LOGE("SPI_Open: Set Mode Fail. Error: %s.", strerror(errno));
SPI_OPEN_ERR_EXIT:
    SPI_Close();
    return FALSE;
  }
  ioctl(gd_SPI_fd, SPI_IOC_RD_MODE, &b_Mode);
  LOGD("SPI_Open: SPI Mode: Set: %d. Read: %d.", K_SPI_MODE, b_Mode);

  // Max Speed
  if (ioctl(gd_SPI_fd, SPI_IOC_WR_MAX_SPEED_HZ, &d_Speed) < 0) {
    LOGE("SPI_Open: Set Speed Fail. Error: %s.", strerror(errno));
    goto SPI_OPEN_ERR_EXIT;
  }
  ioctl(gd_SPI_fd, SPI_IOC_RD_MAX_SPEED_HZ, &d_Speed);
  LOGD("SPI_Open: SPI Speed: Set: %d Hz (%d KHz). Read: %d Hz (%d KHz).", K_SPI_SPEED, (K_SPI_SPEED / 1000), d_Speed, (d_Speed / 1000));

  // Bits Per Word
  if (ioctl(gd_SPI_fd, SPI_IOC_WR_BITS_PER_WORD, &b_BitsPerWord) < 0) {
    LOGE("SPI_Open: Set Bits Per Word Fail. Error: %s.", strerror(errno));
    goto SPI_OPEN_ERR_EXIT;
  }
  ioctl(gd_SPI_fd, SPI_IOC_RD_BITS_PER_WORD, &b_BitsPerWord);
  LOGD("SPI_Open: SPI Bits Per Word: Set: %d. Read: %d.", K_SPI_BITS_PER_WORD, b_BitsPerWord);

  // Init SPI Tx Handler.
  if (!CreateTimerHandler(SPI_TxHandler, &gs_TxTimerHdrID, 255)) {
    LOGD("SPI_Open: Create Tx Timer Fail!");
    goto SPI_OPEN_ERR_EXIT;
  }

  // Init SPI Rx Handler.
  if (!CreateTimerHandler(SPI_RxHandler, &gs_RxTimerHdrID, 255)) {
    LOGD("SPI_Open: Create Rx Timer Fail!");
    TimerHandlerDelete(&gs_TxTimerHdrID);
    goto SPI_OPEN_ERR_EXIT;
  }

#ifdef DEBUG_PACK_RESPONSE_DATA
  // Init Dispatch Message Tx Handler.
  if (!CreateTimerHandler(DispatchMesg_TxHandler, &gs_DispatchTxTimerHdrID, 255)) {
    LOGD("SPI_Open: Create DispatchMesg Tx Timer Fail!");
    TimerHandlerDelete(&gs_TxTimerHdrID);
    TimerHandlerDelete(&gs_RxTimerHdrID);
    goto SPI_OPEN_ERR_EXIT;
  }
#endif

  // Initial Tx/Rx State
  LOGD("SPI_Open: SPI Tx/Rx Buffer Size in Byte (for each): %d (must >= %d).", sizeof(gbs_TxBuf), K_SPI_TX_RX_BUF_SIZE_WITH_OVERHEAD);
  LOGD("SPI_Open: SPI Tx/Rx Buffer Starting Address: 0x%08X / 0x%08X.", (unsigned int)&gbs_TxBuf, (unsigned int)&gbs_RxBuf);

  gb_TxRxState = K_SPI_TXRX_STATE_IDLE;

  // Config the SPI IOC Transfer Structure for Tx/Rx.
  memset(&s_ico_transfer, 0, sizeof(s_ico_transfer));
  s_ico_transfer.tx_buf = (unsigned long)gbs_TxBuf;
  s_ico_transfer.rx_buf = (unsigned long)gbs_RxBuf;

  // Initizlize the SPI Initial Start Code for Toggling Later.
  sp_SPIWriteCmd->b_StartCode = (K_SPI_STARTCODE | K_SPI_STARTCODE_SEQUENCE_BIT);

#ifdef DEBUG_CONFIG_SW_PLATFORM
  // Config the Platform(i.e. A10 or Fibocom) which Software is run.
  char bp_isFibocom_EVK[PROP_VALUE_MAX+1];
  if (__system_property_get(K_isFibocom_EVK_EnvString, bp_isFibocom_EVK))
    gb_isFibocom_EVK = atoi(bp_isFibocom_EVK);
  else
    gb_isFibocom_EVK = 0;  // Default A10 Board.
  LOGD("SPI_Open: isFibocom_EVK: %d.", gb_isFibocom_EVK);
#endif

#ifdef DEBUG_PACK_RESPONSE_DATA
  // Config the Debug Pack Response Data Switches.
  char bp_isPackRespData[PROP_VALUE_MAX+1];
  if (__system_property_get(K_SPIPackRespData_EnvString, bp_isPackRespData))
    gb_isPackRespData = atoi(bp_isPackRespData);
  else
    gb_isPackRespData = 0;  // Default Packing Response Data function is disabled
  LOGD("SPI_Open: SPI isPackRespData: %d.", gb_isPackRespData);
#endif

  LOGD("SPI_Open: SPI Config Completed!");

  return TRUE;
}


/**************************************************************************************************/
// SPI_Close
// Close the SPI device.
// Return : TRUE -> Success.
//        : FALSE -> Fail.
/**************************************************************************************************/
size_t SPI_Close(void)
{
  if (gd_SPI_fd < 0)
    return FALSE;

  TimerHandlerDelete(&gs_TxTimerHdrID);
  TimerHandlerDelete(&gs_RxTimerHdrID);
#ifdef DEBUG_PACK_RESPONSE_DATA
  TimerHandlerDelete(&gs_DispatchTxTimerHdrID);
#endif

  close(gd_SPI_fd);
  gd_SPI_fd = -1;

  GPIOSPPwrSleepDisable();

  LOGD("SPI Device: %s closed", K_SPI_DEV_SPI0);

  return TRUE;
}


/**************************************************************************************************/
// SPI_ResetSequenceBit
// Reset the SPI Starting Code Sequence Bit to sync with the SC side when SC is Reset using the SC Reset Command.
/**************************************************************************************************/
void SPI_ResetSequenceBit(void)
{
  TS_SPI_WRITE_HEADER *sp_SPIWriteCmd = (TS_SPI_WRITE_HEADER *) gbs_TxBuf;

  // Initizlize the SPI Initial Start Code for Toggling Later.
  sp_SPIWriteCmd->b_StartCode = (K_SPI_STARTCODE | K_SPI_STARTCODE_SEQUENCE_BIT);

  LOGD("SPI_ResetSequenceBit: SPI Sequence Bit is Reset: 0x%02X.", sp_SPIWriteCmd->b_StartCode);
}


/**************************************************************************************************/
// SPI Tx/RX Command
/**************************************************************************************************/
/**************************************************************************************************/
// isSPITxRxReady
// Checking SPI is ready to Tx/Rx or Not.
// As the SPI is just One Channel, Read/Write command can't be run at the same time.
// Return : K_SPI_TXRX_READY -> Tx/Rx is Ready.
//        : K_SPI_TXRX_BUSY -> Tx/Rx is Busy and not Ready.
//        : K_SPI_TXRX_SC_ERR -> Can't Wake Up SC. Fail to Send Command.
/**************************************************************************************************/
static size_t isSPITxRxReady(void)
{
#ifndef DEBUG_SPI_DISABLE_SC_SLEEP_DETECTION
  unsigned char gbState = TRUE;
#ifdef DEBUG_ENABLE_TIMING
  struct timeval s_start, s_finish;
#endif
#endif

  while (pthread_mutex_trylock(&gs_TxRxStateMutex));
  if (gb_TxRxState != K_SPI_TXRX_STATE_IDLE) {
    pthread_mutex_unlock(&gs_TxRxStateMutex);
    return K_SPI_TXRX_BUSY;
  }

  gb_TxRxState = K_SPI_TXRX_STATE_LOCK;
  pthread_mutex_unlock(&gs_TxRxStateMutex);

  // Secure CPU in Sleep Mode ?
#ifndef DEBUG_SPI_DISABLE_SC_SLEEP_DETECTION
  if (gpio_get_state_fd(gd_GpioInPwrSleepFd) == K_GPIO_STATE_LOW) {
#ifdef DEBUG_ENABLE_TIMING
    if (gettimeofday(&s_finish, NULL) == -1)
      LOGE_TIME("isSPITxRxReady: Get Time Err.");
    else
      LOGD_TIME("isSPITxRxReady: SC Sleeping, Wake It Up... Elapsed: 0 ms");
#else
    LOGD("isSPITxRxReady: SC Sleeping, Wake It Up...");
#endif

    if (ioctl(gd_SPI_fd, SPI_IOC_ENABLE_SC_CS, &gbState) < 0)
      LOGE("isSPITxRxReady: CS Low Fail: %s.", strerror(errno));
    else
      LOGD("isSPITxRxReady: CS Low...");

#ifdef DEBUG_ENABLE_TIMING
    if (gettimeofday(&s_start, NULL) == -1)
      LOGE_TIME("isSPITxRxReady: Get Time Err.");
#endif

    clock_nanosleep(CLOCK_MONOTONIC, 0, &K_CSPwrSleepLowTime, NULL);

#ifdef DEBUG_ENABLE_TIMING
    if (gettimeofday(&s_finish, NULL) == -1)
      LOGE_TIME("isSPITxRxReady: Get Time Err.");
    else
      LOGD_TIME("isSPITxRxReady: CS Low for: Elapsed: %.6f ms", (((s_finish.tv_sec - s_start.tv_sec) * 1000.0) + ((s_finish.tv_usec - s_start.tv_usec) / 1000.0)));
#endif

    gbState = FALSE;
    if (ioctl(gd_SPI_fd, SPI_IOC_ENABLE_SC_CS, &gbState) < 0)
      LOGE("isSPITxRxReady: CS High Fail: %s.", strerror(errno));
    else
      LOGD("isSPITxRxReady: CS High...");

    // Can wake up Secure CPU or not ?
    if (gpio_get_state_fd(gd_GpioInPwrSleepFd) == K_GPIO_STATE_LOW) {
      LOGI("isSPITxRxReady: Wake Up SC Fail.");
      gb_Error = K_SPI_ERR_SC_WAKE_UP;
      while (pthread_mutex_trylock(&gs_TxRxStateMutex));
      gb_TxRxState = K_SPI_TXRX_STATE_IDLE;
      pthread_mutex_unlock(&gs_TxRxStateMutex);
      return K_SPI_TXRX_SC_ERR;
    }

    LOGD("isSPITxRxReady: Wake Up SC Success.");
  }
#endif

  return K_SPI_TXRX_READY;
}


/**************************************************************************************************/
// SPI Write Command
/**************************************************************************************************/
/**************************************************************************************************/
// SPI_Write
// SPI Send Write Command Data to SC.
// ap_TxBuf = Pointer to the Data Buffer which the Tx Data to be sent.
// aw_TxLen = Length of the Data to be sent.
// Return : -1 -> SPI Write Fail.
//        : Other's Value -> Length of the Data be sent. SPI Write Success.
/**************************************************************************************************/
ssize_t SPI_Write(uint8_t const *ap_TxBuf, uint16_t aw_TxLen)
{
  TS_SPI_WRITE_HEADER *sp_SPIWriteCmd = (TS_SPI_WRITE_HEADER *) gbs_TxBuf;
  uint16_t w_TxLen = aw_TxLen;
  uint16_t w_crc_calc;
  size_t d_SPITxRxState;

  if (w_TxLen > K_SPI_MAX_TX_RX_SIZE) {
    LOGE("SPI_WriteCmd: Tx Data Size over Limit: %d. (Limit: >= 0 and <= %d)", w_TxLen, K_SPI_MAX_TX_RX_SIZE);
    gb_Error = K_SPI_ERR_TX_DATA_SIZE_LIMIT;
    return -1;
  }

SPI_WRITE_RETRY:
  if ((d_SPITxRxState = isSPITxRxReady()) != K_SPI_TXRX_READY) { // If Tx / Rx state is Busy.
    if (d_SPITxRxState == K_SPI_TXRX_BUSY)
      goto SPI_WRITE_RETRY;
    else {
      LOGE("SPI_WriteCmd: Wake Up SC Error! Error: %d", gb_Error);
      return -1;
    }
  }

  gb_Error = K_SPI_ERR_NONE;

  // Init Tx State to lock all of the SPI functions.
  gb_TxRxState = K_SPI_TXRX_STATE_SEND_CMD;
  gb_TxRxRetry = K_SPI_TXRX_NO_OF_RETRY;

#ifdef DEBUG_ENABLE_TIMING
  TS_SC_MESSAGE_HEADER *sp_SCMesg = (TS_SC_MESSAGE_HEADER *)ap_TxBuf;
  if (gettimeofday(&gs_finish, NULL) == -1)
    LOGE_TIME("SPI_WriteCmd: Get Time Err.");
  else
    LOGD_TIME("SPI_WriteCmd: Start...: ClassID: 0x%02X. FuncID: 0x%02X. Elapsed: %.6f ms", sp_SCMesg->b_ClassID, sp_SCMesg->b_FnID, (((gs_finish.tv_sec - gs_start.tv_sec) * 1000.0) + ((gs_finish.tv_usec - gs_start.tv_usec) / 1000.0)));
#else
#ifdef DEBUG_SHOW_MESSG_SPICOMM
  TS_SC_MESSAGE_HEADER *sp_SCMesg = (TS_SC_MESSAGE_HEADER *)ap_TxBuf;
  LOGD("SPI_WriteCmd: ClassID: 0x%02X. FuncID: 0x%02X.", sp_SCMesg->b_ClassID, sp_SCMesg->b_FnID);
#endif
#endif

  // Packing SPI Write Command Packet
  sp_SPIWriteCmd->b_StartCode ^= K_SPI_STARTCODE_SEQUENCE_BIT;
#ifdef K_SPI_HEADER_SC_BIG_ENDIAN
  sp_SPIWriteCmd->w_Cmd = (K_SPI_WRITE_CMD >> 8) | (uint16_t)(K_SPI_WRITE_CMD << 8);
  sp_SPIWriteCmd->w_Length = (w_TxLen >> 8) | (w_TxLen << 8);
#else
  sp_SPIWriteCmd->w_Cmd = K_SPI_WRITE_CMD;
  sp_SPIWriteCmd->w_Length = w_TxLen;
#endif

  // LRC = (SYNC ^ CMD1 ^ CMD2 ^ LEN1 ^ LEN2)
  sp_SPIWriteCmd->b_LRC = sp_SPIWriteCmd->b_StartCode ^ lrc((uint8_t *)&sp_SPIWriteCmd->w_Cmd, (K_SPI_WRITE_HDR_CMD_SIZE + K_SPI_WRITE_HDR_LENGTH_SIZE));

  // Packing Data to be Transferred.
  if (w_TxLen > 0) {   // if Data length is 0, there will be no Data and CRC fields.
    memcpy(sp_SPIWriteCmd->bs_Data, (char *)ap_TxBuf, w_TxLen);

    // CRC of the Data.
#ifndef K_SPI_SC_CRC_SWAP_DATA_HI_LO
    w_crc_calc = CRC16((uint8_t *)(sp_SPIWriteCmd->bs_Data), w_TxLen, K_SPI_CRC_INITIAL_VALUE);
#else
    w_crc_calc = CRC16_SwapHiLo((uint8_t *)(sp_SPIWriteCmd->bs_Data), w_TxLen, K_SPI_CRC_INITIAL_VALUE);
#endif

#ifdef K_SPI_HEADER_SC_BIG_ENDIAN
    sp_SPIWriteCmd->bs_Data[w_TxLen++] = (w_crc_calc >> 8) & 0x00ff;
    sp_SPIWriteCmd->bs_Data[w_TxLen++] = w_crc_calc & 0x00ff;
#else
    sp_SPIWriteCmd->bs_Data[w_TxLen++] = w_crc_calc & 0xff;
    sp_SPIWriteCmd->bs_Data[w_TxLen++] = (w_crc_calc >> 8) & 0xff;
#endif
  }
  // Packing Dummy Postamble and Postamble to get Transmission Result.
  memset(&sp_SPIWriteCmd->bs_Data[w_TxLen], 0, (K_SPI_WRITE_HDR_DUMMYPOSTAMBLE_SIZE + K_SPI_WRITE_HDR_ACKPOSTAMBLE_SIZE));

#ifdef DEBUG_PACK_RESPONSE_DATA
  if (gb_isPackRespData) {
#ifdef DEBUG_CONFIG_SW_PLATFORM
    if (gb_isFibocom_EVK) {
      sp_SPIWriteCmd->bs_Data[w_TxLen + K_SPI_WRITE_HDR_DUMMYPOSTAMBLE_SIZE] = K_ACK;
      sp_SPIWriteCmd->bs_Data[w_TxLen + (K_SPI_WRITE_HDR_DUMMYPOSTAMBLE_SIZE + 1)] = ~K_ACK;
    }
    else
#endif
    {
      sp_SPIWriteCmd->bs_Data[w_TxLen + (K_SPI_WRITE_HDR_DUMMYPOSTAMBLE_SIZE - 2)] = K_ACK;
      sp_SPIWriteCmd->bs_Data[w_TxLen + (K_SPI_WRITE_HDR_DUMMYPOSTAMBLE_SIZE - 1)] = ~K_ACK;
    }

    gw_LastSCCmd = *((uint16_t *)sp_SPIWriteCmd->bs_Data);
  }
#endif

  // Tx/Rx SPI Packets.
  w_TxLen += K_SPI_WRITE_HDR_SIZE + K_SPI_WRITE_HDR_DUMMYPOSTAMBLE_SIZE + K_SPI_WRITE_HDR_ACKPOSTAMBLE_SIZE;
  s_ico_transfer.len = w_TxLen;

#ifdef K_SPI_DATA_FOR_DMA
  gb_isSPITxDMAMode = ((w_TxLen >= K_SPI_DATA_SIZE_FOR_DMA) && ((w_TxLen & 1) == 0));
#endif

#ifdef DEBUG_SHOW_MESSG_SPICOMM
  LOGD("SPI_WriteCmd: Tx Packet Size: %d", w_TxLen);
#endif

  // Not using periodical timer to prevent Thread re-entry/Racing condition.
  if (!TimerHandlerStart(&gs_TxTimerHdrID, 0, 1, 0, 0)) {
    gb_Error = K_SPI_ERR_TX_TIMER_FAIL;
    while (pthread_mutex_trylock(&gs_TxRxStateMutex));
    gb_TxRxState = K_SPI_TXRX_STATE_IDLE;
    pthread_mutex_unlock(&gs_TxRxStateMutex);
    return -1;
  }

  while (gb_TxRxState == K_SPI_TXRX_STATE_SEND_CMD) {};

#ifdef DEBUG_PACK_RESPONSE_DATA
  if (gb_isPackRespData == K_SPIPackRespData_RndWriteCRC) {
    TS_SPI_WRITE_HEADER *sp_SPIWriteCmdReply = (TS_SPI_WRITE_HEADER *)gbs_RxBuf;
    uint8_t *bs_databuf = ((uint8_t *)(sp_SPIWriteCmdReply->bs_Data)) + 2;

#ifdef DEBUG_CONFIG_SW_PLATFORM
    if (gb_isFibocom_EVK)
      bs_databuf -= 2;
#endif

#ifndef K_SPI_SC_CRC_SWAP_DATA_HI_LO
    if (w_crc_calc == CRC16(bs_databuf, aw_TxLen, K_SPI_CRC_INITIAL_VALUE))
#else
    if (w_crc_calc == CRC16_SwapHiLo(bs_databuf, aw_TxLen, K_SPI_CRC_INITIAL_VALUE))
#endif
      LOGD("SPI_WriteCmd: Rnd Data(Size:%d): CRC(0x%02X) Ok.", aw_TxLen, w_crc_calc);
    else {
      LOGD("SPI_WriteCmd: Rnd Data(Size:%d): CRC(0x%02X) Fail.", aw_TxLen, w_crc_calc);
      gb_Error = K_SPI_ERR_RX_DATA_CRC;
      gb_TxRxState = K_SPI_TXRX_STATE_FAIL;
    }
  }
  else
    if (gb_isPackRespData >= K_SPIPackRespData_IgnoreErr) {
      while (pthread_mutex_trylock(&gs_TxRxStateMutex));
      gb_TxRxState = K_SPI_TXRX_STATE_IDLE;
      pthread_mutex_unlock(&gs_TxRxStateMutex);
      return aw_TxLen;
    }
#endif

#ifdef DEBUG_ENABLE_TIMING
  if (gettimeofday(&gs_finish, NULL) == -1)
    LOGE_TIME("SPI_WriteCmd: Get Time Err.");
  else
    LOGD_TIME("SPI_WriteCmd: Completed: ClassID: 0x%02X. FuncID: 0x%02X. Elapsed: %.6f ms", sp_SCMesg->b_ClassID, sp_SCMesg->b_FnID, (((gs_finish.tv_sec - gs_start.tv_sec) * 1000.0) + ((gs_finish.tv_usec - gs_start.tv_usec) / 1000.0)));
#endif

  if (gb_TxRxState == K_SPI_TXRX_STATE_FAIL) {
    while (pthread_mutex_trylock(&gs_TxRxStateMutex));
    gb_TxRxState = K_SPI_TXRX_STATE_IDLE;
    pthread_mutex_unlock(&gs_TxRxStateMutex);
    return -1;
  }

  gb_Error = K_SPI_ERR_NONE;
  while (pthread_mutex_trylock(&gs_TxRxStateMutex));
  gb_TxRxState = K_SPI_TXRX_STATE_IDLE;
  pthread_mutex_unlock(&gs_TxRxStateMutex);
  return aw_TxLen;
}


/**************************************************************************************************/
// SPI Read Command
/**************************************************************************************************/
/**************************************************************************************************/
// SPI_ReadRespLen
// SPI Read Response Length Command.
// Return : -1 -> Get Response Length Fail.
//        : Other's Value -> Response Length of the Data to be read by the SPI_ReadResp Command. SPI Read Success.
/**************************************************************************************************/
static ssize_t SPI_ReadRespLen(void)
{
  TS_SPI_READRESPLEN_HEADER *sp_SPIReadRespLenCmd = (TS_SPI_READRESPLEN_HEADER *) gbs_TxBuf;
  TS_SPI_READRESPLEN_HEADER *sp_SPIReadRespLen = (TS_SPI_READRESPLEN_HEADER *) gbs_RxBuf;

#ifdef DEBUG_ENABLE_TIMING
  if (gettimeofday(&gs_finish, NULL) == -1)
    LOGE_TIME("SPI_ReadRespLen: Get Time Err.");
  else
    LOGD_TIME("SPI_ReadRespLen: Start...: Elapsed: %.6f ms", (((gs_finish.tv_sec - gs_start.tv_sec) * 1000.0) + ((gs_finish.tv_usec - gs_start.tv_usec) / 1000.0)));
#else
  LOGD("SPI_ReadRespLen: Start...");
#endif

  gb_Error = K_SPI_ERR_NONE;

  // Init Rx State to lock all of the SPI functions.
  gb_TxRxState = K_SPI_TXRX_STATE_RECV_LEN;
  gb_TxRxRetry = K_SPI_TXRX_NO_OF_RETRY;

  // Packing SPI Read Response Length Command Packet
  sp_SPIReadRespLenCmd->b_StartCode ^= K_SPI_STARTCODE_SEQUENCE_BIT;
#ifdef K_SPI_HEADER_SC_BIG_ENDIAN
  sp_SPIReadRespLenCmd->w_Cmd = (K_SPI_READ_RESP_LEN_CMD >> 8) | (uint16_t)(K_SPI_READ_RESP_LEN_CMD << 8);
#else
  sp_SPIReadRespLenCmd->w_Cmd = K_SPI_READ_RESP_LEN_CMD;
#endif

    // LRC = (CMD1 ^ SYNC)
#ifdef K_SPI_HEADER_SC_BIG_ENDIAN
    sp_SPIReadRespLenCmd->b_LRC = ((uint8_t)((K_SPI_READ_RESP_LEN_CMD >> 8) & 0xff)) ^ sp_SPIReadRespLenCmd->b_StartCode;
#else
    sp_SPIReadRespLenCmd->b_LRC =  ((uint8_t)(K_SPI_READ_RESP_LEN_CMD & 0xff)) ^ sp_SPIReadRespLenCmd->b_StartCode;
#endif

  // Packing Dummy Postamble and Postamble to get response length - R1|R2 and R1!|R2!.
  sp_SPIReadRespLenCmd->w_DummyPostamble = 0;
  sp_SPIReadRespLenCmd->w_R1R2 = 0;
  sp_SPIReadRespLenCmd->w_CompR1R2 = 0;

#ifdef DEBUG_PACK_RESPONSE_DATA
  if ((gb_isPackRespData) && (gb_isPackRespData < K_SPIPackRespData_InputData)) {
    for (uint i = 0; i < K_SPI_NO_OF_RESPDATA; i++) {
      if (gw_LastSCCmd == *((uint16_t *)gs_SPIRespData[i].bp_RespData)) {

        // Checking Dispatch ON/OFF (MSR_Open/MSR_Close) Commands.
        if (i == 0) {
          if (TimerHandlerStart(&gs_DispatchTxTimerHdrID, K_DISPATCH_MESG_TX_SEC, 0, K_DISPATCH_MESG_TX_SEC, 0))
            LOGD("SPI_ReadRespLen: DispatchTxTimerHdr ON.");
          else
            LOGD("SPI_ReadRespLen: DispatchTxTimerHdr ON Fail.");
        }
        else
          if (i == 1) {
            if (TimerHandlerStop(&gs_DispatchTxTimerHdrID))
              LOGD("SPI_ReadRespLen: DispatchTxTimerHdr OFF.");
            else
              LOGD("SPI_ReadRespLen: DispatchTxTimerHdr OFF Fail.");
          }

        // Checking got Response Data or Not.
        TS_SC_MESSAGE_HEADER *sp_SCMesg = (TS_SC_MESSAGE_HEADER *)gs_SPIRespData[i].bp_RespData;
        if ((sp_SCMesg->b_PayloadLenHi == 0) && (sp_SCMesg->b_PayloadLenLo == 0)) {
          LOGD("SPI_ReadRespLen: No Response Data.");
          while (pthread_mutex_trylock(&gs_TxRxStateMutex));
          gb_TxRxState = K_SPI_TXRX_STATE_IDLE;
          pthread_mutex_unlock(&gs_TxRxStateMutex);
          return 0;
        }

#ifdef DEBUG_CONFIG_SW_PLATFORM
        if (gb_isFibocom_EVK) {
#ifdef K_SPI_HEADER_SC_BIG_ENDIAN
          sp_SPIReadRespLenCmd->w_R1R2 = (gs_SPIRespData[i].w_DataLen >> 8) | (gs_SPIRespData[i].w_DataLen << 8);
#else
          sp_SPIReadRespLenCmd->w_R1R2 = gs_SPIRespData[i].w_DataLen;
#endif
          sp_SPIReadRespLenCmd->w_CompR1R2 = ~sp_SPIReadRespLenCmd->w_R1R2;
        }
        else
#endif
        {
#ifdef K_SPI_HEADER_SC_BIG_ENDIAN
          sp_SPIReadRespLenCmd->w_DummyPostamble = (gs_SPIRespData[i].w_DataLen >> 8) | (gs_SPIRespData[i].w_DataLen << 8);
#else
          sp_SPIReadRespLenCmd->w_DummyPostamble = gs_SPIRespData[i].w_DataLen;
#endif
          sp_SPIReadRespLenCmd->w_R1R2 = ~sp_SPIReadRespLenCmd->w_DummyPostamble;
        }

        break;
      }
    }
  }
#endif

  // Tx/Rx SPI Packets.
  s_ico_transfer.len = K_SPI_READ_RESP_LEN_HDR_SIZE;

  LOGD("SPI_ReadRespLen: Packet Size: %d", K_SPI_READ_RESP_LEN_HDR_SIZE);

#ifdef K_SPI_DATA_FOR_DMA
  gb_isSPITxDMAMode = ((K_SPI_READ_RESP_LEN_HDR_SIZE >= K_SPI_DATA_SIZE_FOR_DMA) && ((K_SPI_READ_RESP_LEN_HDR_SIZE & 1) == 0));
#endif

  /// Not using periodical timer to prevent Thread re-entry/Racing condition.
  if (!TimerHandlerStart(&gs_RxTimerHdrID, 0, 1, 0, 0)) {
    LOGE("SPI_ReadRespLen: RxTimerHdr Fail: gb_TxRxState: %d", gb_TxRxState);
    gb_Error = K_SPI_ERR_RX_TIMER_FAIL;
    while (pthread_mutex_trylock(&gs_TxRxStateMutex));
    gb_TxRxState = K_SPI_TXRX_STATE_IDLE;
    pthread_mutex_unlock(&gs_TxRxStateMutex);
    return -1;
  }

  while (gb_TxRxState == K_SPI_TXRX_STATE_RECV_LEN) {};

#ifdef DEBUG_PACK_RESPONSE_DATA
  if (gb_isPackRespData >= K_SPIPackRespData_IgnoreErr) {
    uint16_t w_R1R2;

#ifdef DEBUG_CONFIG_SW_PLATFORM
    if (gb_isFibocom_EVK)
      w_R1R2 = sp_SPIReadRespLenCmd->w_R1R2;
    else
#endif
    w_R1R2 = sp_SPIReadRespLenCmd->w_DummyPostamble;

#ifdef K_SPI_HEADER_SC_BIG_ENDIAN
    w_R1R2 = (w_R1R2 >> 8) | (w_R1R2 << 8);
#endif
    return (w_R1R2);
  }
#endif

  if (gb_TxRxState == K_SPI_TXRX_STATE_FAIL) {
    while (pthread_mutex_trylock(&gs_TxRxStateMutex));
    gb_TxRxState = K_SPI_TXRX_STATE_IDLE;
    pthread_mutex_unlock(&gs_TxRxStateMutex);
    return -1;
  }

  // R1R2 Value Ok.
#ifdef K_SPI_HEADER_SC_BIG_ENDIAN
  sp_SPIReadRespLen->w_R1R2 = (sp_SPIReadRespLen->w_R1R2 >> 8) | (sp_SPIReadRespLen->w_R1R2 << 8);
#endif
  LOGD("SPI_ReadRespLen: R1R2 Ok: 0x%04X (%d).", sp_SPIReadRespLen->w_R1R2, sp_SPIReadRespLen->w_R1R2);

#ifdef DEBUG_ENABLE_TIMING
  if (gettimeofday(&gs_finish, NULL) == -1)
    LOGE_TIME("SPI_ReadRespLen: Get Time Err.");
  else
    LOGD_TIME("SPI_ReadRespLen: Completed: Elapsed: %.6f ms", (((gs_finish.tv_sec - gs_start.tv_sec) * 1000.0) + ((gs_finish.tv_usec - gs_start.tv_usec) / 1000.0)));
#endif

  return (sp_SPIReadRespLen->w_R1R2);
}


/**************************************************************************************************/
// SPI_ReadResp
// SPI Read the Response Data.
// ap_RxBuf = Pointer to the Data buffer to store the Received Data.
// ad_RxLen = Length of the Data to be received.
// Return : -1 -> Get Response Data Fail.
//        : Other's Value -> Length of the Response Data be read from the Secure CPU. SPI Read Success.
//        :               -> Response Data be placed at the buffer which be pointed by "ap_RxBuf".
/**************************************************************************************************/
static ssize_t SPI_ReadResp(uint8_t const *ap_RxBuf, uint16_t ad_RxLen)
{
  TS_SPI_READRESP_HEADER *sp_SPIReadRespCmd = (TS_SPI_READRESP_HEADER *) gbs_TxBuf;
  TS_SPI_READRESP_HEADER *sp_SPIReadResp = (TS_SPI_READRESP_HEADER *) gbs_RxBuf;
  uint16_t d_TxLen = ad_RxLen;

#ifdef DEBUG_ENABLE_TIMING
  if (gettimeofday(&gs_finish, NULL) == -1)
    LOGE_TIME("SPI_ReadResp: Get Time Err.");
  else
    LOGD_TIME("SPI_ReadResp: Start...: Elapsed: %.6f ms", (((gs_finish.tv_sec - gs_start.tv_sec) * 1000.0) + ((gs_finish.tv_usec - gs_start.tv_usec) / 1000.0)));
#else
  LOGD("SPI_ReadResp: Start...");
#endif

  gb_Error = K_SPI_ERR_NONE;

  if (ad_RxLen == 0) {
    LOGE("SPI_ReadResp: Zero Rx Data Length");
    while (pthread_mutex_trylock(&gs_TxRxStateMutex));
    gb_TxRxState = K_SPI_TXRX_STATE_IDLE;
    pthread_mutex_unlock(&gs_TxRxStateMutex);
    return 0;
  }

  if (ad_RxLen > K_SPI_MAX_TX_RX_SIZE) {
    LOGE("SPI_ReadResp: Rx Data Size Limit: %d. (Size Limit: > 0 and <= %d)", ad_RxLen, K_SPI_MAX_TX_RX_SIZE);
    gb_Error = K_SPI_ERR_RX_DATA_OVER_LIMIT;
    while (pthread_mutex_trylock(&gs_TxRxStateMutex));
    gb_TxRxState = K_SPI_TXRX_STATE_IDLE;
    pthread_mutex_unlock(&gs_TxRxStateMutex);
    return -1;
  }

  // Init Rx State to lock all of the SPI functions.
  gb_TxRxState = K_SPI_TXRX_STATE_RECV_DATA;
  gb_TxRxRetry = K_SPI_TXRX_NO_OF_RETRY;

  // Packing SPI Read Response Command Packet
  sp_SPIReadRespCmd->b_StartCode ^= K_SPI_STARTCODE_SEQUENCE_BIT;
#ifdef K_SPI_HEADER_SC_BIG_ENDIAN
  sp_SPIReadRespCmd->w_Cmd = (K_SPI_READ_RESP_CMD >> 8) | (uint16_t)(K_SPI_READ_RESP_CMD << 8);
  sp_SPIReadRespCmd->w_Length = (d_TxLen >> 8) | (d_TxLen << 8);
#else
  sp_SPIReadRespCmd->w_Cmd = K_SPI_READ_RESP_CMD;
  sp_SPIReadRespCmd->w_Length = d_TxLen;
#endif

  // LRC = (SYNC ^ CMD1 ^ CMD2 ^ LEN1 ^ LEN2)
  sp_SPIReadRespCmd->b_LRC = sp_SPIReadRespCmd->b_StartCode ^ lrc((uint8_t *)&sp_SPIReadRespCmd->w_Cmd, (K_SPI_WRITE_HDR_CMD_SIZE + K_SPI_WRITE_HDR_LENGTH_SIZE));

  // Packing Dummy Postamble, Postamble to get the Response Data and CRC.
  d_TxLen += K_SPI_READ_HDR_DUMMYPOSTAMBLE_SIZE + K_SPI_HDR_CRC_SIZE;

#ifdef DEBUG_PACK_RESPONSE_DATA
  if ((gb_isPackRespData) && (gb_isPackRespData < K_SPIPackRespData_InputData)) {
    uint16_t w_crc_calc;
    for (uint i = 0; i < K_SPI_NO_OF_RESPDATA; i++) {
      if (gw_LastSCCmd == *((uint16_t *)gs_SPIRespData[i].bp_RespData)) {
#ifndef K_SPI_SC_CRC_SWAP_DATA_HI_LO
        w_crc_calc = CRC16(gs_SPIRespData[i].bp_RespData, gs_SPIRespData[i].w_DataLen, K_SPI_CRC_INITIAL_VALUE);
#else
        w_crc_calc = CRC16_SwapHiLo(gs_SPIRespData[i].bp_RespData, gs_SPIRespData[i].w_DataLen, K_SPI_CRC_INITIAL_VALUE);
#endif

#ifdef DEBUG_CONFIG_SW_PLATFORM
        if (gb_isFibocom_EVK) {
/**************************************************************************************************/
          sp_SPIReadRespCmd->w_DummyPostamble = 0;
          memcpy(sp_SPIReadRespCmd->bs_Data, gs_SPIRespData[i].bp_RespData, gs_SPIRespData[i].w_DataLen);
#ifdef K_SPI_HEADER_SC_BIG_ENDIAN
          sp_SPIReadRespCmd->bs_Data[gs_SPIRespData[i].w_DataLen] = (w_crc_calc >> 8) & 0xff;
          sp_SPIReadRespCmd->bs_Data[gs_SPIRespData[i].w_DataLen + 1] = w_crc_calc & 0xff;
#else
          sp_SPIReadRespCmd->bs_Data[gs_SPIRespData[i].w_DataLen] = w_crc_calc & 0xff;
          sp_SPIReadRespCmd->bs_Data[gs_SPIRespData[i].w_DataLen + 1] = (w_crc_calc >> 8) & 0xff;
#endif
/**************************************************************************************************/
        }
        else
#endif
        {
/**************************************************************************************************/
          memcpy((sp_SPIReadRespCmd->bs_Data - 2), gs_SPIRespData[i].bp_RespData, gs_SPIRespData[i].w_DataLen);
#ifdef K_SPI_HEADER_SC_BIG_ENDIAN
          sp_SPIReadRespCmd->bs_Data[gs_SPIRespData[i].w_DataLen - 2] = (w_crc_calc >> 8) & 0xff;
          sp_SPIReadRespCmd->bs_Data[gs_SPIRespData[i].w_DataLen - 1] = w_crc_calc & 0xff;
#else
          sp_SPIReadRespCmd->bs_Data[gs_SPIRespData[i].w_DataLen - 2] = w_crc_calc & 0xff;
          sp_SPIReadRespCmd->bs_Data[gs_SPIRespData[i].w_DataLen - 1] = (w_crc_calc >> 8) & 0xff;
#endif
          memset(&sp_SPIReadRespCmd->bs_Data[gs_SPIRespData[i].w_DataLen], 0, 2);
/**************************************************************************************************/
        }
        break;
      }
    }
  }
  else
    memset(&sp_SPIReadRespCmd->w_DummyPostamble, 0, d_TxLen);

#else

  memset(&sp_SPIReadRespCmd->w_DummyPostamble, 0, d_TxLen);

#endif

  // Tx/Rx SPI Packets.
  d_TxLen += K_SPI_READ_RESP_HDR_SIZE - K_SPI_READ_HDR_DUMMYPOSTAMBLE_SIZE;
  s_ico_transfer.len = d_TxLen;

#ifdef K_SPI_DATA_FOR_DMA
  gb_isSPITxDMAMode = ((d_TxLen >= K_SPI_DATA_SIZE_FOR_DMA) && ((d_TxLen & 1) == 0));
#endif

  LOGD("SPI_ReadResp: Packet Size: %d", d_TxLen);

  // Not using periodical timer to prevent Thread re-entry/Racing condition.
  if (!TimerHandlerStart(&gs_RxTimerHdrID, 0, 1, 0, 0)) {
    LOGE("SPI_ReadResp: RxTimerHdr Fail: gb_TxRxState: %d", gb_TxRxState);
    gb_Error = K_SPI_ERR_RX_TIMER_FAIL;
    while (pthread_mutex_trylock(&gs_TxRxStateMutex));
    gb_TxRxState = K_SPI_TXRX_STATE_IDLE;
    pthread_mutex_unlock(&gs_TxRxStateMutex);
    return -1;
  }

  while (gb_TxRxState == K_SPI_TXRX_STATE_RECV_DATA) {};

#ifdef DEBUG_PACK_RESPONSE_DATA
  if (gb_isPackRespData >= K_SPIPackRespData_IgnoreErr) {

#ifdef DEBUG_CONFIG_SW_PLATFORM
    if (gb_isFibocom_EVK)
      memcpy((char *)ap_RxBuf, (char *)sp_SPIReadRespCmd->bs_Data, ad_RxLen);
    else
#endif
    memcpy((char *)ap_RxBuf, (char *)&sp_SPIReadRespCmd->w_DummyPostamble, ad_RxLen);

    while (pthread_mutex_trylock(&gs_TxRxStateMutex));
    gb_TxRxState = K_SPI_TXRX_STATE_IDLE;
    pthread_mutex_unlock(&gs_TxRxStateMutex);
    return ad_RxLen;
  }
#endif

  if (gb_TxRxState == K_SPI_TXRX_STATE_FAIL) {
    // Fail - Return the Received SC Message Header Data. Try to recover the Class ID and Function ID and return it back to SCF.
    memcpy((char *)ap_RxBuf, (char *)sp_SPIReadResp->bs_Data, K_SC_MESSAGE_HDR_SIZE);
    while (pthread_mutex_trylock(&gs_TxRxStateMutex));
    gb_TxRxState = K_SPI_TXRX_STATE_IDLE;
    pthread_mutex_unlock(&gs_TxRxStateMutex);
    return -1;
  }

  // Success - Return the Received Data.
  memcpy((char *)ap_RxBuf, (char *)sp_SPIReadResp->bs_Data, ad_RxLen);

#ifdef DEBUG_ENABLE_TIMING
  if (gettimeofday(&gs_finish, NULL) == -1)
    LOGE_TIME("SPI_ReadResp: Get Time Err.");
  else
    LOGD_TIME("SPI_ReadResp: Completed: Elapsed: %.6f ms", (((gs_finish.tv_sec - gs_start.tv_sec) * 1000.0) + ((gs_finish.tv_usec - gs_start.tv_usec) / 1000.0)));
#endif

  gb_Error = K_SPI_ERR_NONE;
  while (pthread_mutex_trylock(&gs_TxRxStateMutex));
  gb_TxRxState = K_SPI_TXRX_STATE_IDLE;
  pthread_mutex_unlock(&gs_TxRxStateMutex);

  return ad_RxLen;
}


/**************************************************************************************************/
// SPI_Read
// SPI Read the Response Length and Data from the SC.
// ap_RxBuf = Pointer to the Data buffer to be stored the Response Data.
// Return : -1 -> Get Response Data Fail.
//        : Other's Value -> Length of the Response Data be read from the Secure CPU. SPI Read Success.
//        :               -> Response Data be placed at the buffer which be pointed by "ap_RxBuf".
/**************************************************************************************************/
ssize_t SPI_Read(uint8_t const *ap_RxBuf)
{
  ssize_t d_rtn;
  size_t d_SPITxRxState;

SPI_READ_RETRY:
  if ((d_SPITxRxState = isSPITxRxReady()) != K_SPI_TXRX_READY) { // Prevent Rx IRQ faster than the Tx(Write Command) state be reset.
    if (d_SPITxRxState == K_SPI_TXRX_BUSY)
      goto SPI_READ_RETRY;
    else {
      LOGE("SPI_Read: Wake Up SC Error! Error: %d", gb_Error);
      return -1;
    }
  }

#ifdef DEBUG_ENABLE_TIMING
  if (gettimeofday(&gs_finish, NULL) == -1)
    LOGE_TIME("SPI_Read: Get Time Err.");
  else
    LOGD_TIME("SPI_Read: Start...: Elapsed: %.6f ms", (((gs_finish.tv_sec - gs_start.tv_sec) * 1000.0) + ((gs_finish.tv_usec - gs_start.tv_usec) / 1000.0)));
#endif

  if ((d_rtn = SPI_ReadRespLen()) >= 0)
    return (SPI_ReadResp(ap_RxBuf, d_rtn));
  else {
    // Read Length Fail -> Unknown Class/Function ID -> Set it to zero and return it back to SCF.
    memset((char *)ap_RxBuf, 0, 2);
  }

  return -1;
}


/*******************************************************************************/
// SPI_GetError
// Get the latest SPI Tx/Rx Error Status.
// Return : SPI Communication Error Code which is specified in "SPIComm.h".
//        : e.g. K_SPI_ERR_RX_DATA_CRC.
/*******************************************************************************/
size_t SPI_GetError(void)
{
  uint8_t bError = gb_Error;
  return bError;
}

