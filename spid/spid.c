/***************************************************************************************************
 *  vender/spt/spid/spid.c
 *
 *  Spectra Technologies
 *  SPI Daemon for handling the Tx/Rx Data between Android and SC.
 *  Copyright (C) 2018 Harris Lee.
 *
 *  Date : 18 Jan 2018.
 *  Last updated : 16 May 2019.
 *
***************************************************************************************************/
/*==============================================*/
/* Naming conventions                           */
/* ~~~~~~~~~~~~~~~~~~                           */
/*         Class define : Leading C             */
/*        Struct define : Leading S             */
/*               Struct : Leading s             */
/*               Class  : Leading c             */
/*             Constant : Leading K             */
/*      Global Variable : Leading g             */
/*    Function argument : Leading a             */
/*       Local Variable : All lower case        */
/*            Byte size : Leading b             */
/*            Word size : Leading w  (16 bits)  */
/*           DWord size : Leading d  (32 bits)  */
/*          DDWord size : Leading dd (64 bits)  */
/*              Pointer : Leading p             */
/*==============================================*/

/**************************************************************************************************/
// Headers
/**************************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include <time.h>
#include <signal.h>
#include <pthread.h>

#include <fcntl.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <sys/un.h>
#include <cutils/sockets.h>

#include <utils/Log.h>

#include <poll.h>

#include "../common/dbg.h"
#include "../common/Util.h"
#include "../common/gpio.h"
#include "../common/SCF.h"
#include "../common/ClassFnID.h"

#include "spid.h"
#include "SPIComm.h"


/**************************************************************************************************/
// Function/Config Enable/Disable Switches
/**************************************************************************************************/
/*********************************************************************************/
// K_SPID_DEBOUNCE_ENABLE
// Define it for Enable the GPIO Pin Debounce Detection.
/*********************************************************************************/
//#define K_SPID_DEBOUNCE_ENABLE


/*********************************************************************************/
// K_SPID_ERR_EXIT_DEADLOOP
// Define it for Enable the GPIOD get into the Dead Loop when Error is occurred in Initialization.
/*********************************************************************************/
#define K_SPID_ERR_EXIT_DEADLOOP


/**************************************************************************************************/
// Common Definition
/**************************************************************************************************/
#undef LOG_TAG
#define LOG_TAG                                 "spid"

// ID for Socket which used to be connected to the SPI Daemon.
enum {
  K_SPID_SOCKET_ID_GPIOD,                                 // Socket for GPIOD Connetcion.
  K_SPID_SOCKET_ID_START,                                 // Starting Socket ID for Client Connection.
  K_SPID_SOCKET_ID_SCFSERVICE = K_SPID_SOCKET_ID_START,   // Socket for SCF Service Connetcion.
  K_SPID_MAX_NO_OF_SOCKET                                 // Max Number of Socket used to connecting to the SPI Daemon.
};

#define K_POLL_TIMEOUT                          -1      // Infinite Timeout.

#ifdef K_SPID_DEBOUNCE_ENABLE
// GPIO Interrupt Debounce Time.
#define K_GPIO_DB_GAP_SEC                       0       // in Sec, 0s.
#define K_GPIO_DB_GAP_MS                        0       // in ms, 0ms.  // 50ms for Hand Trigger debug.
#define K_GPIO_DB_GAP_NS                        (500 + (K_GPIO_DB_GAP_MS * 1000000))    // in ns, 500ns.
#endif

// Socket Name for connect to the GPIO Daemon
#define K_SPID_GPIOD_SOCKET_NAME                "gpiod"

// Timer Interval to Make connection to the GPIOD.
#define K_SPID_CONNECT_GPIOD_SEC                0    // in Sec, 0s.
#define K_SPID_CONNECT_GPIOD_MS                 500  // in ms, 0.5s.
#define K_SPID_CONNECT_GPIOD_NS                 (K_SPID_CONNECT_GPIOD_MS * 1000000)


/**************************************************************************************************/
// GPIO I/O Port Config
/**************************************************************************************************/
// GPIO Input Pin's ID
enum {
  K_GPIO_IN_ID_SP_SPI_RX,                       // GPIO ID for Secure CPU SPI Rx Interrupt.
  K_SPID_MAX_NO_OF_GPIO_IN_ID                   // Total Number of GPIO Input Pins.
};

/**************************************************************************************************/
/*************************************************
 * GPIO Pins for Input
 ************************************************/
#define K_GPIO_IN_PIN_SP_RX                     110     // GPIO Pin (Input, Base: PinCtl IO) for SPI Rx Interrupt(A10 Platform).

#ifdef DEBUG_CONFIG_SW_PLATFORM
extern uint8_t gb_isFibocom_EVK;
#define K_GPIO_IN_PIN_FIBOCOM_SP_RX             98      // GPIO Pin (Input, Base: PinCtl IO) for SPI Rx Interrupt(Fibocom Platform).
#endif

/**************************************************************************************************/
/*************************************************
 * GPIO I/O Port Structure
 ************************************************/
typedef struct {
  uint16_t w_GpioNo;                            // GPIO Pin Number.
  uint16_t w_GpioBaseOffset;                    // GPIO Base Offset which be mapped in File System.
  uint16_t w_GpioTrigState;                     // GPIO Trigger State. ("K_GPIO_STATE_LOW", "K_GPIO_STATE_HIGH")
  uint8_t b_GpioEdge;                           // GPIO Edge. ("K_GPIO_EDGE_NONE", "K_GPIO_EDGE_RISING", "K_GPIO_EDGE_FALLING" or "K_GPIO_EDGE_BOTH")
} TS_SPID_GPIO_IN_PORT;


// Total Socket Buffer Size with all Header/Overhead in Byte
#define K_SPID_SOCKET_BUF_SIZE_WITH_OVERHEAD                (K_SPI_MAX_TX_RX_SIZE + TS_REPLY_SCF_SOCKET_HEADER_SIZE)

// Total Socket Buffer Size with all Header/Overhead in Word(16 Bits)
#define K_SPID_SOCKET_BUF_SIZE_WITH_OVERHEAD_WORD           ((unsigned int)((K_SPID_SOCKET_BUF_SIZE_WITH_OVERHEAD + 1) / 2))



/**************************************************************************************************/
// Global
/**************************************************************************************************/
// Thread ID.
static pthread_t gd_SPIDPollGPIOID;
static pthread_t gd_SPIDReadThread;


// Common Buffers
// SPID Socket Buffer for SPID to SCF. (i.e. Tx Buffer)
// uint16_t make sure buffer alignd in Word(16Bits) Starting Address. i.e. Address in Multiple of 2.
static uint16_t gbs_SPIDToSCFBuf[K_SPID_SOCKET_BUF_SIZE_WITH_OVERHEAD_WORD];
// SPID Socket Buffer for SC to SPID. (i.e. Rx Buffer)
static uint16_t gbs_SCToSPIDBuf[K_SPID_SOCKET_BUF_SIZE_WITH_OVERHEAD_WORD];

// Socket Descriptor for Connection.
static int gd_Sd[K_SPID_MAX_NO_OF_SOCKET];


/*************************************************
 * GPIO Input Pins Config
 ************************************************/
static TS_SPID_GPIO_IN_PORT gs_GpioInPort[K_SPID_MAX_NO_OF_GPIO_IN_ID] = {
  {K_GPIO_IN_PIN_SP_RX, K_SYSFS_GPIO_BASE_OFFSET_PINCTL, K_GPIO_STATE_HIGH, K_GPIO_EDGE_RISING}
};

// File Descriptor of GPIO Input Pins.
static int gd_GpioInFd[K_SPID_MAX_NO_OF_GPIO_IN_ID];

#ifdef K_SPID_DEBOUNCE_ENABLE
// GPIO Interrupt Debounce Time.
static const struct timespec K_DebounceRes = { K_GPIO_DB_GAP_SEC, K_GPIO_DB_GAP_NS };
#endif

// Timer Handler ID for SPID Connect to the GPIOD.
static timer_t gs_ConnectGPIODTimerHdrID;

// Store the latest SC Command (i.e. Class ID | Function ID ) which send out by write command.
static uint16_t gw_LastSCCmd;

// SPID Reply To SCF Status Flag and Mutex Lock.
static pthread_mutex_t gs_SPIDToSCFMutex = PTHREAD_MUTEX_INITIALIZER;
static int gd_isSPIDToSCFInProgress = FALSE;

// SPID SPI Rx Buffer Status Flag and Mutex Lock.
static pthread_mutex_t gs_SCRxBufInUseMutex = PTHREAD_MUTEX_INITIALIZER;
static int gd_isSCRxBufInUse = FALSE;

#ifdef DEBUG_ENABLE_TIMING
struct timeval gs_start, gs_finish;
#endif

#ifdef DEBUG_SHOW_MESSG_SPID_SCLOG_COUNT
static int gd_SCLogMesgCnt = 0;
#endif


/**************************************************************************************************/

/**************************************************************************************************/
// SPID_DisableIOChgEnCtlByPhy
// Enable/Disable the GPIO "CHG_EN" be controlled by the USB Phy drivers.
// When Printer is Open, USB drivers cannot control the GPIO "CHG_EN" until the Printer is closed.
// aw_isDisable = TRUE  => Disable USB Phy drivers to control the "CHG_EN".
//              = FALSE => Enable USB Phy drivers to control the "CHG_EN".
// Return : NULL.
/**************************************************************************************************/
static const char Kbp_SysPwrUSBIOChgDisable_Path[] = "/sys/class/power_supply/usb/chgenctl_disable";
static void SPID_DisableIOChgEnCtlByPhy(uint16_t aw_isDisable)
{
  int d_fd;
  char ab_isDisableRead = 0;
  char ab_isDisableSet = (aw_isDisable ? '1' : '0');

  if ((d_fd = open(Kbp_SysPwrUSBIOChgDisable_Path, O_RDWR)) == -1)
    LOGE("SPID_DisableIOChgEnCtlByPhy: Open File Fail! Err: %s", strerror(errno));
  else {
    if (read(d_fd, &ab_isDisableRead, 1) != 1) {
      LOGE("SPID_DisableIOChgEnCtlByPhy: Read File Fail! File is corrupted.");
      // Continuous to repair it...
    }

    if (ab_isDisableRead != ab_isDisableSet) {
      lseek(d_fd, 0, SEEK_SET);
      if ((write(d_fd, &ab_isDisableSet, 1)) == 1) {
        LOGD("SPID_DisableIOChgEnCtlByPhy: Set to: %c.", ab_isDisableSet);
      }
      else
        LOGE("SPID_DisableIOChgEnCtlByPhy: Write File Fail! Err: %s", strerror(errno));
    }
    else {
      LOGD("SPID_DisableIOChgEnCtlByPhy: Flag is same as Previous! No Change: %c.", ab_isDisableSet);
    }
    fsync(d_fd);
    close(d_fd);
  }
}


/**************************************************************************************************/
// SPIDReplyToSCFService
// Send Relpy back to the SCF Service.
// aw_ClassFnIDs = Class and Function IDs of the Reply Message.
// bs_SPIDToSCFBuf = Data Reply Buffer to be sent.
// aw_Length = Length of the Payload in Byte.
// Return : NULL.
/**************************************************************************************************/
static void SPIDReplyToSCFService(uint16_t aw_ClassFnIDs, void *bs_SPIDToSCFBuf, uint16_t aw_Length)
{
  TS_REPLY_SCF_SOCKET_HEADER *sp_SocketReply;
  int d_Sd = gd_Sd[K_SPID_SOCKET_ID_SCFSERVICE];

#if defined (DEBUG_ENABLE_TIMING) || defined (DEBUG_SHOW_MESSG_SPID) || defined (DEBUG_SHOW_MESSG_SPID_SCLOG_COUNT)
  TS_SC_MESSAGE_HEADER *sp_SCMesg = (TS_SC_MESSAGE_HEADER *)&aw_ClassFnIDs;
#endif

SPIDREPLYTOSCFSERVICE_RETRY:
  while (pthread_mutex_trylock(&gs_SPIDToSCFMutex));
  if (gd_isSPIDToSCFInProgress) {
    pthread_mutex_unlock(&gs_SPIDToSCFMutex);
    goto SPIDREPLYTOSCFSERVICE_RETRY;
  }
  gd_isSPIDToSCFInProgress = TRUE;
  pthread_mutex_unlock(&gs_SPIDToSCFMutex);

#ifdef DEBUG_ENABLE_TIMING
  if (gettimeofday(&gs_finish, NULL) == -1)
    LOGE_TIME("SPIDReplyToSCFService: Get Time Err.");
  else
    LOGD_TIME("SPIDReplyToSCFService: Start...: ClassID: 0x%02X. FuncID: 0x%02X. 0x%08X. Length: %d. Elapsed: %.6f ms", sp_SCMesg->b_ClassID, sp_SCMesg->b_FnID, (unsigned int)bs_SPIDToSCFBuf, aw_Length, (((gs_finish.tv_sec - gs_start.tv_sec) * 1000.0) + ((gs_finish.tv_usec - gs_start.tv_usec) / 1000.0)));
#else
  LOGD("SPIDReplyToSCFService:  Start...: ClassID: 0x%02X. FuncID: 0x%02X. Length: %d.", sp_SCMesg->b_ClassID, sp_SCMesg->b_FnID, aw_Length);
#endif  // DEBUG_ENABLE_TIMING

  if (d_Sd >= 0) {
    sp_SocketReply = (TS_REPLY_SCF_SOCKET_HEADER *) bs_SPIDToSCFBuf;
    sp_SocketReply->w_Length = aw_Length;
    sp_SocketReply->w_ClassFnIDs = aw_ClassFnIDs;

    // Send Relpy back to the SCF Service.
#ifndef DEBUG_SHOW_MESSG_SPID
    send(d_Sd, sp_SocketReply, (TS_REPLY_SCF_SOCKET_HEADER_SIZE + aw_Length), 0);
#else
    LogD_HexDump(sp_SocketReply, (TS_REPLY_SCF_SOCKET_HEADER_SIZE + aw_Length), 16, "SPIDToSCF");
    if (send(d_Sd, sp_SocketReply, (TS_REPLY_SCF_SOCKET_HEADER_SIZE + aw_Length), 0) != (TS_REPLY_SCF_SOCKET_HEADER_SIZE + aw_Length)) {
      LOGD("SPIDReplyToSCFService: ClassID: 0x%02X. FuncID: 0x%02X. Fail! Err: %s", sp_SCMesg->b_ClassID, sp_SCMesg->b_FnID, strerror(errno));
    }
    else {
      LOGD("SPIDReplyToSCFService: ClassID: 0x%02X. FuncID: 0x%02X. Success!", sp_SCMesg->b_ClassID, sp_SCMesg->b_FnID);
    }
#endif  // DEBUG_SHOW_MESSG_SPID
  }
  else {
    LOGE("SPIDReplyToSCFService: Socket disconnected. Can't reply response");
  }

  while (pthread_mutex_trylock(&gs_SPIDToSCFMutex));
  gd_isSPIDToSCFInProgress = FALSE;
  pthread_mutex_unlock(&gs_SPIDToSCFMutex);

#ifdef DEBUG_ENABLE_TIMING
  if (gettimeofday(&gs_finish, NULL) == -1)
    LOGE_TIME("SPIDReplyToSCFService: Get Time Err.");
  else
    LOGD_TIME("SPIDReplyToSCFService: Done: ClassID: 0x%02X. FuncID: 0x%02X. Length: %d. Elapsed: %.6f ms", sp_SCMesg->b_ClassID, sp_SCMesg->b_FnID, aw_Length, (((gs_finish.tv_sec - gs_start.tv_sec) * 1000.0) + ((gs_finish.tv_usec - gs_start.tv_usec) / 1000.0)));
#else
  LOGD("SPIDReplyToSCFService: Done: ClassID: 0x%02X. FuncID: 0x%02X. Length: %d.", sp_SCMesg->b_ClassID, sp_SCMesg->b_FnID, aw_Length);
#endif  // DEBUG_ENABLE_TIMING

#ifdef DEBUG_SHOW_MESSG_SPID_SCLOG_COUNT
  if ((sp_SCMesg->b_ClassID == K_AndroidCmdClass) && (sp_SCMesg->b_FnID == K_AndroidFuncSCReset)) {
    gd_SCLogMesgCnt = 0;
    LOGD("SPIDReplyToSCFService: Reset SC Log Mesg Count.");
  }
#endif  // DEBUG_SHOW_MESSG_SPID_SCLOG_COUNT
}


/**************************************************************************************************/
// SPIDReadSCResponse
// Read the SPI Response Data from the SC and Relpy the Response Data back to the SCFService.
// Return : -1 -> Get Response Data Fail.
//        : Other's Value -> Length of the Response Data be read from the Secure CPU. SPI Read Success.
//        :               -> Response Data be placed at the buffer which be pointed by "sp_SCRxBuf".
/**************************************************************************************************/
#ifdef DEBUG_PACK_RESPONSE_DATA
extern uint8_t gb_isPackRespData;
ssize_t SPIDReadSCResponse(void)
#else
static ssize_t SPIDReadSCResponse(void)
#endif
{
  TS_REPLY_SCF_SOCKET_HEADER *sp_SCRxBuf = (TS_REPLY_SCF_SOCKET_HEADER *)gbs_SCToSPIDBuf;
  int d_datalen;

  TS_SC_MESSAGE_HEADER *sp_SCMesg = (TS_SC_MESSAGE_HEADER *)sp_SCRxBuf->bs_Payload;

SPIDREADSCRESPONSE_RETRY:
  while (pthread_mutex_trylock(&gs_SCRxBufInUseMutex));
  if (gd_isSCRxBufInUse) {
    pthread_mutex_unlock(&gs_SCRxBufInUseMutex);
////    LOGI("SPIDReadSCResponse: Busy: Wait...");
    goto SPIDREADSCRESPONSE_RETRY;
  }
  gd_isSCRxBufInUse = TRUE;
  pthread_mutex_unlock(&gs_SCRxBufInUseMutex);

  // Read SPI Response.
  do {
    if ((d_datalen = SPI_Read((uint8_t const *)sp_SCRxBuf->bs_Payload)) == -1) {
      LOGE("SPIDReadSCResponse: SPI_Read Failed: LastSCCmd: 0x%04X. RxSCCmd: 0x%04X. Error Code: %d", gw_LastSCCmd, *((uint16_t *)sp_SCRxBuf->bs_Payload), SPI_GetError());
      // Fail - Return the Received Class ID and Function ID back to SCF. (remark: the Received Class/Function ID may be incorrect.)
      sp_SCRxBuf->w_Type = K_SCF_TYPE_REPLY_FAIL;
      SPIDReplyToSCFService(*((uint16_t *)sp_SCRxBuf->bs_Payload), sp_SCRxBuf, 2);
    }
    else {
      // Relpy the Response back to SCFService.
      if (d_datalen > 0) {
        // When Printer is Open, USB drivers cannot control the GPIO "CHG_EN" until the Printer is closed.
        if ((sp_SCMesg->b_ClassID == K_SysCmdClassLpt) && (sp_SCMesg->b_FnID == K_SysFuncLptOpen))
          if (*((uint16_t *)sp_SCMesg->bs_Payload))   // 0x0001 if printer open successful.
            SPID_DisableIOChgEnCtlByPhy(TRUE);

        sp_SCRxBuf->w_Type = K_SCF_TYPE_REPLY_COMPLETE;
        SPIDReplyToSCFService(*((uint16_t *)sp_SCRxBuf->bs_Payload), sp_SCRxBuf, d_datalen);
      }
      else
        LOGD("SPIDReadSCResponse: No Response relpy to SCF");

#ifdef DEBUG_SHOW_MESSG_SPID_SCLOG_COUNT
      if ((sp_SCMesg->b_ClassID == K_AndroidCmdClass) && (sp_SCMesg->b_FnID == K_AndroidFuncSCMessage))
        gd_SCLogMesgCnt++;
#endif
    }
  } while (gs_GpioInPort[K_GPIO_IN_ID_SP_SPI_RX].w_GpioTrigState == gpio_get_state_fd(gd_GpioInFd[K_GPIO_IN_ID_SP_SPI_RX]));

  while (pthread_mutex_trylock(&gs_SCRxBufInUseMutex));
  gd_isSCRxBufInUse = FALSE;
  pthread_mutex_unlock(&gs_SCRxBufInUseMutex);

#ifdef DEBUG_SHOW_MESSG_SPID_SCLOG_COUNT
  LOGD("SPIDReadSCResponse: ClassID: 0x%02X. FuncID: 0x%02X. SC Log Mesg Count: %d.", sp_SCMesg->b_ClassID, sp_SCMesg->b_FnID, gd_SCLogMesgCnt);
#endif

  return d_datalen;
}


/**************************************************************************************************/
// SPIDReadThread
// Thread to Read the SPI Response Data from the SC and Relpy the Response Data back to the SCFService.
/**************************************************************************************************/
static void *SPIDReadThread()
{
  SPIDReadSCResponse();
  pthread_exit(NULL);
}


/**************************************************************************************************/
// Thread to Poll and Handling the GPIO Input's Pins from the Secure CPU.
/**************************************************************************************************/
static void *SPIDPollGPIO()
{
  static pthread_attr_t s_child_attr;
  struct pollfd s_fdset[K_SPID_MAX_NO_OF_GPIO_IN_ID];
#ifdef DEBUG_SHOW_MESSG_SPID
  size_t d_GPIOState;
#endif
  unsigned int d_GPIOID;
  int d_rc;

#ifdef DEBUG_SHOW_MESSG_SPID
  pid_t d_pid;
  pthread_t d_tid;
  d_pid = getpid();
  d_tid = pthread_self();
  LOGD("SPIDPollGPIO: Started. PID: %u. TID %u(0x%x). gd_SPIDPollGPIOID: (0x%x).", (unsigned int)d_pid, (unsigned int)d_tid, (unsigned int)d_tid, (unsigned int)gd_SPIDPollGPIOID);
#endif

  pthread_attr_init(&s_child_attr);
  pthread_attr_setdetachstate(&s_child_attr, PTHREAD_CREATE_DETACHED);

  // Initialize the GPIO Pin to be Polling.
  for (d_GPIOID = 0; d_GPIOID < K_SPID_MAX_NO_OF_GPIO_IN_ID; d_GPIOID++) {
    s_fdset[d_GPIOID].fd = gd_GpioInFd[d_GPIOID];
    s_fdset[d_GPIOID].events = (POLLPRI | POLLERR);
  }

  while (1) {
    LOGD("SPIDPollGPIO: Polling...");

    if ((d_rc = poll(s_fdset, K_SPID_MAX_NO_OF_GPIO_IN_ID, K_POLL_TIMEOUT)) > 0) {
#ifdef DEBUG_ENABLE_TIMING
      if (gettimeofday(&gs_finish, NULL) == -1)
        LOGE_TIME("SPIDPollGPIO: Get Time Err.");
      else
        LOGD_TIME("SPIDPollGPIO: SPI Rx IRQ Occur...: Elapsed: %.6f ms", (((gs_finish.tv_sec - gs_start.tv_sec) * 1000.0) + ((gs_finish.tv_usec - gs_start.tv_usec) / 1000.0)));
#else
      LOGD("SPIDPollGPIO: SPI Rx IRQ Occur...");
#endif  // DEBUG_ENABLE_TIMING

#ifdef K_SPID_DEBOUNCE_ENABLE
      // Debounce Delay.
      clock_nanosleep(CLOCK_MONOTONIC, 0, &K_DebounceRes, NULL);
#endif

      // Check which GPIO Pins be activated.
      for (d_GPIOID = 0; d_GPIOID < K_SPID_MAX_NO_OF_GPIO_IN_ID; d_GPIOID++) {
        if (s_fdset[d_GPIOID].revents & POLLPRI) {
#ifdef DEBUG_SHOW_MESSG_SPID
          if ((d_GPIOState = gpio_get_state_fd(gd_GpioInFd[d_GPIOID])) == gs_GpioInPort[d_GPIOID].w_GpioTrigState)
            LOGD("SPIDPollGPIO: GPIO %d(%d) IRQ. State = %d. d_rc: %d. revents: 0x%04X", gs_GpioInPort[d_GPIOID].w_GpioNo, gs_GpioInPort[d_GPIOID].w_GpioBaseOffset, d_GPIOState, d_rc, s_fdset[d_GPIOID].revents);
#else
          if (gpio_get_state_fd(gd_GpioInFd[d_GPIOID]) == gs_GpioInPort[d_GPIOID].w_GpioTrigState)
#endif
          {
            // SPI Rx IRQ Detect.
            if (d_GPIOID == K_GPIO_IN_ID_SP_SPI_RX) {
              // Create Thread to Read the SPI Response Data from the SC and Relpy the Response Data back to SCFService.
              // No direct call to SPIDReadSCResponse(), Otherwise, maybe not enough time to Poll the next SPI Rx Interrupt State.
              if ((d_rc = pthread_create(&gd_SPIDReadThread, &s_child_attr, SPIDReadThread, NULL)))
                LOGE("SPIDPollGPIO: SPIDReadThread Create Error: %d.", d_rc);
            }
          }
        }
      }
    }
  }
}


/**************************************************************************************************/
// GPIO Control Functions.
/**************************************************************************************************/
/**************************************************************************************************/
// SPIDGPIOInDisable
// Disable the GPIO Input Pin.
// ad_GPIOConfigID = GPIO Input Pin's ID to be disabled.
//                   i.e. "K_GPIO_IN_ID_SP_SPI_RX", etc...
// Return : None.
/**************************************************************************************************/
static void SPIDGPIOInDisable(unsigned int ad_GPIOConfigID)
{
  gpio_close(gd_GpioInFd[ad_GPIOConfigID]);
  gd_GpioInFd[ad_GPIOConfigID] = -1;
  gpio_set_dir(gs_GpioInPort[ad_GPIOConfigID].w_GpioNo, gs_GpioInPort[ad_GPIOConfigID].w_GpioBaseOffset, K_GPIO_DIR_IN);
  gpio_set_edge(gs_GpioInPort[ad_GPIOConfigID].w_GpioNo, gs_GpioInPort[ad_GPIOConfigID].w_GpioBaseOffset, K_GPIO_EDGE_NONE);
  gpio_unexport(gs_GpioInPort[ad_GPIOConfigID].w_GpioNo, gs_GpioInPort[ad_GPIOConfigID].w_GpioBaseOffset);
}


/**************************************************************************************************/
// SPIDGPIOAllDisable
// Close/Stop/Disable all of the GPIO Setting and GPIO Ports for the GPIO Daemon.
// Return : None.
/**************************************************************************************************/
static void SPIDGPIOAllDisable(void)
{
  unsigned int d_GPIOID;

  // Disable All GPIO Input Pins.
  LOGD("SPIDGPIOAllDisable: Disable Input Ports...");
  for (d_GPIOID = 0; d_GPIOID < K_SPID_MAX_NO_OF_GPIO_IN_ID; d_GPIOID++)
    SPIDGPIOInDisable(d_GPIOID);
}


/**************************************************************************************************/
// SPIDThreadGPIOInit
// Init all of the GPIO Input/Output Pins and Create Thread to handling the GPIO Input for the SPI Daemon.
// Return : TRUE -> Success.
//        : FALSE -> Fail.
/**************************************************************************************************/
static int SPIDThreadGPIOInit(void)
{
  unsigned int d_GPIOID;

#ifdef DEBUG_CONFIG_SW_PLATFORM
    if (gb_isFibocom_EVK)
      gs_GpioInPort[K_GPIO_IN_ID_SP_SPI_RX].w_GpioNo = K_GPIO_IN_PIN_FIBOCOM_SP_RX;
#endif

  // Reset All Input Pins File Descriptor.
  for (d_GPIOID = 0; d_GPIOID < K_SPID_MAX_NO_OF_GPIO_IN_ID; d_GPIOID++)
    gd_GpioInFd[d_GPIOID] = -1;

  // Force to disable all of the GPIO Ports. Otherwise, the GPIO maybe Open error if it has been enabled before.
  SPIDGPIOAllDisable();

  // Init GPIO Input Pins.
  LOGD("SPIDThreadGPIOInit: No. of GPIO Input Pins to be Init: %d.", K_SPID_MAX_NO_OF_GPIO_IN_ID);

  for (d_GPIOID = 0; d_GPIOID < K_SPID_MAX_NO_OF_GPIO_IN_ID; d_GPIOID++) {
    if (!gpio_export(gs_GpioInPort[d_GPIOID].w_GpioNo, gs_GpioInPort[d_GPIOID].w_GpioBaseOffset))
      goto THREADGPIO_INIT_IN_ERROR;

    if (!gpio_set_dir(gs_GpioInPort[d_GPIOID].w_GpioNo, gs_GpioInPort[d_GPIOID].w_GpioBaseOffset, K_GPIO_DIR_IN))
      goto THREADGPIO_INIT_IN_ERROR;

    if (!gpio_set_edge(gs_GpioInPort[d_GPIOID].w_GpioNo, gs_GpioInPort[d_GPIOID].w_GpioBaseOffset, gs_GpioInPort[d_GPIOID].b_GpioEdge))
      goto THREADGPIO_INIT_IN_ERROR;

    if ((gd_GpioInFd[d_GPIOID] = gpio_open(gs_GpioInPort[d_GPIOID].w_GpioNo, gs_GpioInPort[d_GPIOID].w_GpioBaseOffset)) == -1)
      goto THREADGPIO_INIT_IN_ERROR;

    gpio_get_state_fd(gd_GpioInFd[d_GPIOID]);
  }

  // Create Thread to Poll the GPIO Input Pins.
  if (pthread_create(&gd_SPIDPollGPIOID, NULL, SPIDPollGPIO, NULL)) {
    LOGE("SPIDThreadGPIOInit: SPIDPollGPIO Create Error.");
    goto THREADGPIO_INIT_IN_ERROR;
  }
  pthread_detach(gd_SPIDPollGPIOID);

  LOGD("SPIDThreadGPIOInit: Success.");
  LOGD("SPIDThreadGPIOInit: Socket Buffer Size in Byte: %d (must >= %d).", sizeof(gbs_SPIDToSCFBuf), K_SPID_SOCKET_BUF_SIZE_WITH_OVERHEAD);
  LOGD("SPIDThreadGPIOInit: Socket Buffer Starting Address: 0x%08X.", (unsigned int)&gbs_SPIDToSCFBuf);

  return TRUE;

THREADGPIO_INIT_IN_ERROR:
  LOGE("SPIDThreadGPIOInit: Init In Failed. GPIO %d(%d).", gs_GpioInPort[d_GPIOID].w_GpioNo, gs_GpioInPort[d_GPIOID].w_GpioBaseOffset);
  // Disable All GPIO Input Pins.
  for (int d_id = d_GPIOID; d_id >= 0; d_id--)
    SPIDGPIOInDisable(d_id);

  return FALSE;
}


/**************************************************************************************************/
// SPIDConnectGPIODHandler
// Handler to make a Socket Connection to the GPIO Daemon.
/**************************************************************************************************/
static void SPIDConnectGPIODHandler()   // union sigval as_val)   // Remove Warning as not used.
{
  int d_Sd;

  // Create GPIOD Socket.
  if ((d_Sd = socket_local_client(K_SPID_GPIOD_SOCKET_NAME, ANDROID_SOCKET_NAMESPACE_RESERVED, SOCK_STREAM)) < 0 ) {
    LOGE("SPIDConnectGPIOD: Failed to get socket %s. Err: %s (%d).", K_SPID_GPIOD_SOCKET_NAME, strerror(errno), errno);
    // Start Timer to reconnecting to the GPIOD.
    TimerHandlerStart(&gs_ConnectGPIODTimerHdrID, K_SPID_CONNECT_GPIOD_SEC, K_SPID_CONNECT_GPIOD_NS, 0, 0);
    return;
  }

  gd_Sd[K_SPID_SOCKET_ID_GPIOD] = d_Sd;
  LOGD("SPIDConnectGPIOD: Connect to GPIOD Success: fd: %d.", d_Sd);
}


/**************************************************************************************************/
// main
// SPI Daemon for handling the Tx/Rx Data between Android and SC.
// Return : -1 -> Something Error. (will be restart by Android Daemon Manager).
//        : Normally, this Daemon never exit.
/**************************************************************************************************/
int main()
{
  TS_REPLY_SCF_SOCKET_HEADER *sp_SocketReply = (TS_REPLY_SCF_SOCKET_HEADER *) gbs_SPIDToSCFBuf;
  uint8_t bs_SocketRxBuf[K_SPID_SOCKET_BUF_SIZE_WITH_OVERHEAD];  // Socket Data Rx Buffer from SCF.
  TS_REPLY_SCF_SOCKET_HEADER *sp_SocketReplyRxBuf = (TS_REPLY_SCF_SOCKET_HEADER *) bs_SocketRxBuf;
  TS_SC_MESSAGE_HEADER *sp_SCMesg = (TS_SC_MESSAGE_HEADER *) bs_SocketRxBuf;
  TS_SC_MESSAGE_HEADER *sp_SCMesgRxBuf = (TS_SC_MESSAGE_HEADER *) sp_SocketReplyRxBuf->bs_Payload;
  fd_set s_readfds;                       // Set of socket descriptors
  struct sockaddr_in s_ClientSockAddIn;
  int d_ClientSockAddInSize;
  int d_ListenSd, d_NewSd;
  int d_Sd, d_MaxSd;
  int d_SocketID, d_datalen;
#ifdef DEBUG_PACK_RESPONSE_DATA
  uint8_t b_isPackRespData_Old;
#endif

  LOGD("SPID: Main Starting...");

#ifdef DEBUG_USE_PERSIST_DEBUG_ON_FLAG
  InitDebugOn();
#endif // #ifdef DEBUG_USE_PERSIST_DEBUG_ON_FLAG

  // Initialise all Socket Descriptor to -1 to indicate that it is not use/active.
  for (d_SocketID = 0; d_SocketID < K_SPID_MAX_NO_OF_SOCKET; d_SocketID++)
    gd_Sd[d_SocketID] = -1;

  if (!SPI_Open())
    goto SPID_SPI_OPEN_ERR_EXIT;

  if (!SPIDThreadGPIOInit())
    goto SPID_GPIO_ERR_EXIT;

  // Create Timer to connecting to the GPIOD.
  if (!CreateTimerHandler(SPIDConnectGPIODHandler, &gs_ConnectGPIODTimerHdrID, 255)) {
    LOGD("SPID: Create ConnectGPIOD Timer Fail!");
    goto SPID_TIMER_ERR_EXIT;
  }
  // Start Timer to connecting to the GPIOD.
  if (!TimerHandlerStart(&gs_ConnectGPIODTimerHdrID, K_SPID_CONNECT_GPIOD_SEC, K_SPID_CONNECT_GPIOD_NS, 0, 0))
    goto SPID_ERR_EXIT;


  // Create a Master Socket.
  if ((d_ListenSd = android_get_control_socket(K_SPID_SOCKET_NAME)) < 0 ) {
    LOGE("SPID: Failed to get socket %s. Err: %s (%d).", K_SPID_SOCKET_NAME, strerror(errno), errno);
    goto SPID_ERR_EXIT;
  }
  fcntl(d_ListenSd, F_SETFD, FD_CLOEXEC);

  // Set Pending connection and Listen for the Client Socket.
  if (listen(d_ListenSd, (K_SPID_MAX_NO_OF_SOCKET - K_SPID_SOCKET_ID_START)) < 0) {
    LOGE("SPID: Listen failed! %s", strerror(errno));
    goto SPID_ERR_EXIT;
  }

  // Accept the Incoming Connection.
  LOGD("SPID: Waiting for Client connection...");

  while (TRUE) {
    // Clear the Socket Set.
    FD_ZERO(&s_readfds);

    // Add Listen Socket to Set.
    FD_SET(d_ListenSd, &s_readfds);
    d_MaxSd = d_ListenSd;

    // Add Client Sockets to Set.
    for (d_SocketID = 0; d_SocketID < K_SPID_MAX_NO_OF_SOCKET; d_SocketID++) {
      // If Valid Socket Descriptor then add it to Read List.
      if ((d_Sd = gd_Sd[d_SocketID]) >= 0) {
        FD_SET(d_Sd, &s_readfds);
        // Get the Highest File Descriptor Number. (Parameter for the Select Function)
        if(d_Sd > d_MaxSd)
          d_MaxSd = d_Sd;
      }
    }

    // Wait for any activity on one of the Client Sockets, Timeout is NULL so it wait indefinitely.
    if (select(d_MaxSd + 1, &s_readfds, NULL, NULL, NULL) >= 0) {
      //If something happened on the Listen Socket, then its an Incoming Connection
      if (FD_ISSET(d_ListenSd, &s_readfds)) {
        d_ClientSockAddInSize = sizeof(s_ClientSockAddIn);
        if ((d_NewSd = accept(d_ListenSd, (struct sockaddr *) &s_ClientSockAddIn, (socklen_t *) &d_ClientSockAddInSize)) < 0) {
          LOGE("SPID: Accept Failed! %s", strerror(errno));
          continue;   //goto SPID_ERR_EXIT;
        }

        // Inform user of Socket Number - used in Send and Receive Commands
        LOGD("SPID: Accept New connection. Socket fd: %d. IP: %s , Port: %d.", d_NewSd, inet_ntoa(s_ClientSockAddIn.sin_addr), ntohs(s_ClientSockAddIn.sin_port));

        // Add New Client Socket to Array of Socket List.
        for (d_SocketID = K_SPID_SOCKET_ID_START; d_SocketID < K_SPID_MAX_NO_OF_SOCKET; d_SocketID++) {
          if (gd_Sd[d_SocketID] == -1) {      // If position is empty
            gd_Sd[d_SocketID] = d_NewSd;
            LOGD("SPID: Adding Client Socket to list. Socket %d to Pos %d." , d_NewSd, d_SocketID);
            break;
          }
        }
        if (d_SocketID ==  K_SPID_MAX_NO_OF_SOCKET) {
          LOGD("SPID: No more Client Socket is available! Socket %d." , d_NewSd);
        }
      }

      // Else its some IO operation on some other socket.
      for (d_SocketID = 0; d_SocketID < K_SPID_MAX_NO_OF_SOCKET; d_SocketID++) {
        d_Sd = gd_Sd[d_SocketID];

        if (FD_ISSET(d_Sd, &s_readfds)) {
          // Check if it was for closing, and also read the Incoming Message
          if ((d_datalen = read(d_Sd, sp_SCMesg, K_SPID_SOCKET_BUF_SIZE_WITH_OVERHEAD)) == 0) {
            // Somebody disconnected, get his details and print
            getpeername(d_Sd, (struct sockaddr*) &s_ClientSockAddIn, (socklen_t*) &d_ClientSockAddInSize);
            LOGD("SPID: Host disconnected. d_SocketID: %d. d_Sd: %d. IP: %s. Port: %d", d_SocketID, d_Sd, inet_ntoa(s_ClientSockAddIn.sin_addr), ntohs(s_ClientSockAddIn.sin_port));

            // Close the Client Socket and Mark as 0 in list for reuse.
            close(d_Sd);
            gd_Sd[d_SocketID] = -1;
            if (d_SocketID == K_SPID_SOCKET_ID_GPIOD) {
              // Start Timer to reconnecting to the GPIOD.
              if (!TimerHandlerStart(&gs_ConnectGPIODTimerHdrID, K_SPID_CONNECT_GPIOD_SEC, K_SPID_CONNECT_GPIOD_NS, 0, 0))
                goto SPID_ERR_EXIT;
            }
          }
          else {
            switch (d_SocketID) {
              case K_SPID_SOCKET_ID_GPIOD:
#ifdef DEBUG_ENABLE_TIMING
                if (gettimeofday(&gs_finish, NULL) == -1)
                  LOGE_TIME("SPID: Get Time Err.");
                else
                  LOGD_TIME("SPID: Receive from GPIOD... ClassID: 0x%02X. FuncID: 0x%02X.  Elapsed: %.6f ms", sp_SCMesgRxBuf->b_ClassID, sp_SCMesgRxBuf->b_FnID, (((gs_finish.tv_sec - gs_start.tv_sec) * 1000.0) + ((gs_finish.tv_usec - gs_start.tv_usec) / 1000.0)));
#else
                LOGD("SPID: Receive from GPIOD... ClassID: 0x%02X. FuncID: 0x%02X.", sp_SCMesgRxBuf->b_ClassID, sp_SCMesgRxBuf->b_FnID);
#endif  // DEBUG_ENABLE_TIMING

                  // Reset the SPI Starting Code Sequence Bit to sync with the SC side when SC is ever Reset.
                  if (sp_SCMesgRxBuf->b_FnID == K_AndroidFuncSCStatus)
                    if ((sp_SCMesgRxBuf->bs_Payload[1] == K_GPIOD_SC_STATUS_BLDR_RUNNING) && (sp_SocketReplyRxBuf->w_Type == K_SCF_TYPE_REPLY_COMPLETE))  // SC is ever reset ?
                      SPI_ResetSequenceBit();

#ifdef DEBUG_PACK_RESPONSE_DATA     // Temp for debug.
                if (gb_isPackRespData == K_SPIPackRespData_InputData)   // Relpy the Response Data which same as the Input Socket Data. No need reply result from GPIOD.
                  break;
#endif  // DEBUG_PACK_RESPONSE_DATA
                SPIDReplyToSCFService(*((uint16_t *)sp_SCMesgRxBuf), sp_SocketReplyRxBuf, (d_datalen - TS_REPLY_SCF_SOCKET_HEADER_SIZE));
                break;

              case K_SPID_SOCKET_ID_SCFSERVICE:
#ifdef DEBUG_ENABLE_TIMING
                // Elapsed Time count from the Time that SPID receive command from SCF.
                if (gettimeofday(&gs_start, NULL) == -1)
                  LOGE_TIME("SPID: Get Time Err.");
                else
                  LOGD_TIME("SPID: Cmd from SCF: ClassID: 0x%02X. FuncID: 0x%02X. Elapsed: 0 ms", sp_SCMesg->b_ClassID, sp_SCMesg->b_FnID);
#endif  // DEBUG_ENABLE_TIMING

                gw_LastSCCmd = *((uint16_t *)sp_SCMesg);
                sp_SocketReply->w_Type = K_SCF_TYPE_SEND_COMPLETE;
                sp_SocketReply->bs_Payload[0] = K_ACK;
                if (sp_SCMesg->b_ClassID == K_AndroidCmdClass) {   // Android System Command.
#ifdef DEBUG_USE_PERSIST_DEBUG_ON_FLAG
                  // Reset SC Command to Initizlize the Debug ON Flag. i.e. So that No need to kill the SPID and GPIOD.
                  if (sp_SCMesg->b_FnID == K_AndroidFuncSCReset)
                    InitDebugOn();
#endif // #ifdef DEBUG_USE_PERSIST_DEBUG_ON_FLAG

#ifdef DEBUG_PACK_RESPONSE_DATA
                  if (sp_SCMesg->b_FnID == K_AndroidFuncSCSPILoopTest) {  // Android Command - SPIPackRespData_RndWriteCRC. Packing Random Data and Loop Count "K_SPIPackRespData_RndCRC_LoopCnt".
                    b_isPackRespData_Old = gb_isPackRespData;
                    gb_isPackRespData = K_SPIPackRespData_RndWriteCRC;
                    goto SPIPACKRESPDATA_RNDWRITECRC;
                  }
#endif

#ifdef DEBUG_SHOW_MESSG_SPID
                  LogD_HexDump(sp_SCMesg, d_datalen, 16, "SPIDToGPIOD");
#endif // DEBUG_SHOW_MESSG_SPID
                  if (send(gd_Sd[K_SPID_SOCKET_ID_GPIOD], sp_SCMesg, d_datalen, 0) != d_datalen) {
                    LOGD("SPID: Send Cmd to GPIOD Fail!: ClassID: 0x%02X. FuncID: 0x%02X.", sp_SCMesg->b_ClassID, sp_SCMesg->b_FnID);
                    sp_SocketReply->w_Type = K_SCF_TYPE_SEND_FAIL;
                    sp_SocketReply->bs_Payload[0] = K_NACK;
                  }
                  else {
#ifdef DEBUG_ENABLE_TIMING
                    if (gettimeofday(&gs_finish, NULL) == -1)
                      LOGE_TIME("SPID: Get Time Err.");
                    else
                      LOGD_TIME("SPID: Send Cmd to GPIOD Success!: ClassID: 0x%02X. FuncID: 0x%02X. Elapsed: %.6f ms", sp_SCMesg->b_ClassID, sp_SCMesg->b_FnID, (((gs_finish.tv_sec - gs_start.tv_sec) * 1000.0) + ((gs_finish.tv_usec - gs_start.tv_usec) / 1000.0)));
#else
                    LOGD("SPID: Send Cmd to GPIOD Success!: ClassID: 0x%02X. FuncID: 0x%02X.", sp_SCMesg->b_ClassID, sp_SCMesg->b_FnID);
#endif  // DEBUG_ENABLE_TIMING
                  }
                }
                else {  // SC Command.
#ifdef DEBUG_PACK_RESPONSE_DATA     // Temp for debug.
SPIPACKRESPDATA_RNDWRITECRC:
                  if (gb_isPackRespData == K_SPIPackRespData_RndWriteCRC) {
                    int d_cnt;
#ifdef DEBUG_ENABLE_TIMING
                    struct timeval s_start, s_finish;
                    if (gettimeofday(&s_start, NULL) == -1)
                      LOGE_TIME("SPID: Get Time Err.");
#endif  // DEBUG_ENABLE_TIMING

                    d_datalen = K_SPI_MAX_TX_RX_SIZE;
                    for (d_cnt = 0; d_cnt < K_SPIPackRespData_RndCRC_LoopCnt; d_cnt ++) {
                      LOGD("SPID: Packing %d bytes Random Write Cmd. Packet Size: %d.", (K_SPI_MAX_TX_RX_SIZE - K_SC_MESSAGE_HDR_SIZE), d_datalen);
                      for (unsigned int i = 0; i < (K_SPI_MAX_TX_RX_SIZE - K_SC_MESSAGE_HDR_SIZE); i++)
                        sp_SCMesg->bs_Payload[i] = rand();

                      if (SPI_Write((uint8_t const *)sp_SCMesg, d_datalen) != -1)
                        LOGD("SPID: SPI_WriteCmd Success!");
                      else {
                        LOGE("SPID: SPI_WriteCmd Failed: Error Code: %d", SPI_GetError());
                        sp_SocketReply->w_Type = K_SCF_TYPE_SEND_FAIL;
                        if ((sp_SocketReply->bs_Payload[0] = SPI_GetError()) < K_SPI_ERR_TX_TIMEOUT_WAIT)
                          sp_SocketReply->bs_Payload[0] = K_NACK;
                        break;
                      }
                    }

                    if (sp_SCMesg->b_FnID == K_AndroidFuncSCSPILoopTest)  // Android Command - K_AndroidFuncSCSPILoopTest -> Restore Previous "gb_isPackRespData" Value.
                      gb_isPackRespData = b_isPackRespData_Old;

#ifdef DEBUG_ENABLE_TIMING
                    if (gettimeofday(&s_finish, NULL) == -1)
                      LOGE_TIME("SPID: Get Time Err.");
                    else
                      LOGD_TIME("SPID: Random Write Cmd: Loop stop at: %d. Elapsed: %.6f ms", d_cnt, (((s_finish.tv_sec - s_start.tv_sec) * 1000.0) + ((s_finish.tv_usec - s_start.tv_usec) / 1000.0)));
#else
                      LOGD("SPID: Random Write Cmd: Loop stop at: %d.", d_cnt);
#endif  // DEBUG_ENABLE_TIMING
                  }
                  else
#endif  // DEBUG_PACK_RESPONSE_DATA
                  {
                    if (SPI_Write((uint8_t const *)sp_SCMesg, d_datalen) != -1)
                      LOGD("SPID: SPI_WriteCmd Success: ClassID: 0x%02X. FuncID: 0x%02X.", sp_SCMesg->b_ClassID, sp_SCMesg->b_FnID);
                    else {
                      LOGE("SPID: SPI_WriteCmd Failed: ClassID: 0x%02X. FuncID: 0x%02X: Error Code: %d", sp_SCMesg->b_ClassID, sp_SCMesg->b_FnID, SPI_GetError());
                      sp_SocketReply->w_Type = K_SCF_TYPE_SEND_FAIL;
                      if ((sp_SocketReply->bs_Payload[0] = SPI_GetError()) < K_SPI_ERR_TX_TIMEOUT_WAIT)
                        sp_SocketReply->bs_Payload[0] = K_NACK;
                    }
                  }
                }

                // When Printer is Close, Restore the USB drivers to control the GPIO "CHG_EN".
                if ((sp_SCMesg->b_ClassID == K_SysCmdClassLpt) && (sp_SCMesg->b_FnID == K_SysFuncLptClose))
                  SPID_DisableIOChgEnCtlByPhy(FALSE);

                // Reply Send Status back to SCF Service.
                SPIDReplyToSCFService(gw_LastSCCmd, sp_SocketReply, 1);

#ifdef DEBUG_PACK_RESPONSE_DATA     // Temp for debug.
                if (gb_isPackRespData == K_SPIPackRespData_InputData) {   // Relpy the Response Data which same as the Input Socket Data.
                  // Relpy Response back to SCFService.
                  if (d_datalen > 0) {
                    memcpy(sp_SocketReply->bs_Payload, sp_SCMesg, d_datalen);
                    sp_SocketReply->w_Length = d_datalen;
                    SPIDReplyToSCFService(gw_LastSCCmd, sp_SocketReply, d_datalen);
                  }
                }
                else {
                  if ((gb_isPackRespData) && (sp_SCMesg->b_ClassID != K_AndroidCmdClass))  // SC Command Class ?
                      SPIDReadSCResponse();   // Read the Simulated SC SPI Response Data and Relpy the Response Data back to SCFService.
                }
#endif
               break;
            }
          }
        }
      }
    }
#ifdef DEBUG_SHOW_MESSG_SPID
    else {
      if (errno == EINTR) {
        LOGE("GPIOD: Select Interrupt Exit.");
      }
      else {
        LOGE("SPID: Select Error! %s", strerror(errno));
      }
    }
#endif
  }

SPID_ERR_EXIT:
  TimerHandlerDelete(&gs_ConnectGPIODTimerHdrID);

SPID_TIMER_ERR_EXIT:
  SPIDGPIOAllDisable();

SPID_GPIO_ERR_EXIT:
  SPI_Close();

SPID_SPI_OPEN_ERR_EXIT:
  LOGE("SPID: SPI Daemon Deinitialize...");

  for (d_SocketID = 0; d_SocketID < K_SPID_MAX_NO_OF_SOCKET; d_SocketID++)
    if (gd_Sd[d_SocketID] >= 0)
      close(gd_Sd[d_SocketID]);

  if (d_ListenSd)
    close(d_ListenSd);

  pthread_mutex_destroy(&gs_SPIDToSCFMutex);
  pthread_mutex_destroy(&gs_SCRxBufInUseMutex);

  // SPID Start Error. Set Android Tamper Flag.
  SetAndroidTamperFlag(TRUE);

#ifdef K_SPID_ERR_EXIT_DEADLOOP
  // Dead loop for keep the SPID running. Otherwise SPID will be reloaded again and again, that casue the System can't be loaded.
  while(TRUE) {
    sleep(10);  //3600);
    LOGE("SPID: Dead Loop!");
  }
#endif

  LOGE("SPID: SPI Daemon Exit!");
  exit(EXIT_SUCCESS);
}

