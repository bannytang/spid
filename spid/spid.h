/***************************************************************************************************
 *  vender/spt/spid/spid.h
 *
 *  Spectra Technologies
 *  SPI Daemon for handling the Tx/Rx Data between Android and SC.
 *  Copyright (C) 2018 Harris Lee.
 *
 *  Date : 9 Mar 2018.
 *  Last updated : 13 Jul 2018.
 *
***************************************************************************************************/
/*==============================================*/
/* Naming conventions                           */
/* ~~~~~~~~~~~~~~~~~~                           */
/*         Class define : Leading C             */
/*        Struct define : Leading S             */
/*               Struct : Leading s             */
/*               Class  : Leading c             */
/*             Constant : Leading K             */
/*      Global Variable : Leading g             */
/*    Function argument : Leading a             */
/*       Local Variable : All lower case        */
/*            Byte size : Leading b             */
/*            Word size : Leading w  (16 bits)  */
/*           DWord size : Leading d  (32 bits)  */
/*          DDWord size : Leading dd (64 bits)  */
/*              Pointer : Leading p             */
/*==============================================*/



#ifndef __SPID_H_
#define __SPID_H_

/**************************************************************************************************/
// Common Definition
/**************************************************************************************************/
// Socket Name for connect to the SPI Daemon
#define K_SPID_SOCKET_NAME                            "spid"



/**************************************************************************************************/
// Functions
/**************************************************************************************************/

#ifdef DEBUG_PACK_RESPONSE_DATA
/**************************************************************************************************/
// SPIDReadSCResponse
// Read the SPI Response Data from the SC and Relpy the Response Data back to SCFService.
// Return : -1 -> Get Response Data Fail.
//        : Other's Value -> Length of the Response Data be read from the Secure CPU. SPI Read Success.
//        :               -> Response Data be placed at the buffer which be pointed by "ap_RxBuf".
/**************************************************************************************************/
extern ssize_t SPIDReadSCResponse(void);
#endif  // DEBUG_PACK_RESPONSE_DATA

#endif  // __SPID_H_


