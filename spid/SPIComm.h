/***************************************************************************************************
 *  vender/spt/spid/spicomm.c
 *
 *  Spectra Technologies
 *  SPI Communication drivers for handling the Tx/Rx Data between Android and SC.
 *  Copyright (C) 2018 Harris Lee.
 *
 *  Date : 22 Jan 2018.
 *  Last updated : 2 Apr 2019.
 *
***************************************************************************************************/
/*==============================================*/
/* Naming conventions                           */
/* ~~~~~~~~~~~~~~~~~~                           */
/*         Class define : Leading C             */
/*        Struct define : Leading S             */
/*               Struct : Leading s             */
/*               Class  : Leading c             */
/*             Constant : Leading K             */
/*      Global Variable : Leading g             */
/*    Function argument : Leading a             */
/*       Local Variable : All lower case        */
/*            Byte size : Leading b             */
/*            Word size : Leading w  (16 bits)  */
/*           DWord size : Leading d  (32 bits)  */
/*          DDWord size : Leading dd (64 bits)  */
/*              Pointer : Leading p             */
/*==============================================*/



#ifndef _HUB_HSM_H_
#define _HUB_HSM_H_

/**************************************************************************************************/
// Common Definition
/**************************************************************************************************/
// Max TX/RX Buffer Size for SPI Communication
#define K_SPI_MAX_TX_RX_SIZE                      1512    // Max Tx/Rx Data Size in Byte. (i.e. Real Tx/Rx Data Size only, not include Header/Overhead.)
                                                          // 1512 (SC Max Buf Size - 1528 bytes, K_T8BufSize)
                                                          // Max 4080 (4080 bcoz Max Linux SPI Drivers Tx/Rx Size is 4096 bytes)

// Acknowledge Code
#define K_ACK                                     0x06
#define K_NACK                                    0x15
#define K_WAIT                                    0x12

// SPI Communication Error Code
#define K_SPI_ERR_NONE                            0x00    // No Error occur.

#define K_SPI_ERR_TX_DATA_SIZE_LIMIT              0x01    // Tx Data Size over the Tx Buffer Limit.
#define K_SPI_ERR_TX_TIMER_FAIL                   0x02    // Tx Handler Timer is failed.
#define K_SPI_ERR_TX_ACK_CODE                     0x03    // Tx Handshaking Acknowledge Code Verify Error.
#define K_SPI_ERR_TX_SC_NACK                      0x04    // NAck Error from Secure CPU. i.e. SC reply K_NACK.
#define K_SPI_ERR_TX_TIMEOUT                      K_NACK  // Tx Retry Fail and Tx Timeout (for all error except WAIT).
#define K_SPI_ERR_TX_TIMEOUT_WAIT                 K_WAIT  // TX Retry Fail caused by WAIT and Tx Timeout (for WAIT Retry only).

#define K_SPI_ERR_RX_DATA_OVER_LIMIT              0x06    // Rx Data Size over the Rx Buffer Limit.
#define K_SPI_ERR_RX_TIMER_FAIL                   0x07    // Rx Handler Timer is failed.
#define K_SPI_ERR_RX_DATA_LEN                     0x08    // Length of the Data Verify Error.
#define K_SPI_ERR_RX_DATA_CRC                     0x09    // CRC of the Data Verify Error.
#define K_SPI_ERR_RX_TIMEOUT                      0x0a    // Rx Retry Fail and Rx Timeout.

#define K_SPI_ERR_SC_WAKE_UP                      0x0b    // Can't Wake Up Secure CPU.



/**************************************************************************************************/
// Functions
/**************************************************************************************************/

/**************************************************************************************************/
// SPI Device Open/Close
/**************************************************************************************************/
/**************************************************************************************************/
// SPI_Open
// Open the SPI device.
// Return : TRUE -> Success.
//        : FALSE -> Fail.
/**************************************************************************************************/
extern size_t SPI_Open(void);

/**************************************************************************************************/
// SPI_Close
// Close the SPI device.
// Return : TRUE -> Success.
//        : FALSE -> Fail.
/**************************************************************************************************/
extern size_t SPI_Close(void);

/**************************************************************************************************/
// SPI_ResetSequenceBit
// Reset the SPI Starting Code Sequence Bit to sync with the SC side when SC is Reset using the SC Reset Command.
/**************************************************************************************************/
extern void SPI_ResetSequenceBit(void);


/**************************************************************************************************/
// SPI Tx/RX Command
/**************************************************************************************************/
/**************************************************************************************************/
// SPI_Write
// SPI Send Write Command Data to SC.
// ap_TxBuf = Pointer to the Data Buffer which the Tx Data to be sent.
// aw_TxLen = Length of the Data to be sent.
// Return : -1 -> SPI Write Fail.
//        : Other's Value -> Length of the Data be sent. SPI Write Success.
/**************************************************************************************************/
extern ssize_t SPI_Write(uint8_t const *ap_TxBuf, uint16_t aw_TxLen);


/**************************************************************************************************/
// SPI_Read
// SPI Read the Response Length and Data from the SC.
// ap_RxBuf = Pointer to the Data buffer to be stored the Response Data.
// Return : -1 -> Get Response Data Fail.
//        : Other's Value -> Length of the Response Data be read from the Secure CPU. SPI Read Success.
//        :               -> Response Data be placed at the buffer which be pointed by "ap_RxBuf".
/**************************************************************************************************/
extern ssize_t SPI_Read(uint8_t const *ap_RxBuf);


/*******************************************************************************/
// SPI_GetError
// Get the latest SPI Tx/Rx Error Status.
// Return : SPI Communication Error Code which is specified in "SPIComm.h".
//        : e.g. K_SPI_ERR_TX_RX_NOT_READY.
/*******************************************************************************/
extern size_t SPI_GetError(void);


#endif



