LOCAL_PATH:= $(call my-dir)

# ==========================================================================
# Build the manager interface to a library
# which can be used by client
# ==========================================================================
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional debug
LOCAL_MODULE := scfmanagerlibrary
LOCAL_PROGUARD_ENABLED := disabled

LOCAL_AIDL_INCLUDES := $(call all-Iaidl-files-under, src/com/spectratech/scfservice)
LOCAL_SRC_FILES := $(call all-java-files-under, src/com/spectratech/scfmanager)
LOCAL_SRC_FILES += $(call all-Iaidl-files-under, src/com/spectratech/scfservice)
LOCAL_SRC_FILES += src/com/spectratech/scfservice/SCSystemCall.java
LOCAL_SRC_FILES += src/com/spectratech/scfservice/PermissionUtils.java

include $(BUILD_JAVA_LIBRARY)

# ==========================================================================

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional debug
LOCAL_MODULE := scfmanager.xml

LOCAL_MODULE_CLASS := ETC

#this will install the file in /system/etc/permissions
LOCAL_MODULE_PATH := $(TARGET_OUT_ETC)/permissions

LOCAL_SRC_FILES := $(LOCAL_MODULE)

include $(BUILD_PREBUILT)

# ==========================================================================
#  Secure CPU Function Service APK
# ==========================================================================
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional debug

LOCAL_AIDL_INCLUDES := $(call all-Iaidl-files-under, src/com/spectratech/scfservice)
LOCAL_SRC_FILES := $(call all-java-files-under, src/com/spectratech/scfservice)
LOCAL_SRC_FILES += $(call all-Iaidl-files-under, src/com/spectratech/scfservice)

LOCAL_PACKAGE_NAME := scfservice
LOCAL_CERTIFICATE := platform
LOCAL_PRIVILEGED_MODULE := true
LOCAL_PROGUARD_ENABLED := disabled

#include $(LOCAL_PATH)/pruned.mk

include $(BUILD_PACKAGE)

# ==========================================================================
# documentation
# ==========================================================================
include $(CLEAR_VARS)

LOCAL_SRC_FILES := $(call all-java-files-under, src/com/spectratech/scfmanager)
LOCAL_SRC_FILES += $(call all-Iaidl-files-under, src/com/spectratech/scfservice)
LOCAL_SRC_FILES += src/com/spectratech/scfservice/SCSystemCall.java
# LOCAL_SRC_FILES += \
#                 src/com/spectratech/scfservice/IScfRemoteServerService.aidl \
#                 src/com/spectratech/scfservice/IScfDispatcherCallback.aidl \
#                 src/com/spectratech/scfservice/IScfRequestCallback.aidl \
#                 src/com/spectratech/scfservice/IScfCancelCallback.aidl

LOCAL_MODULE:= scfmanagerlibrary_documentation
LOCAL_DROIDDOC_OPTIONS := scfmanagerlibrary
LOCAL_MODULE_CLASS := JAVA_LIBRARIES
LOCAL_DROIDDOC_USE_STANDARD_DOCLET := true

include $(BUILD_DROIDDOC)

# build the placeholder app
include $(call all-makefiles-under,$(LOCAL_PATH))

