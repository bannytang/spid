/******************************************************************************
 * @file    PermissionUtils.java
 * @brief   Permission helper class
 *
 *******************************************************************************/

package com.spectratech.scfservice;

import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.HashMap;

public class PermissionUtils {

    // system
    private static String cert_one_hash   = "53eecf9ec384e63181b416d77d34ba2211c19e0d70724bc452e7c7e43cccd6ff";

    // security
    private static String cert_two_hash   = "71ad2a0c6388832c81c4ac1547caadd229d3537ea29bf3bf7938379a9dbdce21";

    // normal
    private static String cert_three_hash = "80306e901777ca0a233f6a29da6168467e94936315ca00eee634b335625ef07d";

    // 0x0201 - 0x02 ClassID 0x01 FuncId
    // System (* + # + nil)
    private static final Short[] ApiGroupOneArray = new Short[] {
        0x0000, 0x0001, 0x0002, 0x0003, 0x0004, 0x0005, 0x0006, 0x0007, 0x0008, 0x0009, 0x000A, 0x000B, 0x000C, //ClassId 0x00 SysCmdClassSys
        0x0100, 0x0101, 0x0102, 0x0103, 0x0104, 0x0105, //ClassId 0x01 SysCmdClassSam
        0x0200, 0x0201, 0x0202, 0x0203, 0x0204, 0x0205, 0x0206, 0x0207, 0x0208, 0x0209, //ClassId 0x01 SysCmdClassCl
        0x0300, 0x0301, 0x0302, //ClassId 0x03 SysCmdClassMsr
        0x0400, 0x0401, 0x0402, 0x0403, 0x0404, 0x0405, //ClassId 0x04 SysCmdClassLpt
        0x0500, 0x0501, 0x0502, 0x0503, 0x0504, 0x0505, 0x0506, 0x0507, 0x0508, 0x0509, 0x0510, 0x0511, 0x0512, 0x0513,//ClassId 0x05 SysCmdClassHsm
        0x0600, 0x0601, 0x0602, 0x0603, //ClassId 0x06 SysCmdClassIcc
        0x0700, 0x0701, //ClassId 0x08 SysCmdClassRtc
        0x0800, 0x0801, 0x0802, 0x0803, 0x0804, 0x0805, 0x0806, 0x0807, //ClassId 0x08 SysCmdClassTamper
        0x0900, 0x0901, 0x0902, 0x0903, 0x0904, 0x0905, 0x0906, 0x0907, 0x0908, 0x0909, 0x090A, 
        0x090B, 0x090C, 0x090D, 0x090E, 0x090F, 0x0910, 0x0911, 0x0912, 0x0913, 0x0914, 0x0915,
        0x0916, 0x0917, 0x0918, 0x0919, 0x091A, //ClassId 0x09 SysCmdClassMif
        0x0A00, 0x0A01, 0x0A02, 0x0A03, //ClassId 0x0A SysCmdClassRand
        0x0B00, 0x0B01, //ClassId 0x0B SysCmdClassBuz
        0x0C00, 0x0C01, //ClassId 0x0C SysCmdClassAppDl
        0x0D00, 0x0D01, 0x0D02, 0x0D03, 0x0D04, 0x0D05, 0x0D06, //ClassId 0x0D SysCmdClassNfc
        0x1F00, 0x1F01, 0x1F02, 0x1F03, 0x1F04, 0x1F05, 0x1F06, 0x1F09, 0x1F0A, 0x1F0B, 0x1F0C, 0x1FF0, //ClassId 0x1F ApCmdClass
    };

    // Security ( # + nil)
    private static final Short[] ApiGroupTwoArray = new Short[] {
        0x0000, 0x0001, /*0x0002,*/ /*0x0003,*/ /*0x0004,*/ 0x0005, /*0x0006,*/ 0x0007, 0x0008, 0x0009, 0x000A, 0x000B, 0x000C, //ClassId 0x00 SysCmdClassSys
        0x0100, 0x0101, 0x0102, 0x0103, 0x0104, 0x0105, //ClassId 0x01 SysCmdClassSam
        0x0200, 0x0201, 0x0202, 0x0203, 0x0204, 0x0205, 0x0206, 0x0207, 0x0208, 0x0209, //ClassId 0x01 SysCmdClassCl
        0x0300, 0x0301, 0x0302, //ClassId 0x03 SysCmdClassMsr
        0x0400, 0x0401, 0x0402, 0x0403, 0x0404, 0x0405, //ClassId 0x04 SysCmdClassLpt
        0x0500, 0x0501, 0x0502, 0x0503, 0x0504, 0x0505, 0x0506, 0x0507, 0x0508, 0x0509, 0x0510, 0x0511, 0x0512, 0x0513,//ClassId 0x05 SysCmdClassHsm
        0x0600, 0x0601, 0x0602, 0x0603, //ClassId 0x06 SysCmdClassIcc
        0x0700, /*0x0701,*/ //ClassId 0x08 SysCmdClassRtc
        /*0x0800,*/ /*0x0801,*/ /*0x0802,*/ /*0x0803,*/ //ClassId 0x08 SysCmdClassTamper
        0x0900, 0x0901, 0x0902, 0x0903, 0x0904, 0x0905, 0x0906, 0x0907, 0x0908, 0x0909, 0x090A, 
        0x090B, 0x090C, 0x090D, 0x090E, 0x090F, 0x0910, 0x0911, 0x0912, 0x0913, 0x0914, 0x0915,
        0x0916, 0x0917, 0x0918, 0x0919, 0x091A, //ClassId 0x09 SysCmdClassMif
        0x0A00, 0x0A01, 0x0A02, 0x0A03, //ClassId 0x0A SysCmdClassRand
        /*0x0B00,*/ /*0x0B01,*/ //ClassId 0x0B SysCmdClassBuz
        /*0x0C00,*/ /*0x0C01,*/ //ClassId 0x0C SysCmdClassAppDl
        0x0D00, 0x0D01, 0x0D02, 0x0D03, 0x0D04, 0x0D05, 0x0D06, //ClassId 0x0D SysCmdClassNfc
        0x1F00, 0x1F01, 0x1F02, 0x1F03, 0x1F04, 0x1F05, 0x1F06, 0x1F09, 0x1F0A, 0x1F0B, 0x1F0C, 0x1FF0, //ClassId 0x1F ApCmdClass
    };

    // Normal (nil)
    private static final Short[] ApiGroupThreeArray = new Short[] {
        0x0000, 0x0001, /*0x0002,*/ /*0x0003,*/ /*0x0004,*/ 0x0005, /*0x0006,*/ 0x0007, 0x0008, 0x0009, 0x000A, 0x000B, 0x000C, //ClassId 0x00 SysCmdClassSys
        /*0x0100,*/ /*0x0101,*/ /*0x0102,*/ /*0x0103,*/ /*0x0104,*/ 0x0105, //ClassId 0x01 SysCmdClassSam
        /*0x0200,*/ /*0x0201,*/ /*0x0202,*/ /*0x0203,*/ /*0x0204,*/ /*0x0205,*/ /*0x0206,*/ 0x0207, 0x0208, 0x0209, //ClassId 0x01 SysCmdClassCl
        /*0x0300,*/ /*0x0301,*/ /*0x0302,*/ //ClassId 0x03 SysCmdClassMsr
        0x0400, 0x0401, 0x0402, 0x0403, 0x0404, 0x0405, //ClassId 0x04 SysCmdClassLpt
        /*0x0500,*/ /*0x0501,*/ /*0x0502,*/ /*0x0503,*/ /*0x0504,*/ /*0x0505,*/ /*0x0506,*/ /*0x0507,*/ /*0x0508,*/ /*0x0509,*/ /*0x0510,*/ /*0x0511,*/ /*0x0512,*/ //ClassId 0x05 SysCmdClassHsm
        /*0x0600,*/ /*0x0601,*/ /*0x0602,*/ //ClassId 0x06 SysCmdClassIcc
        0x0700, /*0x0701,*/ //ClassId 0x08 SysCmdClassRtc
        /*0x0800,*/ /*0x0801,*/ /*0x0802,*/ /*0x0803,*/ //ClassId 0x08 SysCmdClassTamper
        /*0x0900,*/ /*0x0901,*/ /*0x0902,*/ /*0x0903,*/ /*0x0904,*/ /*0x0905,*/ /*0x0906,*/ /*0x0907,*/ /*0x0908,*/ /*0x0909,*/ /*0x090A,*/ 
        /*0x090B,*/ /*0x090C,*/ /*0x090D,*/ /*0x090E,*/ /*0x090F,*/ /*0x0910,*/ /*0x0911,*/ /*0x0912,*/ /*0x0913,*/ /*0x0914,*/ /*0x0915,*/
        0x0916, /*0x0917,*/ /*0x0918,*/ /*0x0919,*/ 0x091A, //ClassId 0x09 SysCmdClassMif
        0x0A00, 0x0A01, /*0x0A02,*/ /*0x0A03,*/ //ClassId 0x0A SysCmdClassRand
        /*0x0B00,*/ /*0x0B01,*/ //ClassId 0x0B SysCmdClassBuz
        /*0x0C00,*/ /*0x0C01,*/ //ClassId 0x0C SysCmdClassAppDl
        0x0D00, 0x0D01, 0x0D02, 0x0D03, 0x0D04, 0x0D05, 0x0D06, //ClassId 0x0D SysCmdClassNfc
        /*0x1F00,*/ /*0x1F01,*/ /*0x1F02,*/ /*0x1F0A,*/ /*0x1F0B,*/ /*0x1FF0,*/ //ClassId 0x1F ApCmdClass
    };

    private static final List<Short> ApiGroupOneList = Arrays.asList(ApiGroupOneArray);

    private static final List<Short> ApiGroupTwoList = Arrays.asList(ApiGroupTwoArray);

    private static final List<Short> ApiGroupThreeList = Arrays.asList(ApiGroupThreeArray);

    private static final Map<String, List<Short>> ApiGroupsMap;

    static{
        ApiGroupsMap =   new HashMap<String, List<Short>>() {{
            put(cert_one_hash, ApiGroupOneList);
            put(cert_two_hash, ApiGroupTwoList);
            put(cert_three_hash, ApiGroupThreeList);
        }};
    }

    public static String getCertOneHash() {
        return cert_one_hash;
    }

    public static String getCertTwoHash() {
        return cert_two_hash;
    }

    public static String getCertThreeHash() {
        return cert_three_hash;
    }

    public static List<Short> getApiGroupOneList() {
        return ApiGroupOneList;
    }

    public static List<Short> getApiGroupTwoList() {
        return ApiGroupTwoList;
    }

    public static List<Short> getApiGroupThreeList() {
        return ApiGroupThreeList;
    }

    public static boolean checkPermission(String certHash, int classId, int funcId) {

        short id = (short)( (((byte)classId & 0xFF)<<8) | ((byte)funcId & 0xFF) );

        for (Entry<String, List<Short>> entry : ApiGroupsMap.entrySet()) {
            String hash = entry.getKey();
            List<Short>  list = entry.getValue();
            if (certHash.equals(hash)) {
                for(Short tmp : list) {
                    if (tmp == id){
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public static boolean checkPermission(int classId, int funcId) {

        short id = (short)( (((byte)classId & 0xFF)<<8) | ((byte)funcId & 0xFF) );

        for(Short tmp : ApiGroupThreeArray) {
            if (tmp == id){
                return true;
            }
        }
        return false;
    }

}

