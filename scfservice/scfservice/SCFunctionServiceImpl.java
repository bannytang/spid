/******************************************************************************
  @file    SCFunctionServiceImpl.java
******************************************************************************/

package com.spectratech.scfservice;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Message;
import android.os.Process;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.os.PowerManager;
import android.util.Log;
import android.util.Slog;
import android.util.ArrayMap;
import com.android.internal.os.SomeArgs;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.content.pm.Signature;
import android.content.pm.PackageManager;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import java.util.concurrent.ConcurrentHashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

import java.io.IOException;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.MessageDigest;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import android.os.SystemProperties;

/*
 * Main entry for the Secure CPU Funtion remote service
 */
public class SCFunctionServiceImpl {

    private static final String LOG_TAG = "SCFunctionServiceImpl";

    public static final int MESSAGE_SEND_REQUEST         = 1;
    public static final int MESSAGE_RESPONSE             = 2;
    public static final int MESSAGE_CANCEL_REQUEST       = 3;

    public static final int TYPE_SEND_COMPLETE   = 0x01;
    public static final int TYPE_SEND_FAIL       = 0x02;
    public static final int TYPE_REPLY_COMPLETE  = 0x03;
    public static final int TYPE_REPLY_FAIL      = 0x04;
    public static final int TYPE_DATA_DISPATCH   = 0x10;

    public static class ScfRemoteError {

        public static final int SCF_REMOTE_SUCCESS = 0x00;

        public static final int SCF_REMOTE_ERROR   = 0x10;
    }

    private Context mContext;
    private PackageManager mPackageManager = null;
    private ActivityManager mActivityManager = null;

    private static final Object StaticLock = new Object();

    private static volatile int mToken = 0xFF; //initial token
    private final int SCF_SERVICE_MAX_TOKEN = 65535;
    private ScfRemoteServerSocket mRemoteSocket = null;
    private ConcurrentHashMap<SCSystemCall, IScfRequestCallback> mRequestCallbackMap =
        new ConcurrentHashMap<SCSystemCall, IScfRequestCallback>();

    private RemoteCallbackList<IScfDispatcherCallback> mDispatcherCallbackList =
        new RemoteCallbackList<IScfDispatcherCallback>();

    private class DeathRecipientHandler implements IBinder.DeathRecipient{

        private IBinder callBack;
        private int pid;

        public DeathRecipientHandler(IBinder callBack, int pid){
            ScfLog.w("DeathRecipientHandler constructor client pid " + pid);
            this.callBack = callBack;
            this.pid = pid;
        }

        @Override
        public void binderDied() {
            ScfLog.w("binderDied() client pid " + pid +" binder died!");
            removeRequestCallbackByAppId(pid);
            try{
                callBack.unlinkToDeath(this, 0);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    private Handler mCommandHander = new Handler() {

        @Override
        public void dispatchMessage(Message msg) {

            ScfLog.d("handleMessage() msg.what="+msg.what);

            if (msg.what == 1) {
                PowerManager pm = (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
                pm.reboot("normal_reboot");
            }
        }

    };
    private Handler mResponseHander = new Handler() {
        public void handleMessage (Message msg) {

            if (msg.what == MESSAGE_RESPONSE) {
                try {
                    final byte[] bytes = (byte[])msg.obj;
                    final short respType = (short)msg.arg1;
                    final int token = (int)msg.arg2;

                    switch (respType) {

                        case TYPE_DATA_DISPATCH: {
                            int classId = bytes[0] & 0xFF;
                            int funcId = bytes[1] & 0xFF;
                            final String str = String.format("%04x", (classId << 8) | funcId);
                            ScfLog.i("handleMessage() - TYPE_DATA_DISPATCH id:0x"+str);

                            if (classId == 0x1F && funcId == 0x02) {
                                ScfLog.d("clear all request!");
                                mRequestCallbackMap.clear();

                            } else if (isAlertRequest(classId, funcId)){

                                final IScfRequestCallback callback = getAlertCallback(classId, funcId);
                                if (callback == null) {
                                    ScfLog.d("onAlert - null alert callback");
                                    return;
                                }

                                Runnable r = new Runnable() {
                                    @Override
                                    public void run() {
                                        try {

                                            if (bytes.length >= 6) {
                                                callback.onReplyComplete(Arrays.copyOfRange(bytes, 6, bytes.length));
                                            }
                                        } catch (Exception e) {
                                            ScfLog.e("Exception @" +
                                                    "handleMessage() - onReplyComplete");
                                            e.printStackTrace();
                                        }
                                    }
                                };
                                AsyncTask.THREAD_POOL_EXECUTOR.execute(r);
                            } else {
                                if (bytes.length >= 6) {
                                    try {
                                        if (classId == 0x1F && funcId == 0x00 
                                            && bytes[7] == 0 && bytes[8] == 0) {
                                            ScfLog.i("receive sc reset event");
                                            mRequestCallbackMap.clear();
                                        }
                                    } catch (ArrayIndexOutOfBoundsException e) {}

                                    broadcastDispatcherCallback(bytes);
                                }
                            }
                        }
                        break;

                        case TYPE_SEND_COMPLETE: {
                            ScfLog.i("handleMessage() - SEND_COMPLETE");
                            Runnable r = new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        // synchronized(mRequestCallbackMap) {
                                            if(mRequestCallbackMap !=null && mRequestCallbackMap.size()>0) {
                                                IScfRequestCallback callback = null;
                                                for (final Map.Entry<SCSystemCall, IScfRequestCallback> entry : mRequestCallbackMap.entrySet()){
                                                    SCSystemCall sscall = entry.getKey();
                                                    if (sscall != null && sscall.getToken() == token) {
                                                        callback = (IScfRequestCallback) entry.getValue();
                                                        byte replyCode = (byte) (bytes[0] & 0xFF);
                                                        boolean hasResponse = sscall.getHasResponse();
                                                        if (!hasResponse) {
                                                            ScfLog.d("handleMessage() - onSendComplete remove callback");
                                                            mRequestCallbackMap.remove(sscall);
                                                        }
                                                        if (callback != null) {
                                                            ScfLog.d("handleMessage() - onSendComplete invoke callback requestId:" + token
                                                                + " classId:"+sscall.getClassId() + " funcId:"+sscall.getFuncId());
                                                            callback.onSendComplete(replyCode);
                                                        } else {
                                                            ScfLog.d("handleMessage() - onSendComplete null callback requestId:" + token
                                                                + " classId:"+sscall.getClassId() + " funcId:"+sscall.getFuncId());
                                                        }
                                                    }
                                                }
                                            }
                                        // }
                                    } catch (Exception e) {
                                        ScfLog.e("Exception @" +
                                                "handleMessage() - onSendComplete");
                                        e.printStackTrace();
                                    }
                                }
                            };
                            AsyncTask.THREAD_POOL_EXECUTOR.execute(r);
                        }
                        break;

                        case TYPE_SEND_FAIL: {
                            ScfLog.i("handleMessage() - SEND_FAIL");
                            Runnable r = new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        // synchronized(mRequestCallbackMap) {
                                            if(mRequestCallbackMap !=null && mRequestCallbackMap.size()>0) {
                                                IScfRequestCallback callback = null;
                                                for (final Map.Entry<SCSystemCall, IScfRequestCallback> entry : mRequestCallbackMap.entrySet()){
                                                    SCSystemCall sscall = entry.getKey();
                                                    if (sscall != null && sscall.getToken() == token) {
                                                        callback = (IScfRequestCallback) entry.getValue();
                                                        ScfLog.d("handleMessage() - SEND_FAIL remove callback");
                                                        mRequestCallbackMap.remove(sscall);
                                                        if ( callback != null) {
                                                            ScfLog.d("handleMessage() - SEND_FAIL invoke callback");
                                                            callback.onSendComplete(0x15);
                                                        } else {
                                                            ScfLog.d("handleMessage() - SEND_FAIL null callback");
                                                        }
                                                    }
                                                }
                                            }
                                        // }
                                    } catch (Exception e) {
                                        ScfLog.e("Exception @" +
                                                "handleMessage() - onSendFailed");
                                        e.printStackTrace();
                                    }
                                }
                            };
                            AsyncTask.THREAD_POOL_EXECUTOR.execute(r);
                        }
                        break;

                    case TYPE_REPLY_COMPLETE: {
                            final IScfRequestCallback callback = getRequestCallback(token);
                            final short id = (short)((token & 0xFFFF0000) >> 16);
                            ScfLog.i("handleMessage() - onReplyComplete id:" + id + " bytes.length:"+bytes.length);
                            if (callback == null) {
                                ScfLog.d("onReplyComplete - null request callback");

                                // ApFuncSCResetRequest
                                if (id == 0x1F02) {
                                    // synchronized(mRequestCallbackMap) {
                                        ScfLog.d("clear all request!");
                                        mRequestCallbackMap.clear();
                                    // }
                                }

                                return;
                            }

                            Runnable r = new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        ScfLog.d("thread name:"+Thread.currentThread().getName() + " time:"+System.currentTimeMillis());
                                        // only send message payload not header
                                        if (bytes.length >= 6) {
                                            removeRequestCallback(token, false);
                                            callback.onReplyComplete(Arrays.copyOfRange(bytes, 6, bytes.length));
                                        } else {
                                            // something wrong with response data, remove callback and send null reply
                                            removeRequestCallback(token, false);
                                            callback.onReplyComplete(null);
                                        }

                                        // ApFuncSCResetRequest
                                        if (id == 0x1F02) {
                                            // synchronized(mRequestCallbackMap) {
                                                ScfLog.d("reset request queue");
                                                mRequestCallbackMap.clear();
                                            // }
                                        }

                                    } catch (Exception e) {
                                        ScfLog.e("Exception @" +
                                                "handleMessage() - onReplyComplete");
                                        e.printStackTrace();
                                    }
                                }
                            };
                            AsyncTask.THREAD_POOL_EXECUTOR.execute(r);
                        }
                        break;

                        /*
                         * Will not happen becasue there's always a reply response from SC, if not the SC has been dead.
                         * Upper layer no need to take care of the case that no response from SC
                         */
                        case TYPE_REPLY_FAIL: {
                            // two approachs to notify the failure status to upper layer
                            // 1. set response data to null - all response data must not be null normally
                            //    but upper layer must handle the null data first or will throw a null-pointer exception
                            // 2. set a reply failure callback
                            // use the first approach at this time
                            final IScfRequestCallback callback = getRequestCallback(token);
                            if (callback == null) {
                                ScfLog.d("onReplyFailed - null request callback");
                                return;
                            }
                            ScfLog.i("handleMessage() - onReplyFailed");
                            Runnable r = new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        removeRequestCallback(token, true);
                                        // TOOD : return null to up layer will generate a null pointer exeception
                                        callback.onReplyComplete(null);
                                    } catch (Exception e) {
                                        ScfLog.e("Exception @" +
                                                "handleMessage() - onReplyFailed");
                                        e.printStackTrace();
                                    }
                                }
                            };
                            AsyncTask.THREAD_POOL_EXECUTOR.execute(r);
                        }
                        break;

                        default:
                            ScfLog.e("unknown respType:"+respType);
                            return;

                    }
                } catch (Exception e) {
                    ScfLog.e("error occured when parsing the resp/ind");
                    e.printStackTrace();
                    return;
                }
            } else {
                ScfLog.e("unknown message:" + msg.what);
                return;
            }
        }
    };

    /**
     * Constructor of SCFunctionServiceImpl class
     *
     * @param Context.
     */
    public SCFunctionServiceImpl(Context context) {
        ScfLog.v("init constructor ");

        mContext = context;
        mActivityManager =
            (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        mPackageManager = mContext.getPackageManager();
    }

    /**
     * get the ISCFunctionService binder to publish system service.
     *
     * @return ISCFunctionService binder.
     */
    public IScfRemoteServerService.Stub getBinder() {
        return mBinder;
    }

    /**
     *
     */
    public void unBindService(int appId) {
        ScfLog.d("unBindService()");
        removeRequestCallbackByAppId(appId);
    }

    /**
     * Method initialize connection to remote service
     */
    public void init() {
        ScfLog.d("init()");
        if (mRemoteSocket == null) {
            mRemoteSocket = new ScfRemoteServerSocket(mResponseHander);
            mRemoteSocket.start();
        }
    }

    private synchronized int generateToken(int classId, int funcId) {
        short low = (short) (mToken & 0x0000FFFF);
        low++;
        if(low > SCF_SERVICE_MAX_TOKEN) {
            low = 0x100; // 0x00 is for dispatch message
        }
        mToken = (classId & 0xFF) << 24 | (funcId  & 0xFF) << 16 | (low & 0xFFFF);
        return mToken;
    }

    private synchronized boolean searchToken(int classId, int funcId) {
        boolean found = false;
        if (mRequestCallbackMap != null && mRequestCallbackMap.size() > 0) {
            for (Map.Entry<SCSystemCall, IScfRequestCallback> entry : mRequestCallbackMap.entrySet()) {
                SCSystemCall sscall = entry.getKey();
                if (sscall.getClassId() == classId &&
                    sscall.getFuncId() == funcId) {
                    int appId = sscall.getAppId();

                    List<RunningAppProcessInfo> procs =
                        mActivityManager.getRunningAppProcesses();

                    for (RunningAppProcessInfo proc : procs) {
                        if (appId == proc.pid) {
                            ScfLog.d("searchToken found running appId!");
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        ScfLog.w("searchToken remove obselete sscall");
                        mRequestCallbackMap.remove(sscall);
                    }
                }
            }
        }

        return found;
    }

    private synchronized IScfRequestCallback getRequestCallback(int token) {
        IScfRequestCallback callback = null;
        if (mRequestCallbackMap != null && mRequestCallbackMap.size() > 0) {
            for (Map.Entry<SCSystemCall, IScfRequestCallback> entry : mRequestCallbackMap.entrySet()) {
                SCSystemCall sscall = entry.getKey();
                if (sscall.getToken() == token) {
                    ScfLog.d("getRequestCallback found sscall!");
                    callback = entry.getValue();
                    break;
                }
            }
        }

        return callback;
    }

    private synchronized void removeRequestCallback(int token, boolean force) {
        IScfRequestCallback callback = null;
        if (force || !isAlertRequest(token)) {
            if (mRequestCallbackMap != null && mRequestCallbackMap.size() > 0) {
                for (Map.Entry<SCSystemCall, IScfRequestCallback> entry : mRequestCallbackMap.entrySet()) {
                    SCSystemCall sscall = entry.getKey();
                    if (sscall.getToken() == token) {
                        ScfLog.d("removeRequestCallback found sscall!");
                        mRequestCallbackMap.remove(sscall);
                        break;
                    }
                }
            }
        }
    }

    private synchronized void removeRequestCallbackByAppId(int appId) {
        if (mRequestCallbackMap != null && mRequestCallbackMap.size() > 0) {
            for (Map.Entry<SCSystemCall, IScfRequestCallback> entry : mRequestCallbackMap.entrySet()) {
                SCSystemCall sscall = entry.getKey();
                if (sscall.getAppId() == appId) {
                    ScfLog.d("removeRequestCallback found sscall!");
                    mRequestCallbackMap.remove(sscall);
                }
            }
        }
    }

    // Sam class      0x01 0x05 SysFuncSamAlert
    // MSR class      0x03 0x02 SysFuncMsrReadAlert
    // Printer class  0x04 0x04 SysFuncLptAlert
    private static final Short[] AlertGroup = new Short[] {0x0105, 0x0302, 0x0404};
    private static final List<Short> AlertGroupList = Arrays.asList(AlertGroup);

    private boolean isAlertRequest(int token) {
        short id = (short)((token & 0xFFFF0000) >> 16);
        for(Short tmp : AlertGroupList) {
            if (tmp == id){
                return true;
            }
        }
        return false;
    }

    private boolean isAlertRequest(final int classId, final int funcId) {
        short id = (short) (((classId & 0xFF) << 8) | (funcId & 0xFF));
        for(Short tmp : AlertGroupList) {
            if (tmp == id){
                return true;
            }
        }
        return false;
    }

    private synchronized IScfRequestCallback getAlertCallback(int classId, int funcId) {
        IScfRequestCallback callback = null;
        if (mRequestCallbackMap != null && mRequestCallbackMap.size() > 0) {
            for (Map.Entry<SCSystemCall, IScfRequestCallback> entry : mRequestCallbackMap.entrySet()) {
                SCSystemCall sscall = entry.getKey();
                 if (classId == sscall.getClassId() && funcId == sscall.getFuncId()) {
                    ScfLog.d("getRequestCallback found sscall!");
                    callback = entry.getValue();
                    break;
                }
            }
        }

        return callback;
    }

    private void broadcastDispatcherCallback(byte[] bytes){
        final int len = mDispatcherCallbackList.beginBroadcast();
        for (int i = 0; i < len; i++){
            try {
                mDispatcherCallbackList.getBroadcastItem(i).onSCDataDispatched(bytes);
            } catch (RemoteException e) {
                ScfLog.e("broadcast error!");
                e.printStackTrace();
            }
        }
        mDispatcherCallbackList.finishBroadcast();
    }

    protected void destroyService() {
        ScfLog.d("destroyService()");
        // mContext = null;
        if (mRemoteSocket != null) {
            mRemoteSocket.stop();
            mRemoteSocket = null;
        }
        mResponseHander = null;
        mDispatcherCallbackList.kill();
        mDispatcherCallbackList = null;
        mRequestCallbackMap.clear();
        mRequestCallbackMap = null;

    }

    private final IScfRemoteServerService.Stub mBinder = new IScfRemoteServerService.Stub() {

        @Override
        public boolean onTransact(int code, Parcel data, Parcel reply, int flags)
                throws RemoteException {
            try {
                return super.onTransact(code, data, reply, flags);
            } catch (RuntimeException e) {
                if (!(e instanceof SecurityException) && !(e instanceof IllegalArgumentException)) {
                    Slog.wtf(LOG_TAG, "SCFService Crash", e);
                }
                throw e;
            }
        }

        public int registerCallback(IScfDispatcherCallback cb) throws RemoteException, RuntimeException, IllegalArgumentException {
            if(!verifyAuthenticity(mBinder.getCallingUid())) {
                ScfLog.i("Verify authenticity failed");
                return ScfRemoteError.SCF_REMOTE_ERROR;
            }
            if (cb == null) {
                ScfLog.d("registerCallback() - null cb");
                throw new IllegalArgumentException("IScfDispatcherCallback cb is null!");
            }
            ScfLog.d("registerCallback() - add cb");
            cb.asBinder().linkToDeath(new DeathRecipientHandler(cb.asBinder(), mBinder.getCallingPid()), 0);
            mDispatcherCallbackList.register(cb);

            return ScfRemoteError.SCF_REMOTE_SUCCESS;
        }

        public int deregisterCallback(IScfDispatcherCallback cb) throws RemoteException, RuntimeException, IllegalArgumentException {
            if(!verifyAuthenticity(mBinder.getCallingUid())) {
                ScfLog.d("Verify authenticity failed");
                return ScfRemoteError.SCF_REMOTE_ERROR;
            }
            if (cb == null) {
                ScfLog.d("deregisterCallback() - null cb");
                throw new IllegalArgumentException("IScfDispatcherCallback cb is null!");
            }
            ScfLog.d("deregisterCallback() - remove cb");
            mDispatcherCallbackList.unregister(cb);
            return ScfRemoteError.SCF_REMOTE_SUCCESS;
        }

        public int SCFRemoteServerConnect() {
            throw new UnsupportedOperationException("Not yet implemented");
        }

        public int SCFRemoteServerDisconnect() {
            ScfLog.d("SCFRemoteServerDisconnect()");
            if(!verifyAuthenticity(mBinder.getCallingUid())) {
                ScfLog.d("Verify authenticity failed");
                return ScfRemoteError.SCF_REMOTE_ERROR;
            }
            removeRequestCallbackByAppId(mBinder.getCallingPid());
            return ScfRemoteError.SCF_REMOTE_SUCCESS;
        }


        /**
         *        0   - Failed
         *        > 0 - Request Id
         */
        public int SCFRemoteServerSend(SCSystemCall sccall, IScfRequestCallback cb) throws RemoteException, RuntimeException, IllegalArgumentException {
            synchronized (StaticLock) {
                boolean hasResponse = sccall.getHasResponse();
                int classId = sccall.getClassId();
                int funcId = sccall.getFuncId();
                int id = (classId & 0xFF) << 8 | (funcId & 0xFF);
                if(!verifyAuthenticity(mBinder.getCallingUid(), classId, funcId)) {
                    ScfLog.d("Verify authenticity failed");
                    return 0;
                }

                if (mRemoteSocket == null) {
                    ScfLog.e("socket is not connected");
                    return 0;
                }

                // special request to reboot Android system for EMV Level 2 cert
                if ( id == 0x1F0C ) {
                    mCommandHander.obtainMessage(1).sendToTarget();
                    return 0x1F0C;
                }

                if ( id == 0x1F02 ) {
                    ScfLog.d("Send reset SC request directly");
                    int token = generateToken(classId, funcId);
                    sccall.setToken(token);

                } else {
                    int appid = mBinder.getCallingPid();
                    if (searchToken(classId, funcId)) {
                        ScfLog.d("Found classId " + classId + " funcId " + funcId + " in queue!");
                        return 0;
                    }

                    int token = generateToken(classId, funcId);
                    sccall.setToken(token);
                    sccall.setAppId(appid);
                    ScfLog.d("add token " + sccall.getToken() + " appid:"+sccall.getAppId());
                    if (cb != null) {
                        mRequestCallbackMap.put(sccall, cb);
                    } else {
                        ScfLog.d("SCFRemoteServerSend() callback is null");
                        IScfRequestCallback dummyCb = new IScfRequestCallback.Stub(){
                            @Override
                            public void onSendComplete(int returnCode) throws RemoteException {
                            }

                            @Override
                            public void onReplyComplete(byte[] respData) throws RemoteException {
                            }

                        };
                        mRequestCallbackMap.put(sccall, dummyCb);
                    }

                    if ( id == 0x0105 || id == 0x0302 || id == 0x0404){ //Alert requests should not sent to SC
                        return sccall.getToken();
                    }
                }

                CountDownLatch latch = new CountDownLatch(1);
                SomeArgs args = SomeArgs.obtain();
                args.arg1 = sccall;
                args.arg2 = latch;

                ScfLog.d("SCFRemoteServerSend() cbmap size:"+ mRequestCallbackMap.size() +" send token: "+ sccall.getToken());
                mRemoteSocket.obtainMessage(MESSAGE_SEND_REQUEST, sccall.getToken(), 0, args).sendToTarget();

                try {
                    // 0x1FF0 is SPI link test request
                    if (id != 0x1FF0 && !latch.await(3, TimeUnit.SECONDS)) {
                        ScfLog.w("Timeout waiting for sending complete");
                        return 0;
                    }
                } catch (InterruptedException e) {
                    ScfLog.w("Interrupted waiting for sending complete");
                }

                return sccall.getToken(); // return the the request token id must > 0
            }
        }

        public int SCFRemoteServerCancel(int requestId, IScfCancelCallback cb) throws RemoteException, RuntimeException, IllegalArgumentException {
            int result = ScfRemoteError.SCF_REMOTE_ERROR;
            if(!verifyAuthenticity(mBinder.getCallingUid())) {
                ScfLog.d("Verify authenticity failed");
                return ScfRemoteError.SCF_REMOTE_ERROR;
            }

            ScfLog.d("SCFRemoteServerCancel() + requestId:" + requestId);
            removeRequestCallback(requestId, true);
            try {
                if (cb != null) {
                    cb.onCancelComplete(result);
                }
            } catch (RemoteException e) {
                ScfLog.w("Failed to cancel request.");
                e.printStackTrace();
            }

            CountDownLatch latch = new CountDownLatch(1);
            SomeArgs args = SomeArgs.obtain();
            args.arg1 = requestId;
            args.arg2 = latch;

            ScfLog.d("SCFRemoteServerCancel() cbmap size:"+ mRequestCallbackMap.size() +" requestId: "+ requestId);
            mRemoteSocket.obtainMessage(MESSAGE_CANCEL_REQUEST, 0, 0, args).sendToTarget();

            try {
                if (!latch.await(3, TimeUnit.SECONDS)) {
                    ScfLog.w("Timeout waiting for sending complete");
                    return ScfRemoteError.SCF_REMOTE_ERROR;
                }
            } catch (InterruptedException e) {
                ScfLog.w("Interrupted waiting for sending complete");
            }

            return ScfRemoteError.SCF_REMOTE_SUCCESS;
        }
    };

	private static String bytesToHex(byte[] inputBytes) {
        final StringBuilder sb = new StringBuilder();
        for(byte b : inputBytes) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }

    private boolean verifyAuthenticity(int uid, int classId, int funcId){
        ScfLog.d("verifyAuthenticity uid:" + uid + " classId:" + classId + " funcId:" + funcId);

        return true;
    }

	private boolean verifyAuthenticity(int uid){
        ScfLog.d("verifyAuthenticity uid:" + uid);

        return true;
    }

}
