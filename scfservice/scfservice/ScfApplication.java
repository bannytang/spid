/******************************************************************************
  @file    ScfApplication.java
******************************************************************************/

package com.spectratech.scfservice;

import android.app.Application;
import android.content.Context;
import android.content.ComponentName;
import android.content.Intent;
import android.util.Log;

public class ScfApplication extends Application {
    public final static String TAG = "ScfApplication";

    public ScfApplication() {
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate()");
        ComponentName comp = new ComponentName(getApplicationContext().getPackageName(), 
                                               SCFunctionService.class.getName());
        if (comp != null) {
            ComponentName service = getApplicationContext().startService(new Intent().setComponent(comp));
            if (service == null) {
                Log.e(TAG, "service failed to start " + comp.toString());
            } else {
                Log.d(TAG, "service started successfully");
            }
        } else {
            Log.e(TAG, "service does not exist!");
        }
    }

};
