/******************************************************************************
 * @file    ISCSystemCall.aidl
 * @brief   This interface provides the APIs to Secure CPU function call
 *
 *******************************************************************************/

package com.spectratech.scfservice;

import java.util.List;
import android.os.Parcelable;
import android.os.Parcel;

public class SCSystemCall implements Parcelable {
    protected int mAppId;
    protected byte mClassId;
    protected byte mFuncId;
    protected int mToken = 0;
    protected boolean mHasResponse = true;
    protected byte[] mData = null;

    public SCSystemCall(int appId, int classId, int funcId, boolean hasResp, byte[] payload) {
        this.mAppId = appId;
        this.mClassId = (byte)(classId & 0xFF);
        this.mFuncId = (byte)(funcId & 0xFF);
        this.mHasResponse = hasResp;
        if(payload != null && payload.length > 0) {
            this.mData = new byte[payload.length];
            System.arraycopy(payload, 0, this.mData, 0, payload.length);
        }
    }

    public void setAppId (int appId) {
        this.mAppId = appId;
    }

    public int getAppId () {
        return this.mAppId;
    }

    public void setClassId (int classId) {
        this.mClassId = (byte)(classId & 0xFF);
    }

    public byte getClassId () {
        return this.mClassId;
    }

    public void setFuncId (int funcId) {
        this.mFuncId = (byte)(funcId & 0xFF);
    }

    public byte getFuncId () {
        return this.mFuncId;
    }

    public void setHasResponse(boolean hasResponse) {
        this.mHasResponse = hasResponse;
    }

    public boolean getHasResponse() {
        return this.mHasResponse;
    }

    public void setPayload (byte[] payload) {
        if(payload == null) {
            return;
        }
        this.mData = new byte[payload.length];
        System.arraycopy(payload, 0, this.mData, 0, payload.length);
    }

    public byte[] getPayload () {
        if(this.mData == null) {
            return null;
        }
        return this.mData;
    }

    public int getPayloadLength () {
        return mData != null ? mData.length : 0;
    }

    public int getToken() {
        return this.mToken;
    }

    public void setToken(int token) {
        this.mToken = token;
    }

    private byte calculateLrc(byte[] bytes) {
        byte lrc = 0;
        for (byte b : bytes) {
            lrc ^= b;
        }
        return lrc;
    }

    public int calculateCrc(byte[] bytes) { 
        int crc = 0xFFFF;          // initial value
        int polynomial = 0x1021;   // 0001 0000 0010 0001  (0, 5, 12) 

        for (byte b : bytes) {
            for (int i = 0; i < 8; i++) {
                boolean bit = ((b   >> (7-i) & 1) == 1);
                boolean c15 = ((crc >> 15    & 1) == 1);
                crc <<= 1;
                if (c15 ^ bit) crc ^= polynomial;
            }
        }

        crc &= 0xFFFF;
        return crc;
    }

    public byte[] toByteArray() {
        int len = getPayloadLength();
        byte[] result = new byte[len + 12];
        byte len_l = (byte)(len & 0xFF);
        byte len_h = (byte)((len >> 8) & 0xFF);
        int crc = calculateCrc(mData);
        byte crc_l = (byte)(crc & 0xFF);
        byte crc_h = (byte)((crc >> 8) & 0xFF);
        byte lrc = calculateLrc(new byte[] {(byte) 0x00, (byte) 0x00, len_l, len_h});
        byte[] header = {(byte) 0x00, lrc, (byte) 0x00, (byte)0x00, len_h, len_l};
        byte[] tail = {crc_l, crc_h, (byte) 0x00, (byte)0x00, (byte)0x00, (byte)0x00};
        System.arraycopy(header, 0, result, 0, header.length);
        System.arraycopy(mData, 0, result, header.length, mData.length);
        System.arraycopy(tail, 0, result, header.length + mData.length, tail.length);
        return result;
    }

    public SCSystemCall(Parcel in) {
        this.mAppId   = in.readInt();
        this.mClassId = in.readByte();
        this.mFuncId  = in.readByte();
        this.mToken  = in.readInt();
        this.mHasResponse = in.readInt() == 1;
        this.mData    = in.createByteArray();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mAppId);
        dest.writeInt(mClassId);
        dest.writeInt(mFuncId);
        dest.writeInt(mToken);
        dest.writeInt(mHasResponse ? 1 : 0);
        dest.writeByteArray(mData);
    }


    public static final Parcelable.Creator<SCSystemCall> CREATOR =
                                            new Parcelable.Creator<SCSystemCall>() {

        public SCSystemCall createFromParcel(Parcel in) {
            return new SCSystemCall(in);
        }

        public SCSystemCall[] newArray(int size) {
            return new SCSystemCall[size];
        }
    };

    @Override
    public String toString() {
        return "SCSystemCall [appID=" + mAppId + ", classId=" + mClassId + " funcId="
                + mFuncId + " bytes="+bytesToHex(toByteArray())+"]";
    }

    private static String bytesToHex(byte[] inputBytes) {
        final StringBuilder sb = new StringBuilder();
        for(byte b : inputBytes) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }

}

