/******************************************************************************
  @file    SCFunctionService2.java
******************************************************************************/

package com.spectratech.scfservice;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.ServiceManager;
import android.os.SystemProperties;

import com.spectratech.scfservice.SCFunctionServiceImpl;

/*
 * SCFunctionService
 */
public class SCFunctionService extends Service {

    public static final boolean DBG = (SystemProperties.getInt("ro.debuggable", 0) == 1);
    static final String TAG = "SCFunctionService";

    private SCFunctionServiceImpl mServiceImpl = null;
    private IScfRemoteServerService.Stub mBinder = null;

    @Override
    public void onCreate() {
        log("BANNY onCreate");
        if (mServiceImpl == null) {
            mServiceImpl = new SCFunctionServiceImpl(this);
        }

        mServiceImpl.init();

        if (ServiceManager.getService("scfservice") == null) {
            log("register scfservice on entry");
            ServiceManager.addService("scfservice", mServiceImpl.getBinder());
        }

    }

    @Override
    public void onDestroy() {
        log("BANNY onDestroy()");
        stopSelf();
        super.onDestroy();
        if (mBinder != null && mServiceImpl != null) {
            log("destroy service");
            mServiceImpl.destroyService();
        }
        mBinder = null;
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        log("BANNY onStartCommand");
        mServiceImpl.init();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        log("BANNY onStartCommand");
        // mServiceImpl.init();
        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        log("BANNY onBind");
        if (mBinder == null) {
            mBinder = mServiceImpl.getBinder();
        }
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        log("BANNY onUnbind");
        if (mBinder != null && mServiceImpl != null) {
            log("unbind service " + intent);
            mServiceImpl.unBindService(mBinder.getCallingPid());
        }

        return super.onUnbind(intent);
    }

    private static void log(String msg) {
        if (DBG) {
            android.util.Log.d(TAG, msg);
        }
    }

}
