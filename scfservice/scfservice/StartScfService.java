/******************************************************************************
  @file    StartScfService.java
******************************************************************************/

package com.spectratech.scfservice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ComponentName;
import android.content.Intent;
import android.util.Log;

public class StartScfService extends BroadcastReceiver
{
    public final static String TAG = "StartScfService";

    @Override
    public void onReceive(Context context, Intent intent)
    {
        final boolean bootCompleted = intent.getAction().equals("android.intent.action.BOOT_COMPLETED");
        if( bootCompleted ){
            Log.d(TAG, "Starting SCF service after boot completed");
            startService(context);
        }
    }

    private void startService(Context context) {

         ComponentName comp = new ComponentName(context.getPackageName(), 
                                                SCFunctionService.class.getName());
         if (comp != null) {
             ComponentName service = context.startService(new Intent().setComponent(comp));
             if (service == null) {
                 Log.e(TAG, "Could Not Start Service " + comp.toString());
             } else {
                 Log.d(TAG, "Started Service Successfully");
             }
         } else {
             Log.e(TAG, "Not Started Service Successfully");
         }
    }

};
