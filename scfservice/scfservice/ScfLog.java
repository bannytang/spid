/******************************************************************************
  @file    SCFunctionServiceImpl.java
******************************************************************************/

package com.spectratech.scfservice;


import android.os.SystemProperties;
import android.util.Log;

/**
 * Log utilities class for RCS. Default log level is
 * {@link com.spectratech.scfservice.ScfLog#DEFAULT_LOG_LEVEL}.
 */
public class ScfLog {
    private static final int LOG_LEVEL_NONE = 0;

    private static final int LOG_LEVEL_VERBOSE = 1;

    private static final int LOG_LEVEL_DEBUG = 2;

    private static final int LOG_LEVEL_INFO = 3;

    private static final int LOG_LEVEL_WARNNING = 4;

    private static final int LOG_LEVEL_ERROR = 5;

    private static final int DEFAULT_LOG_LEVEL = LOG_LEVEL_VERBOSE;

    private static final String LOG_LEVEL_PROPERTIES = "persist.sys.scf.log.level";

    private static String sTag = "SCFService";

    public static void v(String msg) {
        int logLevel = getLogLevel();
        if (logLevel > 0 && logLevel <= LOG_LEVEL_VERBOSE) {
            Log.v(sTag, msg);
        }
    }

    public static void d(String msg) {
        int logLevel = getLogLevel();
        if (logLevel > 0 && logLevel <= LOG_LEVEL_DEBUG) {
            Log.d(sTag, msg);
        }
    }

    public static void i(String msg) {
        int logLevel = getLogLevel();
        if (logLevel > 0 && logLevel <= LOG_LEVEL_INFO) {
            Log.i(sTag, msg);
        }
    }

    public static void w(String msg) {
        int logLevel = getLogLevel();
        if (logLevel > 0 && logLevel <= LOG_LEVEL_WARNNING) {
            Log.w(sTag, msg);
        }
    }

    public static void w(Throwable tr) {
        int logLevel = getLogLevel();
        if (logLevel > 0 && logLevel <= LOG_LEVEL_WARNNING) {
            Log.w(sTag, tr);
        }
    }

    public static void w(String msg, Throwable tr) {
        int logLevel = getLogLevel();
        if (logLevel > 0 && logLevel <= LOG_LEVEL_WARNNING) {
            Log.w(sTag, msg, tr);
        }
    }

    public static void e(String msg) {
        int logLevel = getLogLevel();
        if (logLevel > 0 && logLevel <= LOG_LEVEL_ERROR) {
            Log.e(sTag, msg);
        }
    }

    public static void e(Throwable tr) {
        int logLevel = getLogLevel();
        if (logLevel > 0 && logLevel <= LOG_LEVEL_ERROR) {
            Log.e(sTag, "", tr);
        }
    }

    public static void e(String msg, Throwable tr) {
        int logLevel = getLogLevel();
        if (logLevel > 0 && logLevel <= LOG_LEVEL_ERROR) {
            Log.e(sTag, msg, tr);
        }
    }

    private static int getLogLevel() {
        return SystemProperties.getInt(LOG_LEVEL_PROPERTIES, DEFAULT_LOG_LEVEL);
    }

    public static void setTag(String tag) {
        sTag = tag;
    }
}
