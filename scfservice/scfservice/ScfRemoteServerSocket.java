/******************************************************************************
 * @file    ScfRemoteServerSocket.java
 *
******************************************************************************/

package com.spectratech.scfservice;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Iterator;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.concurrent.atomic.AtomicBoolean;

import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import com.android.internal.os.SomeArgs;

import static java.lang.Short.MAX_VALUE;
import static java.lang.Short.MIN_VALUE;

public class ScfRemoteServerSocket extends Handler implements Runnable {
    private static final String LOG_TAG = "ScfRemoteServerSocket";

    private final String SocketAddress = "spid";
    private final int SOCKET_FAILED_RETRY_TIME = 8;
    private final int SOCKET_FAILED_SLEEP_TIME = 4000;

    // length of SPI message header
    private final int SPI_PROTOCOL_HEADER_LENGTH = 6;

    // length of SPID protocol header
    private final int SPID_PROTOCOL_HEADER_LENGTH = 6;

    private LocalSocket mSocket = null;
    private InputStream mIS = null;
    private OutputStream mOS = null;

    private static final Object mStaticLockOut = new Object();

    private final int BUFFER_SIZE = 4096;

    private Handler mReceiveHandler = null;
    private CopyOnWriteArrayList<SCSystemCall> mRequestList;

    CountDownLatch mSendingCountDown;

    Thread backgroundThread;

    public ScfRemoteServerSocket(Handler recvHdlr){
        ScfLog.d("ScfRemoteServerSocket()");
        mRequestList = new CopyOnWriteArrayList<SCSystemCall>();
        mReceiveHandler = recvHdlr;
    }


    public void start() {
       if( backgroundThread == null ) {
          backgroundThread = new Thread( this );
          backgroundThread.start();
          mRequestList.clear();
       }
    }

    public void stop() {
        ScfLog.d("stop()");
        if( backgroundThread != null ) {
            backgroundThread.interrupt();
            mRequestList.clear();
        }
        // resetSocket();
    }

    private boolean connectSocket() {
        ScfLog.d("connectSocket()");
        // TODO - reset previous socket?
        // resetSocket();

        int retryCount = 0;

        try {for (;;) {
            try{
                LocalSocketAddress addr = new LocalSocketAddress(SocketAddress, LocalSocketAddress.Namespace.RESERVED);
                mSocket = new LocalSocket();

                ScfLog.i("Connecting to socket " + addr.getName() + "...");
                mSocket.connect(addr);

                ScfLog.i("Connected to socket " + addr.getName());
                mOS = mSocket.getOutputStream();
                mIS = mSocket.getInputStream();

                return true;
            } catch (IOException e) {
                ScfLog.e("Socket connection failed");
                e.printStackTrace();

                try {
                    if (mSocket != null) {
                        mSocket.close();
                    }
                } catch (IOException ex2) {
                    //ignore failure to close after failure to connect
                }
                mSocket = null;

                // don't print an error message after the the first time
                // or after the 8th time

                if (retryCount == SOCKET_FAILED_RETRY_TIME) {
                    ScfLog.e(
                        "Couldn't find '" + SocketAddress
                        + "' socket after " + retryCount
                        + " times, continuing to retry silently");
                } else if (retryCount >= 0 && retryCount < SOCKET_FAILED_RETRY_TIME) {
                    ScfLog.w(
                        "Couldn't find '" + SocketAddress
                        + "' socket; retrying after timeout");
                }

                try {
                    Thread.sleep(SOCKET_FAILED_SLEEP_TIME);
                } catch (InterruptedException sleepExp) {
                }

                retryCount++;
                continue;
            }
        }} catch (Throwable tr) {
            ScfLog.e("Uncaught exception", tr);
        }
        return false;
    }

    private void resetSocket() {
        ScfLog.d("resetSocket()");
        if (mSocket != null) {
            try {
                mRequestList.clear();
                mSocket.shutdownInput();
                mSocket.shutdownOutput();
                mSocket.close();
            } catch (IOException e) {
                ScfLog.e("resetSocket() - failed!");
                e.printStackTrace();
            }
            mSocket = null;
            mIS = null;
            mOS = null;

        } else {
            ScfLog.w("resetSocket() - socket is not initialized");
        }
    }

    @Override
    public void handleMessage(Message msg) {
        switch (msg.what) {
            case SCFunctionServiceImpl.MESSAGE_CANCEL_REQUEST:
            {
                SomeArgs args = (SomeArgs)msg.obj;
                int requestId = (Integer) args.arg1;

                for (SCSystemCall sccall : mRequestList) {
                    if (requestId == sccall.getToken()) {
                        mRequestList.remove(sccall);
                        break;
                    }
                }
                synchronized (args.arg2) {
                    ((CountDownLatch)args.arg2).countDown();
                }
                args.recycle();
                return;
            }

            case SCFunctionServiceImpl.MESSAGE_SEND_REQUEST:
            {
                SomeArgs args = (SomeArgs)msg.obj;
                SCSystemCall sccall = (SCSystemCall) args.arg1;
                Log.d("ScfRemoteServerSocket","==> appid:" + sccall.getAppId()
                            + " classid:0x" + Integer.toHexString(sccall.getClassId())
                            + " funcid:0x" + Integer.toHexString(sccall.getFuncId())
                            + " token:" + sccall.getToken()
                            + " payload len:" + sccall.getPayloadLength());

                int token = (int)msg.arg1;
                int id = ((sccall.getClassId() & 0xFF) << 8) | (sccall.getFuncId() & 0xFF);
                // ScfLog.v("id: " + id + " token:" + token);
                if ( id == 0x1F02 && mRequestList != null ) {
                    // mRequestList.clear(); //clear after send_complete
                } else {
                    mRequestList.add(sccall);
                }

                // TODO : dummy gcm_iv value
                short gcm_iv = 0x0000;

                // spidaemon protocol
                int len = sccall.getPayloadLength();
                byte[] bytes = new byte[len + SPI_PROTOCOL_HEADER_LENGTH];

                    bytes[0] = (byte)sccall.getClassId();
                    bytes[1] = (byte)sccall.getFuncId();
                    bytes[2] = (byte)((gcm_iv >> 8) & 0xFF);
                    bytes[3] = (byte)((gcm_iv >> 0) & 0xFF);
                    bytes[4] = (byte)((len >> 8) & 0xFF);
                    bytes[5] = (byte)((len >> 0) & 0xFF);

                if (len > 0) {
                    byte[] payload = sccall.getPayload();
                    if ((len & 0x01) != 0) {
                        for (int i = 0; i < (len - 1); i += 2) {
                            bytes[i + SPI_PROTOCOL_HEADER_LENGTH]     = payload[i + 1]; //swap high / low bytes
                            bytes[i + SPI_PROTOCOL_HEADER_LENGTH + 1] = payload[i];
                        }
                        bytes[len + SPI_PROTOCOL_HEADER_LENGTH - 1] = payload[len - 1];
                    } else {
                        for (int i = 0; i < len; i += 2) {
                            bytes[i + SPI_PROTOCOL_HEADER_LENGTH]     = payload[i + 1]; //swap high / low bytes
                            bytes[i + SPI_PROTOCOL_HEADER_LENGTH + 1] = payload[i];
                        }
                    }
                }

                // ScfLog.d("bytes:" + bytesToHex(bytes));
                byte[] bytes1 = Arrays.copyOf(bytes, 16);               

                post(new Runnable() {
                    public void run() {
                        mSendingCountDown = new CountDownLatch(1);
                        if (mSocket == null || mOS == null) {
                            ScfLog.e("send() - spid socket is not connected!");
                            return;
                        }

                        try {
                            mOS.write(bytes);
                            mOS.flush();
                        } catch (IOException e) {
                            ScfLog.e("send() - Sending failed !!!");
                            e.printStackTrace();
                        }
                    }
                });

                if (mSendingCountDown != null) {
                    try {
                        if (id != 0x1FF0 && !mSendingCountDown.await(600, TimeUnit.MILLISECONDS)) {
                            ScfLog.w("Timeout waiting for sending complete");
                        }
                    } catch (InterruptedException e) {
                        Log.e(LOG_TAG, "Sending count down interruped!");
                    }
                    mSendingCountDown = null;
                }

                synchronized (args.arg2) {
                    ((CountDownLatch)args.arg2).countDown();
                }
                args.recycle();
                Log.d(LOG_TAG, "<== bytes.length:"+ bytes.length + " bytes1:" + bytesToHex(bytes1));
                return;
            } //case SCFunctionServiceImpl.MESSAGE_SEND_REQUEST
            default:
            {
                ScfLog.w("Unknown message "+msg.what);
                return;
            }
        }
    }

    private void send(byte[] bytes) {
        if (mSocket == null || mOS == null) {
            ScfLog.e("send() - spid socket is not connected!");
            return;
        }

        try {
            mOS.write(bytes);
            mOS.flush();
        } catch (IOException e) {
            ScfLog.e("send() - write failed !!!");
            e.printStackTrace();
        }
    }

    @Override
    public void run(){
        ScfLog.d("run()");
        boolean toConnectSocket = true;
        while (!backgroundThread.interrupted()) {
            if (toConnectSocket && !connectSocket()) {
                ScfLog.e("run() - connect socket failed.");
                return;
            }
            toConnectSocket = false;

            /*
             * A very simple protocol between SCFuncLib and spid
             * Two bytes type indicates the response type from SC
             * int TYPE_SEND_COMPLETE   = 0x01;
             * int TYPE_SEND_FAIL       = 0x02;
             * int TYPE_REPLY_COMPLETE  = 0x03;
             * int TYPE_REPLY_FAIL      = 0x04;
             * +-------------------------------+
             * |            message            |
             * +-------------------------------+
             *  \                               \_____________________________
             *   \                                                            \
             *    +----------+-----------+-------------+------------+---------+
             *    | type (2) | lenth (2) | classid (1) | funcid (1) | payload |
             *    +----------+-----------+-------------+------------+---------+
             *
            */
            int bytesRead = 0;
            int posOffset = 0;
            int bytesToRead = SPID_PROTOCOL_HEADER_LENGTH;
            short type = 0;
            short length = 0;
            int classid;
            int funcid;
            byte[] buffer = new byte[BUFFER_SIZE];

            if (mIS != null) {
                try {
                    do {
                        bytesRead = mIS.read(buffer, posOffset, bytesToRead);
                        if (bytesRead < 0) {
                            ScfLog.e("run() - bytesRead < 0 when reading message length");
                            break;
                        }
                        posOffset += bytesRead;
                        bytesToRead -= bytesRead;
                    } while (bytesToRead > 0);
                } catch (IOException | ArrayIndexOutOfBoundsException e) {
                    ScfLog.e("Exception in reading socket");
                    e.printStackTrace();
                    break;
                }
            }

            if (bytesRead < 0) {
                toConnectSocket = true;
                continue;
            }

            ScfLog.v("run() - "+String.format("0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x",
                buffer[0], buffer[1], buffer[2], buffer[3], buffer[4], buffer[5]));

            type = (short) (((buffer[1] & 0xFF) << 8) | (buffer[0] & 0xFF));
            length = (short) (((buffer[3] & 0xFF) << 8) | (buffer[2] & 0xFF));
            bytesToRead = length;
            classid = buffer[4] & 0xFF;
            funcid = buffer[5] & 0xFF;

            ScfLog.d("run() type:"+type+" length:"+length);
            if (length > BUFFER_SIZE) {
                // TODO error?
                ScfLog.e("run() too long");
                continue;
            }
            // re-use buffer to obtain message payload
            posOffset = 0;

            if (mIS != null) {
                try {
                    do {
                        bytesRead = mIS.read(buffer, posOffset, bytesToRead);
                        if (bytesRead < 0) {
                            ScfLog.e("run() - bytesRead < 0 when reading message");
                            break;
                        }
                        posOffset += bytesRead;
                        bytesToRead -= bytesRead;
                    } while (bytesToRead > 0);
                } catch (IOException | ArrayIndexOutOfBoundsException e) {
                    ScfLog.e("Exception in reading socket");
                    e.printStackTrace();
                    break;
                }
            }

            if (bytesRead < 0) {
                toConnectSocket = true;
                continue;
            }
            handleRecvBytes(classid, funcid, type, buffer, length);
        }
    }

    private void handleRecvBytes(final int classId, final int funcId, int type, byte[] buffer, int length) {
        ScfLog.v("handleRecvBytes()");
        synchronized(mStaticLockOut) {
            if (mReceiveHandler == null) {
                return;
            }

            try {
                int id = ((classId & 0xFF) << 8) | (funcId & 0xFF);
                int token = 0;
                Message msg;
                byte[] bytes = Arrays.copyOf(buffer, length);
                // Log.d(LOG_TAG, "handleRecvBytes() ==> "+
                //     String.format("classId: 0x%02X ", classId) +
                //     String.format("funcId: 0x%02X ", funcId));

                switch (type) {
                    case SCFunctionServiceImpl.TYPE_SEND_COMPLETE:
                        for (SCSystemCall sccall : mRequestList) {
                            if (classId == (sccall.getClassId() & 0xFF) &&
                                funcId == (sccall.getFuncId() & 0xFF)) {
                                token = sccall.getToken();
                                if (!sccall.getHasResponse()) {
                                    mRequestList.remove(sccall);
                                }
                                if (mSendingCountDown != null) {
                                    mSendingCountDown.countDown();
                                }
                                break;
                            }
                        }
                        break;

                    case SCFunctionServiceImpl.TYPE_SEND_FAIL:
                        for (SCSystemCall sccall : mRequestList) {
                            if (classId == (sccall.getClassId() & 0xFF) &&
                                funcId == (sccall.getFuncId() & 0xFF)) {
                                token = sccall.getToken();
                                mRequestList.remove(sccall);
                                if (mSendingCountDown != null) {
                                    mSendingCountDown.countDown();
                                }
                                break;
                            }
                        }
                        break;

                    case SCFunctionServiceImpl.TYPE_REPLY_COMPLETE:
                        int id1 = (((bytes[0] & 0xFF) << 8) | (bytes[1] & 0xFF));

                        for (SCSystemCall sccall : mRequestList) {
                            if (classId == (sccall.getClassId() & 0xFF) &&
                                funcId == (sccall.getFuncId() & 0xFF)) {
                                token = sccall.getToken();
                                mRequestList.remove(sccall);
                                break;
                            }
                        }

                        // only swap high / low bytes for payload data
                        if (bytes.length > SPI_PROTOCOL_HEADER_LENGTH) {
                             for (int i = SPI_PROTOCOL_HEADER_LENGTH; i < bytes.length; i += 2) {
                                 byte tmp = bytes[i + 1];
                                 bytes[i + 1] = bytes[i];
                                 bytes[i] = tmp;
                             }
                        }

                        if (id1 == 0x1F0A) {// SC Debug ascii message
                            String str = new String(Arrays.copyOfRange(bytes, 6, bytes.length));
                            Log.i("SC-message", ""+str);
                            return;
                        }

                        if (id1 == 0x1F0B) {// SC Debug hex message
                            Log.i("SC-message", ""+bytesToHex(Arrays.copyOfRange(bytes, 6, bytes.length)));
                            return;
                        }

                        if (id1 == 0x1F02) {
                            ScfLog.d("reply sc reset");
                            mRequestList.clear();
                        }

                        // token not found in queue, consider it is a dispatcher response
                        if (token == 0) {
                            type = SCFunctionServiceImpl.TYPE_DATA_DISPATCH;
                        }
                        break;

                    case SCFunctionServiceImpl.TYPE_REPLY_FAIL:
                        int id2 = (((bytes[0] & 0xFF) << 8) | (bytes[1] & 0xFF));

                        for (SCSystemCall sccall : mRequestList) {
                            if (classId == (sccall.getClassId() & 0xFF) &&
                                funcId == (sccall.getFuncId() & 0xFF)) {
                                token = sccall.getToken();
                                mRequestList.remove(sccall);
                                break;
                            }
                        }

                        if (id2 == 0x1F02) {
                            ScfLog.d("reply sc reset");
                            mRequestList.clear();
                        }

                        break;

                    default:
                        ScfLog.w("unknown respType:"+ type);
                        return;
                }

                ScfLog.d("type="+ type +" token="+token+" bytes:"+bytesToHex(bytes));
                msg = mReceiveHandler.obtainMessage(SCFunctionServiceImpl.MESSAGE_RESPONSE, type, token, bytes);
                msg.sendToTarget();
                // Log.d(LOG_TAG, "handleRecvBytes() <== type="+ type +" token="+token);

            } catch (ArrayIndexOutOfBoundsException e) {
                ScfLog.e("ArrayIndexOutOfBoundsException");
                e.printStackTrace();
                return;
            }
        }
    }

    private static short toShort(int num) {
        if (num > MAX_VALUE) {
            throw new IllegalArgumentException(num + " > " + MAX_VALUE);
        } else if (num < MIN_VALUE) {
            throw new IllegalArgumentException(num + " < " + MIN_VALUE);
        }
        return (short) num;
    }

    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}
